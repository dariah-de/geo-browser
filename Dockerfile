###
# PLATIN
###
FROM ruby:3.0 as platin-builder
ARG PLATIN_BRANCH="develop"

RUN mkdir build
WORKDIR /build

RUN git clone -b ${PLATIN_BRANCH} --single-branch https://github.com/DARIAH-DE/PLATIN.git
RUN apt-get update && apt-get -y install default-jre

WORKDIR /build/PLATIN

RUN rake all 

###
# DOCS
###
FROM python:3.10 as doc-builder

COPY ./docs /docs
WORKDIR /docs

RUN apt-get update && apt-get -y install python3-virtualenv
RUN virtualenv venv
RUN pwd . venv/bin/activate
RUN pip install -r requirements.txt
#RUN beta="-BETA"
#RUN if test "$BRANCH_NAME" = 'main'; then beta=""; fi
RUN RELEASE_DATE="$(date +'%Y/%m/%d')${beta}"
RUN RELEASE_VERSION="$(date +'%Y')${beta}"
RUN make clean html SPHINXOPTS="-D version=${RELEASE_VERSION} -D release=${RELEASE_DATE}"

###
# DATASHEET EDITOR
###
FROM node:15 as js-builder

COPY . /build
COPY --from=platin-builder /build/PLATIN /build/PLATIN
WORKDIR /build

RUN npm ci
# echo "VERSION=${VERSION}" >> build.env
RUN npm run build:release




###
# assemble container
###
FROM php:8.2.3-apache-bullseye

RUN pwd

COPY --from=js-builder /build/dist /var/www/html
COPY --from=doc-builder /docs/_build/html /var/www/html/doc


