.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


====================
The Datasheet Editor
====================


The DARIAH-DE Datasheet Editor is accessible at https://geobrowser.de.dariah.eu/edit or by clicking “Datasheet Editor” on Geo-Browser. The Geo-Browser is documented in the tutorial :doc:`DARIAH-DE Geo-Browser <geobrowser>`.

Further information on the Geo-Browser and Datasheet Editor can be found at `DARIAH-DE Portal <https://de.dariah.eu/geobrowser>`__.

GUI documentation
-----------------

How does it work?
^^^^^^^^^^^^^^^^^

In the **Manage your data** section you can either create a new datasheet or import a local CSV file from your local drives. You can edit your new datasheet and copy'n'paste data from Excel, Calc, or Numbers. Your dataset will be stored to the DARIAH-DE OwnStorage and can be referenced by ID and URL. It will at first only be available to you for editing. Sharing and unsharing your data with everyone is possible whenever you like, please use the share and unshare dataset buttons. You can also download and delete your dataset.

From the place name in the “Address“ fields of your datasheet you can automatically **Add geocoordinates** using the Getty Thesaurus of Geographical Names, just click on geolocation completion. You can fine-tune your places and coordinates by using the **Place selection**, **Map selection**, and **Map search** sections.

If completed, you can **View your dataset** by opening it with the Geo-Browser.


Create or import your data
^^^^^^^^^^^^^^^^^^^^^^^^^^

The first option after calling the website is to either create a new datasheet or to import an existing datasheet. By clicking “Create a new datasheet”, a new document is generated. By clicking “Import local CSV or KML file“ a CSV or KML file from the local storage can be loaded into the datasheet.

KML files must comply with the `specification <https://geobrowser.de.dariah.eu/doc/_downloads/M3.3.2_eConnect_KML_Specification_v1.0_UGOE.pdf>`__ in order to be processed correctly. Hence, KML files with no “Placemark“ tag cannot be imported! Once the KML file was read without errors, it is transformed to CSV format and  also stored as such. As a CSV file, it can be enhanced by additional content such as further columns.

After clicking on “Create new datasheet“ or “Durchsuchen...“ / “Browse...“, you will be redirected to the DARIAH-DE log-in, the DARIAH AAI (Authentication & Authorization Infrastructure).

Here, you have to log in with your DARIAH account or any other eduGAIN federation account.

.. figure:: datasheet-screenshots/1-datasheet-login.png
    :align: center

    Fig. 1: Login to the Datasheet Editor


.. figure:: datasheet-screenshots/2-datasheet-dariah-login.png
    :align: center

    Fig. 2: Login to the Datasheet Editor with your DARIAH account


If you are using your DARIAH account for the very first time, you will be redirected to your `User Data Page of the DARIAH AAI Selfservice <https://auth.de.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=modifyentry>`__. Please add at least your full name and read and confirm reading the DARIAH-DE Terms of Use `<https://hdl.handle.net/21.11113/0000-000B-CB46-2@data>`__. You only have this to do once.

.. figure:: datasheet-screenshots/2.1-datasheet-selfservice.png
    :align: center

    Fig. 2.1: Your User Data in the DARIAH AAI Selfservice

.. figure:: datasheet-screenshots/2.2-datasheet-terms-of-use.png
    :align: center

    Fig. 2.2: Please read and confirm the DARIAH-DE Terms of Use


You are then asked to grant the application access to your DARIAH-DE Storage. This will only be asked if you use the OwnStorage from Geo-Browser or Datasheet Editor the first time. Internally the Geo-Browser and Datasheet Editor are using the DARIAH-DE PDP and OAuth2 tokens for authorization.

.. figure:: datasheet-screenshots/3-pdp-grant.png
    :align: center

    Fig. 3: Confirm storage access permissions


The creation of your datasheet can now begin back on the Datasheet Editor's start page.

.. figure:: datasheet-screenshots/4-datasheet-editor-start.png
    :align: center

    Fig. 4: Datasheet Editor's start page


After logging in, a green window with the title “New datasheet created!” opens at the top of the Datasheet Editor. The new datasheet has now an own ID in the DARIAH-DE OwnStorage, with which the sheet can be retrieved again. By using the DARIAH-DE OwnStorage to store your datasets, you can work on a dataset privately until it is complete and working, and then you can share it with the world.

.. figure:: datasheet-screenshots/5-datasheet-created.png
    :align: center

    Fig. 5: New datasheet created


By right-clicking on a cell in the datasheet, a row above (“Insert row above”) or below (“Insert row below”) can be inserted. Rows can be removed via “Remove row” and columns via “Remove column”. Columns can be inserted as well either to the left (“Insert column left”) or right (“Insert column right”) in order to add further information about locations in the datasheet.

.. figure:: datasheet-screenshots/6-insert-column.png
    :align: center

    Fig. 6: Insert new column

By clicking on a cell, it gets highlighted in blue and is considered as selected. You can extend the selection by holding the left mouse button. Right-click to open the context menu and select an action for the selected cells. Alternatively, you can use a keyboard shortcut, e.g. CRTL+C for copying or DEL for deleting the contents of selected cells. Moreover, cell selection can be used to specify which addresses should be geocoded (see below).

If no cells are selected (highlighted in blue), the entire datasheet is considered as selected by default. You can clear a selection by either clicking on the button “Clear selection“ or by left-clicking on the white area outside the datasheet.

Find the coordinates for the places in your datasheet
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For locations entered into the Datasheet Editor, longitude and latitude must be specified in order to display them in the Geo-Browser. If these coordinates are not known, the tool „Geolocation completion“ and its refinement options “Place Selection“,  “Map selection“ and “Map search“ can be used to find them. By clicking “Geolocation completion“ in the “Add geocoordinates“ box, the fields “Longitude“, “Latitude“ and “GettyID“ are added automatically for all (or all selected) locations listed in the “Address“ field of the datasheet. These automatically processed locations will also be listed in the “Place selection“ box.

.. figure:: datasheet-screenshots/7-geolocation-completion.png
    :align: center

    Fig. 7: Using geolocation completion

By default, The Getty Thesaurus of Geographic Names (TGN) is used for finding coordinates for addresses (“geocoding“). With TGN, smaller locations are not always found. As alternatives, the drop-down menu in the “Place selection“ box offers two other geocoders: GeoNames and Nominatim by OpenStreetMap (OSM). OSM often finds smaller locations not available in the TGN, and also streets and house numbers. OSM also tends to be more accurate with larger locations by choosing the most relevant entry.

When using TGN, you further have the choice between geocoding based on the “Address“ column, or based on the “GettyID“ column. The default is “Address“. OSM and GeoNames can only be based on “Address“.

If the automatically retrieved location is not the desired one, you can click on the arrow next to the location name in the “Place selection“ box to open a drop-down menu with a list of other locations with the same name (if available). For example, when geocoding with TGN, the method “exact-match“ of the `DARIAH-DE Normdata Services <https://wiki.de.dariah.eu/display/publicde/DARIAH-DE+Normdatendienste#DARIAH-DENormdatendienste-ThesaurusofGographicalNames(Getty)>`_ queries the Thesaurus of Geographical Names and adds the coordinates of the first match to the table. Existing entries will not be overwritten automatically. If additional matches are available, they are added to the drop-down list. After choosing the correct location from this list, you can click “Set“ on the right of the drop-down menu to complete the corresponding fields “Longitude“, “Latitude“ and (when using TGN based on “Address“) “GettyID“ in the datasheet. By clicking “Set“, existing coordinates will be overwritten. Please note that the content of the „Address“ field itself will not be changed - except in the case of using TGN based on GettyID. Here, “Address“ fields will be completed automatically in case they are empty and as long as valid GettyIDs are given.

If the desired location is not listed in the drop-down menu, you can click on the “Map“ field next to it. By this, the location will be entered in the “Map selection“ box, both above and below the world map. Below the world map you have the option of searching TGN, OpenStreetMap (OSM) or GeoNames for the address. By clicking the “Set“ field above the world map, the datasheet fields “Longitude“ and “Latitude“ will be updated for the corresponding location. Existing values will be overwritten. If you have multiple locations with the same address in the “Address“ column and want to update only some of them, select them in the table first. Caution: the address entered in the text field next to “Set“ has to be spelled in the same way as the term in the “Address“ field of the datasheet!

.. figure:: datasheet-screenshots/8-manual-search.png
    :align: center

    Fig. 8: Manual place and map selection

Another option is to drag the flag in the “Map selection“ box with the mouse in order to retrieve longitude and latitude with high precision. By zooming into the map, streets names can been viewed in detail. A manual address search by using TGN, OSM or GeoNames is also possible. When using TGN, you can even enter a GettyID into the search field. With OSM and Geonames, however, no GettyIDs are recognized or returned.

.. figure:: datasheet-screenshots/9-current-table-view.png
    :align: center

    Fig. 9: Completed table view


DARIAH-DE OwnStorage
^^^^^^^^^^^^^^^^^^^^

Initially, the datasets stored in the OwnStorage are accessible only to you. You can read, write, delete, and share each of your datasets.

.. figure:: datasheet-screenshots/10-share-dataset.png
    :align: center

    Fig. 10: Share your dataset with the world


If you want to share your datasets, you can do so by clicking on “Share dataset“ in the “Manage your data“ box. The dataset shown in the table view will then become available for everyone to read, editing the dataset will still only be possible by you. You can unshare the dataset any time you like. You can also delete your dataset by clicking “Delete dataset“ in the “Manage your data“ box. Sharing to and editing with a specific group of people is not yet possible.

.. figure:: datasheet-screenshots/11-shared-dataset.png
    :align: center

    Fig. 11: Dataset has been shared


If you click on “Create new dataset“ in table view, your currently displayed dataset is not being deleted and can be viewed again by its ID or URL.

.. figure:: datasheet-screenshots/12-create-new-dataset-in-table-view.png
    :align: center

    Fig. 12: Create new dataset from table view


Access your data
^^^^^^^^^^^^^^^^

When a new datasheet is created, an ID from the DARIAH-DE OwnStorage Service is assigned to the file. The datasheet can be retrieved again with the ID.

.. figure:: datasheet-screenshots/14-search-for-local-csv-files.png
    :align: center

    Fig. 13: Import local CSV files


By clicking “Download datasheet“ in the “Manage your data“ box, the file can be downloaded as a CSV file and stored in the local storage. Furthermore, the storage ID is shown, and the complete URL can be copied to the clipboard. Using this URL the file can be addressed directly.

You can also delete your dataset by clicking “Delete dataset“ in the “Manage your data“ box.

.. figure:: datasheet-screenshots/15-delete-dataset.png
    :align: center

    Fig. 14: Deleting a dataset


With this action the data from the datasheet can be opened in Geo-Browser. The entered locations are shown as points on the world map. The data is also shown below the world map in a list. If you shared your data, everyone is able to see it in the Geo-Browser. Otherwise, only you can see it.

.. figure:: datasheet-screenshots/16-open-with-geo-browser.png
    :align: center

    Fig. 15: Open your dataset with the Geo-Browser


As soon as you make an entry in your datasheet, it will be saved (autosave always on). In the datasheet the following data can be entered:

    **Name** enables you to name your data point. Places have to be entered in the “Address“ field, so “Name“ can be used to display the name of an event or a name that would not be recognized as Address.

    **Address** identifies the place and is used to add the values for the “Longitude“ and “Latitude“ fields via “Geolocation completion“.

    **Description** is used to display any additional data.

    **Longitude** can be filled in by “Geolocation completion“ or manually entered and uses decimal geographic coordinates. Existing coordinates will not be overwritten by “Geolocation completion“.

    **Latitude** can be filled in by “Geolocation completion“ or manually entered and uses decimal geographic coordinates. Existing coordinates will not be overwritten by “Geolocation completion“.

    **TimeStamp** is used to store the date of an entry. Some examples of entries for this field would be *1970*, *1970-12*, *1970-12-10*, and *1970-12-10T20:15:00*. For more information on accepted date and time formats please have a look at `xsd:date <http://books.xmlschemata.org/relaxng/ch19-77041.html>`__, `xsd:dateTime <http://books.xmlschemata.org/relaxng/ch19-77049.html>`__, `xsd:gYear <http://books.xmlschemata.org/relaxng/ch19-77127.html>`__, and `xsdGYearMonth <http://books.xmlschemata.org/relaxng/ch19-77135.html>`__ types.

    **TimeSpan:begin** and **TimeSpan:end** can be used to convey a timespan. Some examples of entries for this field would also be *1970*, *1970-12*, *1970-12-10*, and *1970-12-10T20:15:00*. For more information on accepted date and time formats please have a look at `xsd:date <http://books.xmlschemata.org/relaxng/ch19-77041.html>`__, `xsd:dateTime <http://books.xmlschemata.org/relaxng/ch19-77049.html>`__, `xsd:gYear <http://books.xmlschemata.org/relaxng/ch19-77127.html>`__, and `xsdGYearMonth <http://books.xmlschemata.org/relaxng/ch19-77135.html>`__ types.

    **GettyID** is used for the exact identification of TGN locations and is automatically filled in using the Getty Thesaurus of Geographic Names via “Geolocation completion“. An existing GettyID will not be overwritten by “Geolocation completion“.

The description of these variables can also be found in the Datasheet Editor by clicking on the “How to fill the table“ button right to the datasheet. It is not required to enter all data to create a datasheet.

.. figure:: datasheet-screenshots/17-how-to-fill-the-table.png
    :align: center

    Fig. 16: Help is on the way -- How to fill the table


New vs. old datasets in the Datasheet Editor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The look of the dataset's IDs has slightly changed: instead of *721203* the IDs now look like *EAEA0-7A23-E68F-EBC2-0*. The storages URL host and path of “old“ datasets stays as it always has been: https://geobrowser.de.dariah.eu/storage/, new hostname and path of datasets now will be https://cdstar.de.dariah.eu/dariah/.

All your old dataset references (CSV and KML files) will still be readable via their old URLs, such as https://geobrowser.de.dariah.eu/storage/721203. If you do not want to edit an old dataset, just leave it as it is. Otherwise, you could just store it to the OwnStorage. Then edit it and share it if you want it to be publicly available.


Migrating an old dataset to the OwnStorage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

How it works:

    1. Just request an old URL for editing with the Datasheet Editor including an old dataset storage ID, such as https://geobrowser.de.dariah.eu/edit?id=721203.

    2. The old dataset is loaded and displayed read-only in the table view. You now can (i) download the old dataset as a CSV file using the download button, or (ii) create a new dataset automatically in the DARIAH-DE OwnStorage by clicking “Create new datasheet“ in the “Manage your data“ box. The newly created dataset will contain your old data and you then can edit and share it. The old dataset will not be touched.

    3. An example of a migrated and already shared dataset you can see here: https://geobrowser.de.dariah.eu/edit?id=EAEA0-7FCA-A72C-9B3C-0.


Open with Geo-Browser
^^^^^^^^^^^^^^^^^^^^^

With this action the data from the datasheet can be opened in Geo-Browser. The entered locations are shown as points on the world map. The data is also shown below the world map in a list. If you shared your data, everyone is able to see it in the Geo-Browser. Otherwise, only you can see it.

The Geo-Browser documentation can be found here: :doc:`DARIAH-DE Geo-Browser <geobrowser>`.



Versions and Releases
---------------------

The current version of the DARIAH-DE Datasheet Editor is always accessible at https://geobrowser.de.dariah.eu/edit. We try to keep new versions compatible with older ones. Please note that there have been major changes in version 3.6.

.. note::

    With version 3.6 of the DARIAH-DE Geo-Browser and Datasheet Editor some changes took place mainly affecting authentication, authorization, and accessibility:

    **AAI (Authentication & Authorization)**: With version 3.6 of Geo-Browser and Datasheet Editor the DARIAH-DE OwnStorage is used to store your datasets! That makes the usage of the Geo-Browser and Datasheet Editor more secure, and you can work on your datasets privately until your dataset is complete and working, and then you can share it with the world. To store your datasets to the DARIAH-DE OwnStorage, you will have to log in (authenticate) via the DARIAH AAI using your DARIAH account or an eduGAIN federation account. Internally the Geo-Browser and Datasheet Editor are using the DARIAH-DE PDP and OAuth2 tokens for authorization.

    **Accessibility**: The datasets stored in the OwnStorage are only accessible to the owners at first. Only they can read, write, delete, and share each of the datasets.

    **Sharing and unsharing**: If you want to share your datasets, you can do so using the Datasheet Editor's “Manage your data“ box. The dataset shown in the table view will then become available for everyone to read. You can unshare the dataset any time you like. Sharing to and editing with a specific group of people is not yet possible.

    **Identifiers and references**: The look of the dataset's IDs have slightly been changed: instead of *721203* the IDs now look like *EAEA0-7A23-E68F-EBC2-0*. The storages URL host and path of “old“ datasets stays https://geobrowser.de.dariah.eu/storage/. The new host and path of datasets will now be https://cdstar.de.dariah.eu/dariah/.

    **URLs**: With the ID of your dataset and depending on the dataset's shared status you now can

        1. directly access your shared datasets in the DARIAH-DE OwnStorage: https://cdstar.de.dariah.eu/dariah/EAEA0-8BAC-F1F7-FB43-0. This URL is used for storing and sharing a dataset. Please note: Only shared datasets can be downloaded using this URL!

        2. access and edit your datasets in the Datasheet Editor: https://geobrowser.de.dariah.eu/edit?id=EAEA0-8BAC-F1F7-FB43-0. This is the link to the Datasheet Editor and your dataset to be edited.

        3. view your dataset in the Geo-Browser if you are the owner of a private dataset or dataset has been shared: https://geobrowser.de.dariah.eu?csv1=https://cdstar.de.dariah.eu/dariah/EAEA0-8BAC-F1F7-FB43-0.

    **Old dataset references**: All your „old“ dataset references will still be readable via their old URLs, such as https://geobrowser.de.dariah.eu/storage/721203. If you want to edit an old dataset, you could just migrate it to the OwnStorage and then edit it and share it if you want it to be publicly available:

        1. Just call an old URL for editing with the Datasheet Editor including an old dataset storage ID, such as https://geobrowser.de.dariah.eu/edit?id=721203.

        2. The old dataset is loaded and displayed read-only in the table view. You now can (i) download the old dataset as a CSV file using the download button, or (ii) create a new dataset automatically in the DARIAH-DE OwnStorage by clicking “Create new datasheet“. The newly created dataset will contain your old data and you then can edit and share it. The old dataset will not be touched.



GIT Repository
--------------

    * https://gitlab.gwdg.de/dariah-de/geo-browser/-/tree/main


Bug Tracking
------------

    * https://gitlab.gwdg.de/dariah-de/geo-browser/-/issues
