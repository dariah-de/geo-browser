.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


===============
Geo-Browser FAQ
===============

.. topic:: I would like to display the places of residence of multiple authors. Is it possible to show two or more biographies to compare?

    Yes, you can visualize multiple biographies if you create one datasheet for every author. Each datasheet maps its residences in different colors, in the timeline and the table. This `example <https://geobrowser.de.dariah.eu/index.html?kml1=http://geobrowser.de.dariah.eu/data/goethe.kml&kml2=http://geobrowser.de.dariah.eu/data/schiller.kml>`__ covers the places and times of publication of Goethe and Schiller from the OPAC of SUB Goettingen.

    To add data to your project click the load data-function and choose a local CSV or KML/KMZ-file. Click the load-button to add the new source to your Geo-Browser.


.. topic:: Can I display my sheets created with the Datasheet Editor in the Geo-Browser directly?

    Yes, you can use the above used Geo-Browser URL, just embed your datasheet's storage URLs as following: <https://geobrowser.de.dariah.eu/index.html?csv1=https://cdstar.de.dariah.eu/test/dariah/EAEA0-2593-C1E7-D88A-0&csv2=https://cdstar.de.dariah.eu/test/dariah/EAEA0-D088-5071-4C39-0> **TODO: Please do add "real" links!**


.. topic:: We would like to embed the Geo-Browser in our website. Is this possible?

    Our Geo-Browser’s `Embedded-Version <http://geobrowser.de.dariah.eu/embed/>`__ can be embedded in any website. To display correctly your URL has to refer to one or multiple CSV- or KML-files, for example:

    http://geobrowser.de.dariah.eu/embed/?kml=http://geobrowser.de.dariah.eu/data/goethe.kml

    or

    http://geobrowser.de.dariah.eu/embed/index.html?kml1=http://geobrowser.de.dariah.eu/data/goethe.kml&kml2=http://geobrowser.de.dariah.eu/data/schiller.kml.

    URL’s can be recalled as Magnetic Link. To do this you have to load your data in Geo-Browser and copy the link under Dataset and Magnetic Link (right-click, then “copy link address”, the link is now in your clipboard).

    Just like the URL <http://geobrowser.de.dariah.eu/> the Embedded-Version will stay stable (please refer to `Geo-Browser <https://wiki.de.dariah.eu/display/publicde/Geo-Browser+Dokumentation#Geo-BrowserDokumentation-VersionierungundRelease>`__ and `Datasheet Editor <https://wiki.de.dariah.eu/display/publicde/Datasheet+Editor+Dokumentation#DatasheetEditorDokumentation-VersionierungundRelease>`__ Versioning and Release).


.. topic:: What map-data is used in Geo-Browser? Can we also use it?

    The current base-map is `OpenStreetMap <https://www.openstreetmap.org/>`__. The historic maps where created by the `europeana4d <http://labs.europeana.eu/apps/Europeana4D/>`__ project and are used in public domain. More information can be found at the `original e4D-Service <http://www.informatik.uni-leipzig.de:8080/e4D/>`__. The same data is also used in our `GeoServer <https://ref.de.dariah.eu/geoserver/web/>`__, which provides the Geo-Browser. You can download and use these maps at `Layer-Preview <https://ref.de.dariah.eu/geoserver/web/H3YQtIyw50o4teVI-BQjXBzecL2pbSEacJIklbLAtXrfIgS6fGUWeukMclexVZpZ4IPY3tcpwbDJYV8h55zAlg/H3Y2d/JYV66>`__.


.. topic:: What datatype can be uploaded at Load Overlay under the ArcGIS WMS and XYZ Layer option?

    Both are not a datatype, but a layer-type/service for overlays, displayed on the map. ArcGIS WMS uses a WMS-Server-URL to show a specific layer. XYZ Layer can enter a URL with x/y/z parameters in `OpenLayers-Format <http://dev.openlayers.org/docs/files/OpenLayers/Format-js.html>`__.


.. topic:: Can I integrate my own map-data in Geo-Browser?

    Historic maps offered by our Geo-Browser are provided by `GeoServer <https://ref.de.dariah.eu/geoserver/web//>`__, which supports many different files and formats. Integrating digitized maps in GeoServer is possible, but not yet for users. Individual maps have to be adjusted to work with GeoServer, and then manually uploaded by us. We would be happy to integrate your maps if licensing is handled and if they are of interest for other users. More technical detail can be found at `GeoServer-Documentation <http://docs.geoserver.org/>`__.

    However Load Overlay allows you to display your own maps over the given base-map.


.. topic:: Why does Load Data not allow me to integrate KML, KMZ and CSV-files under KML/KMZ/CSV File URL?

    HTTP-requests by JavaScript to external URL’s by proxy-server are currently guarded. This means data from URL’s can only be loaded in GeoBrowser when they are registered in our proxy-server. We can register your hostname to our proxy-server to make your data accessible for you and other users. Please note that we only add official and institutional URL’s. You can contact us by `e-mail <info@de.dariah.eu>`__.


.. topic:: Is it possible to integrate a JPG or other HTML-Code in the datasheets to display it in the Geo-Browser or display URL’s as link for example?

    Yes, the “Description”-window allows you to use HTML-Code. <img>-, <a>-Tags and CSS-code can be formatted right there. For example: <http://geobrowser.de.dariah.eu/?kml1=http://geobrowser.de.dariah.eu/data/flickr/Earthquake.kml>
