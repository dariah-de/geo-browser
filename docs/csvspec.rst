.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


=================
CSV Specification
=================


CSV files store tabular data in plain text by separating values with a comma. Each line is a data record with multiple fields, separated by commas. The first line of the Datasheet Editor's CSV file specifies the value fields by listing them in correct order. The fields Datasheet Editor uses are as follows:

::

    "Name","Address","Description","Longitude","Latitude","TimeStamp","TimeSpan:begin","TimeSpan:end","GettyID"

The fields are used for the following information:

* “Name” enables you to name your data point. Places have to be entered in “Address” so “Name” can be used to display the name of an event or a name that would not be recognized as Address.

* “Address“ identifies the place and is used to add “Longitude” and “Latitude” via “Geolocation completion”

* “Description” is used to display any additional Data.

* “Longitude” and “Latitude” can be filled in by “Geolocation completion” or manually entered and uses decimal geographic coordinates. Existing coordinates will not be overwritten by “Geolocation completion”.

* “TimeStamp” is used to store the date of an entry. The format for time stamps is YYYY-MM-DD, however just naming the year is also possible.

* “TimeSpan:begin” and “TimeSpan:end” can be used to convey a timespan. Just like “TimeStamp” it uses YYYY-MM-DD but can be used without specifying the day.

* “GettyID” is automatically filled in using the “Getty Thesaurus of Geographic Names” via “Geolocation completion”. An existing GettyID will not be overwritten by “Geolocation completion”.


Every following Line contains data in the place of these categories. Fields without data remain empty and are displayed as "":

::

    "","Berlin","","13.4167","52.5333","2018","","","7003712"


This example shows a CSV line with Empty “Name” and “Description” fields. “TimeStamp” is shown as "2018" and the “TimeSpan” fields are also empty. CSV data like this can be used in Geo-Browser. All required fields are “Longitude”, ”Latitude” and “Address” for a visualization of the defined place. For displaying time-place relations, the field “TimeStamp“ or both fields “TimeSpan:begin“ and “TimeSpan:end“ must be present.

Für die Nutzung des Geo-Browsers sind folgende Felder zwingend erforderlich:

* Address
* TimeStamp oder TimeSpan:begin und TimeSpan:end
* Longitude
* Latitude

A CSV file, created by Datasheet Editor, automatically generates 40 empty lines.

::

    "","","","","","","","","",""

Unused lines remain empty and can be deleted or leaved out by CSV file creation.

Additional fields can be present in the table, nevertheless are possibly not displayed in the table view of the Geo-Browser.
