.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


===================
Datasheet EditorFAQ
===================


.. topic:: Wieso finde ich mit der Geolocation Completion nicht die Orte, die ich in die Tabelle eingetragen habe?

    Die Ortsnamen müssen (a) in die Spalte mit der Überschrift „Address“ eingetragen sein und (b) sollten Sie auf Groß-/Kleinschreibung achten. Momentan berücksichtigt die standardgemäß verwendete TGN-Suche nur korrekt groß geschriebene Ortsnamen (z. B. Mainz, Göttingen). Wenn Sie dies berücksichtigt haben und dennoch keine Treffer erhalten, ist der betreffende Ortsname möglicherweise nicht im Getty-Thesaurus verzeichnet. In diesem Fall können Sie im Dropdown-Menü unter „Place selection“ einen der beiden anderen verfügbaren Geocoder (OSM oder GeoNames) wählen und es damit erneut versuchen.


.. topic:: Meine Daten aus dem Datasheet Editor werden unvollständig im Geo-Browser dargestellt. Woran kann das liegen?

    Einige HTML-Sonderzeihen wie z. B. das „&“ bringen die Darstellung im Geo-Browser durcheinander. Prüfen Sie bitte Ihre Eingaben in der Tabelle auf solche Sonderzeichen und ersetzen Sie z. B. das „&“ mit einem „&amp;“. Mehr Informationen hierzu siehe unter `Bug #14359 <https://projects.gwdg.de/issues/14359>`__ im Geo-Browser-Bugtracker.


.. topic:: Kann ich auch nur ausgewählte Ortsnamen über den „Set“-Button mit Koordinaten versehen?

    Ja, das ist nun möglich. Wenn Sie beispielsweise mehrere Einträge für die Adresse „Berlin“ haben und nur einigen davon andere Koordinaten zuweisen möchten, ziehen Sie mit der linken Maustaste einen Auswahlbereich (blau umrandet) über die betreffenden Zellen in der Tabelle. Stellen Sie dabei sicher, dass die „Address“-Felder Teil der Selektion sind, nicht nur die Latitude/Longitude/GettyID-Felder. Am einfachsten ist es, komplette Zeilen zu selektieren, indem Sie mit der linken Maustaste auf den grauen nummerierten Zeilenindex (1,2,...) klicken und durch Ziehen die Auswahl beliebig erweitern oder verringern. Einzelne Zeilen können durch Klick mit der linken Maustaste und gleichzeitiges Drücken der STRG-Taste der Auswahl hinzugefügt werden. Wenn Sie anschließend den „Set“-Button nutzen (oder den gesamten „Geolocation completion“-Prozess starten), werden nur die Orte im selektierten Bereich der Tabelle berücksichtigt.


.. topic:: Die automatisch mit Getty ID erzeugten geographischen Daten sind nur partiell korrekt, im Bereich „Map Selection“ bspw. wurde Stuttgart auf der Karte im Gebiet um Köngen zwischen Plochingen und Denkendorf angezeigt, was einer geographischen Verschiebung von ca. 40 km entspricht und somit ein unbrauchbares Ergebnis anzeigt. Solche Abweichungen können auch für weitere eingegebene Ortsnamen beobachtet werden. Woran liegt das?

    Die Abweichungen der geographischen Koordinaten zu einem Stadtkern bei der Geokodierung lassen sich dadurch erklären, dass bei einigen Suchanfragen wie z. B. „Stuttgart“ als erster Treffer die Koordinaten des administrativen Gebietes Stuttgart (vgl. `GettyID 1003262 <http://www.getty.edu/vow/TGNFullDisplay?find=1003262&place=&nation=&prev_page=1&english=Y&subjectid=1003262>`__) angezeigt werden. Die Koordinaten sind damit nicht fehlerhaft, sondern beziehen sich auf eine unterschiedliche Definition des Raumes Stuttgart. Falls mehrere Ortstypen (z. B. administrativer Bereich, Hauptstadt etc.) gefunden werden, können sie durch den Dialog der „Place Selection“ des Datasheet Editors verfeinert werden. So fällt z. B. die zweite Position der Dropdown-Liste „Stuttgart:Stuttgart district“ mit den zugehören Koordinaten in das Stadtgebiet Stuttgarts. Falls ein anderer Bereich gewünscht ist, kann dieser auch manuell mit dem Versetzen des Fähnchen-Markers auf der Karte selektiert werden.
