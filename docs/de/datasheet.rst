.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


====================
Der Datasheet Editor
====================


Der DARIAH-DE Datasheet Editor kann unter https://geobrowser.de.dariah.eu/edit oder mit Klick auf „Datasheet Editor“ im Geo-Browser aufgerufen werden. Der Geo-Browser wird im Tutorial :doc:`DARIAH-DE Geo-Browser <geobrowser>` dokumentiert.

Weitere Infos zu Geo-Browser und Datasheet Editor gibt es im `DARIAH-DE Portal <https://de.dariah.eu/geobrowser>`__.

GUI-Dokumentation
-----------------


Anlegen oder Importieren von Daten
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die erste Option nach Aufruf der Seite ist entweder ein neues Datasheet zu erstellen oder ein vorhandenes Datasheet zu importieren. Mit Klick auf „Create new datasheet“ kann ein neues Dokument erstellt werden. Mit Klick auf „Import local CSV or KML file“ kann eine CSV-Datei oder eine KML-Datei vom lokalen Speicher in ein Datasheet geladen werden.

CSV-Dateien können mithilfe von Microsoft Excel oder Open Office Calc erstellt werden. Unter Mac OS X können CSV-Dateien auch mit Numbers erstellt werden.

KML-Daten müssen der `Spezifikation <https://geobrowser.de.dariah.eu/doc/_downloads/M3.3.2_eConnect_KML_Specification_v1.0_UGOE.pdf>`__ entsprechen, um korrekt eingelesen zu werden. KML-Files, die kein „Placemark“-Tag enthalten, können also nicht importiert werden! Wurde das KML-File korrekt eingelesen, wird es in eine CSV-Datei umgewandelt und auch als solche im OwnStorage gespeichert. Als CSV-File kann es nun um beliebige Tabelleninhalte (z.B. zusätzliche Spalten) ergänzt werden.

Nach Klick auf „Create new datasheet“ or „Durchsuchen...“ / „Browse...“ werden Sie umgeleitet zum DARIAH-DE Login, zur DARIAH AAI (Authentication & Authorization Infrastructure).

Sie können sich mit Ihrem DARIAH-Account oder mit jedem anderen eduGAIN Föderations-Account einloggen.

.. figure:: ../datasheet-screenshots/1-datasheet-login.png
    :align: center

    Abb. 1: Einloggen in den Datasheet Editor


.. figure:: ../datasheet-screenshots/2-datasheet-dariah-login.png
    :align: center

    Abb. 2: Einloggen in den Datasheet Editor mit Ihrem DARIAH-Account


Benutzen Sie Ihren DARIAH-Account zum allerersten Mal, werden Sie zu Ihrer `Nutzerdaten-Seite des DARIAH AAI Self Service <https://auth.de.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=modifyentry>`__ weitergeleitet. Bitte ergänzen Sie Ihren vollen Namen und lesen Sie die `DARIAH-DE Nutzungsbedingungen <https://hdl.handle.net/21.11113/0000-000B-CB45-3@data>`__ und bestätigen Sie diese. Sie müssen dies nur einmalig tun.

.. figure:: ../datasheet-screenshots/2.1-datasheet-selfservice.png
    :align: center

    Abb. 2.1: Nutzerdaten-Seite des DARIAH AAI Selfservice

.. figure:: ../datasheet-screenshots/2.2-datasheet-terms-of-use.png
    :align: center

    Abb. 2.2: Bitte lesen und bestätigen Sie die DARIAH-DE Nutzungsbedingungen


Sie werden gebeten, dem Geo-Browser und dem Datasheet Editor Zugriff auf Ihren Bereich des DARIAH-DE OwnStorage zu geben. Dies wird nur abgefragt, wenn Sie zum ersten Mal den OwnStorage mit dem GeoBrowser oder dem Datasheet Editor nutzen wollen. Intern arbeiten der Geo-Browser und der Datasheet Editor für die Autorisierung mit dem DARIAH-DE PDP und OAuth2-Tokens.

.. figure:: ../datasheet-screenshots/3-pdp-grant.png
    :align: center

    Abb. 3: Bitte bestätigen Sie die DARIAH-DE Storage-Nutzung


Das Erstellen und Bearbeiten Ihres Datasheets beginnt nun auf der Startseite des Datasheet-Editors.

.. figure:: ../datasheet-screenshots/4-datasheet-editor-start.png
    :align: center

    Abb. 4: Die Startseite des Datasheet Editors


Nach dem Einloggen erscheint ein Bestätigungsfenster mit dem Titel „New datasheet created!“ im oberen Bereich des Datasheet Editors. Der neue Datensatz hat nun eine ID im DARIAH-DE OwnStorage und kann damit jederzeit wieder aufgerufen werden. Durch die Nutzung des OwnStorage kann zunächst privat an den Datensätzen gearbeitet werden, bis diese bereit sind zum Teilen.

.. figure:: ../datasheet-screenshots/5-datasheet-created.png
    :align: center

    Abb. 5: Ein neues Datasheet wurde erstellt


Durch Rechts-Klick in einer Zelle des Datasheets können Zeilen oberhalb der aktuellen Zeile („Insert row above”) und unterhalb der aktuellen Zeile („Insert row below”) hinzugefügt werden. Spalten können sowohl links durch „Insert column left” als auch rechts („Insert column right”) beiderseits hinzugefügt werden, um weiterführende Informationen über die zu beschreibenden Orte zu speichern. Zeilen und Spalten können durch „Remove row” bzw. „Remove column” entfernt werden.

.. figure:: ../datasheet-screenshots/6-insert-column.png
    :align: center

    Abb. 6: Einfügen einer neuen Spalte

Wenn Sie mit der Maus auf eine Zelle klicken, wird diese blau umrandet und gilt somit als selektiert. Indem Sie die linke Maustaste gedrückt halten, können Sie den Auswahlbereich auf mehrere Zellen erweitern. Klicken Sie dann einmal mit der rechten Maustaste, um das Kontextmenü zu öffnen und eine gewünschte Aktion für die Zellen in diesem Bereich auszuführen. Alternativ können Sie dafür Tastenkombinationen verwenden, zum Beispiel STRG+C um die Inhalte im markierten Bereich zu kopieren, oder ENTF um die Inhalte zu löschen. Das Selektieren von Tabellenbereichen kann zudem für die Geokodierung verwendet werden, um anzugeben, welche Bereiche geokodiert werden sollen (siehe unten).

Sind in der Tabelle keine einzelnen Zellen selektiert (blau umrandet), gilt standardgemäß die gesamte Tabelle als selektiert. Eine Selektion kann aufgehoben werden, indem Sie entweder auf den Button „Clear selection“ oder einmal mit der linken Maustaste auf die weiße Fläche außerhalb der Tabelle klicken.

Ermittlung der Koordinaten für die Orte im Datasheet
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Um in den Editor eingetragene Orte im Geo-Browser darstellen zu können, müssen ihnen zunächst Längen- und Breitengrade zugewiesen werden. Falls diese Koordinaten nicht bekannt sind, kann das Tool „Geolocation completion“ mit den Verfeinerungsoptionen „Place Selection“, „Map selection“ und „Map search“ bei der Ermittlung helfen. Durch Klicken auf „Geolocation completion“ im Bereich „Add geocoordinates“ werden automatisch alle (bzw. alle zuvor ausgewählten) Felder in den Spalten „Longitude“, „Latitude“ und „GettyID“ für alle Orte in der Spalte „Address“ des Datasheets ermittelt und ergänzt. Die Box „Place selection“ zeigt Ihnen die automatisch prozessierten Orte an.

.. figure:: ../datasheet-screenshots/7-geolocation-completion.png
    :align: center

    Abb. 7: Nutzen der „Geolocation completion“


Standardgemäß wird für das automatisierte Ermitteln von Adresskoordinaten („Geokodieren“) der Getty Thesaurus of Geographic Names (TGN) genutzt. Bei dieser Suchoption werden größere Orte gefunden, kleinere Orte werden durch TGN jedoch nicht immer gefunden. Als Alternative stehen im Dropdown-Menü unter „Place selection“ zwei weitere Geokodierer (Geocoder) zur Verfügung: GeoNames und der Geocoder von OpenStreetMap (OSM), Nominatim. OSM findet oft auch kleinere Orte, die im TGN nicht verzeichnet sind, außerdem findet man mit OSM auch Straßen und Hausnummern. Zusätzlich ist OSM bei der Suche nach großen Städten oft präziser, da der relevanteste Eintrag ausgewählt wird.

Bei der Nutzung von TGN haben Sie zudem die Wahl zwischen der Geokodierung auf Basis der „Address“-Spalte oder der „GettyID“-Spalte. Der Standardwert ist „Address“. OSM und GeoNames können grundsätzlich nur basierend auf „Address“ verwendet werden.

Falls der automatisch ermittelte Ort nicht der gewünschte Ort ist, gibt es die Option, unter „Place selection“ rechts neben dem eingetragenen Ortsnamen auf den Pfeil zu klicken, wodurch sich eine Liste an anderen Orten (falls vorhanden) mit dem gleichen Namen öffnet. Bei Verwendung des TGN wird beispielsweise die Funktion „exact-match“ des `DARIAH-DE Normdatendienstes <https://wiki.de.dariah.eu/display/publicde/DARIAH-DE+Normdatendienste#DARIAH-DENormdatendienste-ThesaurusofGographicalNames(Getty)>`_ genutzt, der auf den Thesaurus of Geographical Names zugreift. Werden hierbei mehrere Einträge gefunden, werden jeweils die Koordinaten des ersten Treffers in das Datasheet eingetragen. Bereits existierende Koordinaten werden dabei allerdings nicht automatisch überschrieben. Sind mehrere Treffer vorhanden, werden sie in der Dropdown-Liste vermerkt. Nachdem der gewünschte Ort manuell aus der Liste gewählt wurde, können durch Klicken von „Set“ die zugehörigen Latitude/Longitude-Koordinaten (und im Falle von TGN basierend auf „Address“ auch die GettyID) in das Datasheet übernommen werden. Duch Klick auf „Set“ werden existierende Koordinaten überschrieben. Der Inhalt des „Address“-Feldes selbst wird jedoch nicht verändert - mit der einzigen Ausnahme von TGN-Geokodierung basierend auf „GettyID“. In diesem Fall können Adressen automatisch in die Tabelle eingetragen werden, sofern gültige GettyIDs vorhanden sind und die zugehörigen „Address“-Felder noch leer sind.

Ist der gewünschte Ort nicht in der Liste aufgeführt, kann durch Klick auf „Map“ direkt daneben der Ort in die Karte der „Map selection“ übernommen werden. Unterhalb der Karte gibt es die Möglichkeit, über den Getty Thesaurus of Geographical Names (TGN), OpenStreetMap (OSM) oder GeoNames nach der entsprechenden Addresse zu suchen. Durch Klicken auf „Set“ oberhalb der Karte werden die Koordinaten im Datasheet für den gewählten Ort aktualisiert (d.h. überschrieben, falls bereits vorhanden). Falls Sie mehrere Orte mit gleichem Namen im „Address“-Feld haben und nur bestimmte davon aktualisieren wollen, selektieren Sie die betreffenden Zeilen zunächst in der Tabelle. Vorsicht: Der Ortsname im Textfeld neben „Set“ muss exakt so geschrieben sein wie der Ort im Feld „Address“, für den die Koordinaten eingetragen werden sollen!

.. figure:: ../datasheet-screenshots/8-manual-search.png
    :align: center

    Abb. 8: Manuelle Ort- und Kartenauswahl


Eine weitere Option ist es, die Flagge auf der Karte der „Map selection“ per Maus zu verschieben, um die Koordinaten zu präzisieren. Durch Zoomen in der Karte ist es möglich, auch Straßennamen kleinerer Straßen zu erfassen. Eine manuelle Suche per TGN, OSM und GeoNames ist im Suchfeld unterhalb der Karte ebenfalls möglich. Bei Verwendung des TGN kann hier auch gezielt nach einer GettyID gesucht werden; mit OSM und GeoNames können jedoch keine GettyIDs erkannt oder zurückgeliefert werden.

.. figure:: ../datasheet-screenshots/9-current-table-view.png
    :align: center

    Abb. 9: Die komplettierte Tabellenansicht


DARIAH-DE OwnStorage
^^^^^^^^^^^^^^^^^^^^

Ihre Datensätze im OwnStorage sind zunächst nur für Sie zugänglich. Sie können Ihre Datensätze lesen, schreiben, löschen und teilen.

.. figure:: ../datasheet-screenshots/10-share-dataset.png
    :align: center

    Abb. 10: Teilen des Datensatzes mit der Welt


Wenn Sie Ihren Datensatz mit andern teilen wollen, können Sie dies über die Box „Manage your data“ und den Knopf „Share dataset“ tun. Der Datensatz, der gerade in der Tabelle angezeigt wird, wird dann für jede und jeden zum Lesen freigegeben, den Datensatz bearbeiten können weiterhin nur Sie. Der Datensatz kann jederzeit von Ihnen mit der „unshare“-Funktion wieder privat geschaltet werden. Sie können den Datensatz durch Klicken auf „Delete dataset“ unwiederbringlich aus dem OwnStorage löschen. Das Teilen eines Datensatzes zum gemeinsamen Bearbeiten ist zur Zeit nicht möglich.

.. figure:: ../datasheet-screenshots/11-shared-dataset.png
    :align: center

    Abb. 11: Datensatz wurde geteilt


Durch Klicken auf „Create new dataset“ im Table View kann ein neuer Datensatz angelegt werden, der aktuell angezeigte bleibt erhalten und kann mit seiner ID bzw. URL wieder aufgerufen werden.

.. figure:: ../datasheet-screenshots/12-create-new-dataset-in-table-view.png
    :align: center

    Abb. 12: Erstellen eines neuen Datensatzes aus dem Table View


Datenzugriff
^^^^^^^^^^^^^^^^

Sobald ein neues Worksheet erstellt wird, wird der Datei eine ID im DARIAH-Storage Service zugeteilt. Über die ID lässt sich das Worksheet wieder aufrufen.

.. figure:: ../datasheet-screenshots/14-search-for-local-csv-files.png
    :align: center

    Abb. 13: Download- und Lösch-Optionen für eine lokale CSV-Datei


Beim Klick auf „Download CSV“ lässt sich die Datei als CSV-Datei herunterladen und im lokalen Speicher speichern. Außerdem wird die Storage ID angezeigt und die gesamte URL kann durch Klick auf den Button „Copy URL to clipboard“ in den Zwischenspeicher kopiert werden, zwecks direkter Adressierung der Datei im DARIAH-Storage.

Ein Datensatz kann durch Klicken auf “Delete dataset“ in the “Manage your data“ gelöscht werden.

.. figure:: ../datasheet-screenshots/15-delete-dataset.png
    :align: center

    Abb. 14: Löschen eines Datensatzes


Unter diesem Punkt können die in das Datasheet eingegebenen Daten mit dem Geo-Browser geöffnet werden. Dort werden die eingegebenen Orte als Punkte auf der Weltkarte angezeigt. Die Daten werden ebenfalls unter der Weltkarte in einer Liste angezeigt. Haben Sie Ihren Datensatz geteilt, kann jede und jeder die Darstellung Ihrer Daten im Geo-Browser sehen, andernfalls haben nur Sie Zugriff.

.. figure:: ../datasheet-screenshots/16-open-with-geo-browser.png
    :align: center

    Abb. 15: Öffnen des Datasheets mit dem Geo-Browser


Sobald Sie Einträge in der Tabelle geändert haben, werden diese in Ihrem Datasheet im OwnStorage gespeichert. Folgende Daten können Sie eintragen:

    Mit **Name** können Sie Ihr Datum benennen. Orte müssen jedoch im Feld „Address“ eingetragen werden, „Name“ kann genutzt werden, um den Namen eines Ortes oder eines Ereignisses darzustellen, der nicht als Adresse genutzt werden soll oder kann.

    **Address** identifiziert den Ort und wird genutzt, um die Koordinaten in den „Longitude“- und „Latitude“-Feldern per “Geolocation completion“ zu füllen.

    **Description** wird für die weitere Beschreibung und die Darstellung zusätzlicher Daten genutzt.

    **Longitude** kann per „Geolocation completion“ oder manuell ausgefüllt werden und nutzt dezimale geographische Koordinaten. Bereits eingetragene Koordinaten werden durch „Geolocation completion“ nicht überschrieben.

    **Latitude** kann per „Geolocation completion“ oder manuell ausgefüllt werden und nutzt dezimale geographische Koordinaten. Bereits eingetragene Koordinaten werden durch „Geolocation completion“ nicht überschrieben.

    **TimeStamp** wird genutzt, um den Zeitpunkt eines Eintrags zu definieren. Einige Beispiele für dieses Feld wären etwa *1970*, *1970-12*, *1970-12-10*, and *1970-12-10T20:15:00*. Weitere Informationen über nutzbare Zeitformate finden Sie unter `xsd:date <http://books.xmlschemata.org/relaxng/ch19-77041.html>`__, `xsd:dateTime <http://books.xmlschemata.org/relaxng/ch19-77049.html>`__, `xsd:gYear <http://books.xmlschemata.org/relaxng/ch19-77127.html>`__ und `xsdGYearMonth <http://books.xmlschemata.org/relaxng/ch19-77135.html>`__.

    **TimeSpan:begin** und **TimeSpan:end** kann für die Angabe von Beginn und Ende einer Zeitspanne genutzt werden. Beispiele für dieses Feld wären ebenso *1970*, *1970-12*, *1970-12-10*, and *1970-12-10T20:15:00*. Weitere Informationen über nutzbare Zeitformate finden Sie unter `xsd:date <http://books.xmlschemata.org/relaxng/ch19-77041.html>`__, `xsd:dateTime <http://books.xmlschemata.org/relaxng/ch19-77049.html>`__, `xsd:gYear <http://books.xmlschemata.org/relaxng/ch19-77127.html>`__ und `xsdGYearMonth <http://books.xmlschemata.org/relaxng/ch19-77135.html>`__.

    Die **GettyID** wird für die Identifizierung von Orten genutzt, die automatisch bei Nutzung des Getty Thesaurus of Geographic Names via „Geolocation completion“ eingetragen wird. Eine existierende GettyID wird nicht durch  „Geolocation completion“ überschrieben.

Die Beschreibung dieser Variablen kann durch Klicken des „How to fill the table“-Buttons rechts vom Datasheet im Datasheet Editor geöffnet werden. Es müssen nicht alle Felder ausgefüllt werden, um ein Datasheet zu erstellen.

.. figure:: ../datasheet-screenshots/17-how-to-fill-the-table.png
    :align: center

    Abb. 16: Hilfe ist unterwegs -- Wie die Tabelle ausgefüllt wird


Neue vs. alte Datensätze im Datasheet Editor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Das Format der IDs der Datensätze hat sich leicht geändert, anstatt *721203* haben die IDs nun das Format *EAEA0-7A23-E68F-EBC2-0*. Der Hostname und Pfad von „alten“ Datensätzen bleibt https://geobrowser.de.dariah.eu/storage/, neuer Hostname und Pfad ist nun https://cdstar.de.dariah.eu/dariah/.

Alle „alten“ Datensatz-Referenzen bleiben erhalten, wie etwa https://geobrowser.de.dariah.eu/storage/721203 und sind ab sofort nur noch lesbar und nicht mehr editierbar. Sofern Sie einen alten Datensatz nicht mehr editieren wollen, können Sie alles lassen, wie es ist. Andernfalls können Sie diesen in den neuen OwnStorage migrieren und dann dort editieren und teilen.


Migration eines alten Datensatzes in den OwnStorage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

So geht's:

    1. Rufen Sie einfach eine alte Storage ID mit dem Datasheet Editor: https://geobrowser.de.dariah.eu/edit?id=721203.

    2. Der alte Datensatz wird nun nur lesbar in die Tabelle geladen und dort angezeigt. Sie können diesen nun (i) mit dem Download-Button als CSV-Datei herunterladen oder (ii) daraus einen Datensatz im OwnStorage durch Klicken von „Create new dataset“ erzeugen. Der neu erzeugte Datensatz enthält nun eine Kopie der Daten des alten Datensatzes und kann von Ihnen bearbeitet und geteilt werden. Der alte Datensatz wird nicht geändert.

    3. Ein Beispiel für einen migrierten und geteilten Datensatz finden Sie hier: https://geobrowser.de.dariah.eu/edit?id=EAEA0-7FCA-A72C-9B3C-0.


Open with Geo-Browser
^^^^^^^^^^^^^^^^^^^^^

Unter diesem Punkt können die in das Datasheet eingegebenen Daten mit dem Geo-Browser geöffnet werden. Dort werden die eingegebenen Orte als Punkte auf der Weltkarte angezeigt. Die Daten werden ebenfalls unter der Weltkarte in einer Liste angezeigt. Haben Sie Ihren Datensatz geteilt, kann jede und jeder die Darstellung Ihrer Daten im Geo-Browser sehen, andernfalls haben nur Sie Zugriff.

Die Geo-Browser-Dokumentation finden Sie hier: :doc:`DARIAH-DE Geo-Browser <geobrowser>`.


Versionen und Releases
----------------------

Die aktuelle Version des DARIAH-DE Datasheet Editors ist immer zu erreichen unter https://geobrowser.de.dariah.eu/edit. Wir versuchen, neue Versionen abwärtskompatibel zu halten. Bitte beachten Sie, dass mit Version 3.6 einige größere Änderungen eingeführt wurden.


.. note::

    Mit der Version 3.6 des DARIAH-DE Geo-Browsers und Datasheet Editors haben sich einige Dinge geändert, die hauptsächlich die Authentifizierung, Autorisierung und den Zugriff auf die Daten betreffen.

    **AAI (Authentifizierung & Autorisierung)**: Ab Version 3.6 von Geo-Browser und Datasheet Editor wird der DARIAH-DE OwnStorage für die Speicherung der Datensätze genutzt. Dadurch wird die Nutzung von Geo-Browser und Datasheet Editor sicherer, und das Arbeiten an privaten Datensätzen wird so ermöglicht, bis diese bereit sind für ein Teilen mit der Welt. Um Datensätze im DARIAH-DE OwnStorage zu speichern, ist ein Login mit der DARIAH AAI nötig, entweder per DARIAH- oder eduGAIN-Föderations-Account. Intern benutzen Geo-Browser und Datasheet Editor den DARIAH-DE PDP und OAuth2 für die Autorisierung.

    **Zugreifbarkeit**: Die Datensätze im OwnStorage sind zunächst nur für die Besitzer zugreifbar. Nur diese können ihre Datensätze lesen, schreiben, löschen und teilen.

    **Sharing and unsharing**: Wenn Sie Ihre Datensätze mit andern teilen wollen, können Sie dies über die Box „Manage your data“ tun. Der Datensatz, der gerade in der Tabelle angezeigt wird, wird dann für jede und jeden zum Lesen freigegeben. Der Datensatz kann jederzeit von Ihnen mit der „unshare“-Funktion wieder privat geschaltet werden. Das Teilen eines Datensatzes zum gemeinsamen Bearbeiten ist zur Zeit nicht möglich.

    **Identifier und URLs**: Das Format der IDs der Datensätze hat sich leicht geändert, anstatt *721203* haben die IDs nun das Format *EAEA0-7A23-E68F-EBC2-0*. Der Hostname und Pfad von „alten“ Datensätzen bleibt https://geobrowser.de.dariah.eu/storage/. Der neue Hostname und Pfad von Datensätzen ist nun https://cdstar.de.dariah.eu/dariah/.

    **URLs**: Mit dem Identifier Ihrer Datensätze und abhängig vom Status derselben (privat oder geteilt), können Sie nun

        1. direkt auf Ihre geteilten Datensätze im DARIAH-DE OwnStorage zugreifen: https://cdstar.de.dariah.eu/dariah/EAEA0-8BAC-F1F7-FB43-0. Diese URL wird zum Speichern und Teilen eines Datensatzes genutzt. Merke: Nur geteilte Datensätze können über diese URL heruntergeladen werden!

        2. Ihre Datensätze im Datasheet Editor anzeigen und editieren: https://geobrowser.de.dariah.eu/edit?id=EAEA0-8BAC-F1F7-FB43-0. Dies ist der Link zum Datasheet Editor mit geladenem Datensatz.

        3. Ihre Datensätze im Geo-Browser anzeigen, sofern Sie die Eigentümerin eines privaten Datensatzes sind oder der Datensatz geteilt wurde: https://geobrowser.de.dariah.eu?csv1=https://cdstar.de.dariah.eu/dariah/EAEA0-8BAC-F1F7-FB43-0.

    **Referenzen auf alte Datensätze**: Alle „alten“ Datensatzreferenzen bleiben erhalten, wie etwa https://geobrowser.de.dariah.eu/storage/721203 und sind ab sofort nur noch lesbar und nicht mehr editierbar. Sollten Sie einen alten Datensatz editieren wollen, können Sie diesen in den neuen OwnStorage migrieren und dann dort editieren und teilen:

        1. Rufen Sie einfach eine alte Storage ID mit dem Datasheet Editor: https://geobrowser.de.dariah.eu/edit?id=721203.

        2. Der alte Datensatz wird nun nur lesbar in die Tabelle geladen und dort angezeigt. Sie können diesen nun (i) mit dem Download-Button als CSV-Datei herunterladen oder (ii) daraus einen Datensatz im OwnStorage durch Klicken von „Create new dataset“ erzeugen. Der neu erzeugte Datensatz enthält nun eine Kopie der Daten des alten Datensatzes und kann von Ihnen bearbeitet und geteilt werden. Der alte Datensatz wird nicht geändert.


GIT Repository
--------------

    * https://gitlab.gwdg.de/dariah-de/geo-browser/-/tree/master/edit


Bug Tracking
------------

    * https://gitlab.gwdg.de/dariah-de/geo-browser/-/issues
