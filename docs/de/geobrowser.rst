.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


=========================
Der DARIAH-DE Geo-Browser
=========================

Der `DARIAH-DE Geo-Browser <https://geobrowser.de.dariah.eu>`__ ermöglicht eine vergleichende Visualisierung mehrerer Anfragen und unterstützt die Darstellung von Daten und deren Visualisierung in einer Korrelation von geographischen Raumverhältnissen zu entsprechenden Zeitpunkten und -abläufen. Hierdurch können Forscherinnen und Forscher Raum-Zeit-Relationen von Daten- und Quellensammlungen analysieren und zugleich Korrelationen zwischen diesen herstellen. Hier findet sich ein `Beispiel-Datensatz <https://geobrowser.de.dariah.eu/?kml1=http://geobrowser.de.dariah.eu/data/schiller.kml&kml2=http://geobrowser.de.dariah.eu/data/goethe.kml>`__ für den Geo-Browser, in dem Publikationszeiten und -Orte von Veröffentlichungen Friedrich Schillers und Johann Wolfgang von Goethes gegeneinander aufgetragen sind.

Der DARIAH-DE Geo-Browser ist unter https://geobrowser.de.dariah.eu im Internet abrufbar. Alle Funktionen sind ohne Anmeldung frei zugänglich. Der Geo-Browser ist ein Tool zur Visualisierung und Analyse von Raum-Zeit-Relationen für Geisteswissenschaftler. Der Browser zeigt eine Karte der Welt. Durch Scrollen lässt sich in die Karte hinein- oder herauszoomen. Links gibt es eine Leiste, auf der man durch Ziehen Richtung „+“ oder „-“ ebenfalls hinein- oder herauszoomen kann.

.. figure:: ../geobrowser-screenshots/1-geobrowser-start-screen.png
    :align: center

    Abb. 1: Der Geo-Browser Startbildschirm


Weitere Informationen zum Geo-Browser und zum Datasheet Editor finden Sie im `DARIAH-DE-Portal <https://de.dariah.eu/geobrowser>`__


GUI-Dokumentation
-----------------

Menü und Navigation
^^^^^^^^^^^^^^^^^^^

Mithilfe des Menüs erreicht man direkt den Datasheet Editor sowie weitere DARIAH Webseiten und Dienste. Über „Help“ haben Sie weiterhin Zugriff auf Information und Dokumentation zum DARIAH-DE Geo-Browser, die FAQ-Seite eingeschlossen. Über „Actions“ kann der Geo-Browser zurückgesetzt werden, so dass alle momentan dargestellten Datasets aus der Ansicht entfernt werden und außerdem die Karte zurückgesetzt wird. Mit „Login“ oder „Logout“ können Sie sich ein- (oder aus)loggen für den Fall, dass Sie sich private Datasets anzeigen lassen wollen.

Load data
^^^^^^^^^

Im oberen Bereich kann man auswählen, welche Art von Daten in den Geo-Browser geladen werden. Unter „Static data“ lassen sich Beispieldaten auswählen. Diese sind aus dem WebOPAC Göttingen, von `Flickr <https://www.flickr.com/>`__, aus der `Internet Movie Database <https://www.imdb.com/>`__ und von `Wikidata <https://www.wikidata.org/wiki/Wikidata:Main_Page>`__ extrahiert und für den Geo-Browser aufgearbeitet worden.

Static Flickr Data verwendet Daten von Flickr. Hier sind Ort und Zeit der Aufnahmen von Nutzern gespeichert. (Beispielsuche: Konzerte einer bestimmten Band), „Political Data“ enthält politische Beispieldaten. Static IMDB-Data verwendet Daten der „Internet Movie Database“ und extrahiert die Metadaten Zeit und Ort, an dem der Film jeweils produziert wurde. „DBPedia Queries“ ist eine Sammlung von strukturierter Information der Plattform Wikipedia (https://wiki.dbpedia.org/About).

.. figure:: ../geobrowser-screenshots/2-static-data-museums.png
    :align: center

    Abb. 2: Laden lokaler Daten - Museen


Unter „KML file URL“ kann man über die URL eine KML-Datei hochladen. Eine KML-Datei kann zum Beispiel mit der `GoogleMaps-Engine <https://mapsengine.google.com/map/?hl=de>`__ erstellt werden. Hier kann eine Karte erstellt werden, die dann heruntergeladen werden kann. Die heruntergeladene Datei hat das Format KMZ, was z. B. mit `7-zip <https://www.7-zip.org/>`__ zu einer KML-Datei konvertiert werden, oder auch direkt als KMZ-Datei hochgeladen werden kann.

Es können auch KML-Dateien von der eigenen Festplatte hochgeladen werden, dazu muss „local KML file“ ausgewählt werden.

Das gleiche kann über „CSV file URL“ auch mit CSV-Dateien gemacht werden. Anmerkung: CSV-Dateien können unter Windows zum Beispiel mit Microsoft Excel, OpenOffice Calc oder unter Mac OS X auch mit Numbers erstellt werden.

Werden CSV-Dateien aus dem DARIAH-DE Storage (https://cdstar.de.dariah.eu/dariah/) geladen, so sind diese Dateien entweder öffentlich zugänglich und werden im Geo-Browser ohne weiteres angezeigt (d. h. sie wurden vom Datasheet Editor aus freigeschaltet) oder sie sind privat. Im letzteren Fall erfolgt entweder eine Weiterleitung auf das DARIAH-AAI-Login, oder die Nutzerin ist bereits eingeloggt - beispielsweise im Datasheet Editor -, so dass die Datei ebenfalls angezeigt werden kann. Private Dateien werden nur authentifizierten Nutzerinnen angezeigt, die Besitzerinnen dieser Datei sind.

Die geladenen Daten werden in bunten Kreisen auf der Karte dargestellt. Durch Doppelklick auf einen bunten Kreis werden die Kategorie und Anzahl der Treffer für diesen bestimmten Bereich angezeigt.

.. figure:: ../geobrowser-screenshots/3-double-click-a-circle.png
    :align: center

    Abb. 3:  Anzeige von Kategorie und Anzahl der Treffer für einen bestimmten Bereich


Werden unterschiedliche Kategorien geladen, wird jede Kategorie in einer anderen Farbe angezeigt. Unten rechts in der Karte erscheint dann eine Liste mit Farben und den jeweiligen zugeordneten Kategorien.


Load Overlay
^^^^^^^^^^^^

Im oberen Fenster unter „Load Overlay“ muss der Datentyp der Datei bestimmt werden, der als Overlay geladen werden soll. Ein Overlay ist eine grafische Karte, die sich über die tatsächliche Karte legt. Das heißt, wenn Orte aus einer aktuellen Karte geladen sind und ein Overlay einer historischen Karte geladen wird, sind die Orte dort, wo sie heute sind und man kann sehen, wo dieser Ort zum Beispiel im 19 Jhd. lag. Hier können ebenfalls URLs von KML/KMZ-Dateien geladen werden, mit Auswahl der Option „KML File URL“ bzw. „KMZ File URL“.

Unter „ArcGIS WMS“ kann eine WMS-Server-URL und ein Layer angegeben werden, welches angezeigt werden soll.

Das „XYZ Layer“ hat neben den Informationen Breiten- und Längengrad noch die „thickness“. Hier kann eine URL mit x/y/z Platzhaltern eingetragen werden. (Das Format ist dasselbe, wie es in OpenLayers verwendet wird). „Roman Empire“ lädt eine Karte aus dem römischen Reich.


Dataset Information
^^^^^^^^^^^^^^^^^^^

**Magnetic link**: Sobald Daten in den Geo-Browser geladen wurden, kann man diese im Bereich „Dataset“ als „Magnetic link“ öffnen, sofern es sich um online-Daten handelt, lokale CSV- und KML-Dateien können nicht als Magnetic Link zur Verfügung gestellt werden. Dieser Link öffnet dann immer wieder den momentan angezeigten Stand von Karte und Datasets. Beim Klick auf „Magnetic Link“ öffnet sich ein neuer Tab im Browser.

**Download**: Ein Klick auf den Pfeil nach oben rechts neben dem Dateinamen lädt die jeweiligen Datei auf Ihren Rechner. Die Datei muss dazu öffentlich oder aber für Sie als Besitzer im DARIAH-DE Storage lesbar sein.

.. figure:: ../geobrowser-screenshots/4-magnetic-link-and-download.png
    :align: center

    Abb. 4: Magnetic link und Download


**Upload to DARIAH-DE Storage**: Per [upload to DARIAH-DE Storage] können Sie Ihre lokale CSV-Datei im DARIAH-DE Storage speichern und gleich öffentlich teilen. Ihre Datei erhält eine ID und ist so über den Magnetic Link auch für andere sichtbar.

.. figure:: ../geobrowser-screenshots/5-upload-to-dariah-storage.png
    :align: center

    Abb. 5: Speichern einer lokalen CSV-Datei im DARIAH-DE Storage


**Open datasheet**: Mit [open datasheet] können CSV-Dateien direkt im Datasheet Editor geöffnet werden.

.. figure:: ../geobrowser-screenshots/6-open-datasheet.png
    :align: center

    Abb. 6: Öffnen einer CSV-Datei direkt im Datasheet Editor


Background Map
^^^^^^^^^^^^^^

Es gibt mehrere Optionen für Karten im Hintergrund. Es gibt eine aktuelle Karte „Contemporary Map (2010)“, es gibt historische Karten, die älteste ist von 2000 v. Chr, es gibt eine Karte des „Barrington Roman Empire“, eine Karte der Antike aus den Jahren zwischen 550 v. Chr. und 640 n. Chr. Als Default ist eine Karte von Open Street Map (MapQuest) eingestellt, hier werden neben Straßen und Staatsgrenzen auch Gebirge, Vegetation und Wüsten angezeigt.


Map Selector Tools
^^^^^^^^^^^^^^^^^^

.. figure:: ../images/mapselectortools.png

Mit den Map Selector Tools ist es möglich, eine geometrische Form auf der Karte zu markieren. Durch Klicken auf die Form kann danach die entsprechende Form auf der Karte markiert werden. Es gibt ein Rechteck, einen Kreis, eine flexible Form mit der Möglichkeit, Punkte auf der Karte zu setzen, die verbunden werden. Dazu müssen die Punkte jeweils angeklickt werden. Wenn die Form fertig ist, kann man den Bereich durch Doppelklick auf den letzten Punkt fixieren. Sobald ein Bereich ausgewählt wurde, sind die Punkte in dem Bereich in der Auflistung der Ergebnisse markiert.

.. figure:: ../geobrowser-screenshots/7-map-selector-circle.png
    :align: center

    Abb. 7: Map Selector Tools – Auswahl eines Kreisbereichs


Es gibt außerdem die Möglichkeit, Staaten zu markieren, was allerdings momentan nur bei den historischen Karten funktioniert.


Filter
^^^^^^

.. figure:: ../images/filterminus.png

Mit dem Filter können Daten in einem mit den Map Selector Tools markierten Bereich ausgefiltert werden. Sie sind dann nicht mehr im Geo-Browser sichtbar.


Publication Place
^^^^^^^^^^^^^^^^^

„Publication Place“ zeigt an, wieviele Ergebnisse es an unterschiedlichen Orten für die Suchanfrage gibt.


Liste unter der Weltkarte
^^^^^^^^^^^^^^^^^^^^^^^^^

Wenn Daten in den Geo-Browser geladen werden, erscheint zusätzlich in einem Feld unter der Weltkarte eine Grafik, die den zeitlichen Verlauf der Daten anzeigt, und darunter eine Auflistung der Daten.

.. figure:: ../images/timestart.png

Unter „Time start“ lässt sich der Zeitpunkt auswählen, an dem die Grafik beginnen soll.

.. figure:: ../images/timeunit.png

Unter „Time unit“ lassen sich die Zeiteinheiten auswählen. Diese reichen von „continuous“ bis zu 500 Jahren. Die ausgewählte Einheit wird jeweils in Kästchen angezeigt. Im Auswahlfenster „Scaling“ kann die Skalierung des Diagramms verändert werden. Es gibt neben „normal“ die Optionen „logarithm“ und „percentage“.

.. figure:: ../geobrowser-screenshots/8-the-time-line.png
    :align: center

    Abb. 8: Die Timeline und die Liste der Ergebnisse (Franz Kafka OPAC-Daten)


Animation
^^^^^^^^^

Um eine Animation zu starten, muss zuerst auf der Timeline ein bestimmter Bereich ausgewählt werden. Mit Klick auf die Grafik lässt sich ein Zeitfenster markieren.

.. figure:: ../images/animation.png

Beim Klick auf den Play- Button läuft der markierte Bereich einmal von links nach rechts über die Grafik und auf der Karte blinken jeweils die Punkte auf, die mit einem bestimmten Zeitpunkt verknüpft sind. Dies kann zu Demonstrationszwecken verwendet werden.

.. figure:: ../images/schliessen.png

Unter der Grafik ist ein orangefarbenes Feld, in dem durch Klick auf  die geladenen Daten gelöscht werden können.

.. figure:: ../images/herunterladenorange.png

Außerdem können die Daten hier mit Klick als KML Datei heruntergeladen werden. Unter der Zeitleiste befindet sich eine Auflistung aller Ereignisse. Mit Klick auf

.. figure:: ../images/hochklappen.png

kann diese Ansicht hochgeklappt werden. Es kann eingestellt werden, wie viele Ergebnisse der Liste jeweils auf einer Seite angezeigt werden (10 bis 100). Durch Klick auf

.. figure:: ../images/weiter.png

kann die Seite weitergeblättert werden. Durch Klick auf

.. figure:: ../images/N.png

kann die letzte Seite der Ergebnisse angezeigt werden. Mit Klick auf

.. figure:: ../images/Bild1.png

gelangt man zurück auf die erste Seite.

Unter der Grafik wird eine Liste angezeigt, die die Beschreibung (description), den Name (name) und den Ort (place) jedes gefundenen Objekts anzeigt. Beim Klick auf die Box links neben einem Eintrag wird der Eintrag markiert. Beim Klick auf die Lupe neben dem Suchfenster wird nur noch dieser Eintrag in der Weltkarte und in der Liste angezeigt. Alle anderen Elemente werden aus der Weltkarte und aus der Liste entfernt.


Kartenmaterial
--------------

Soweit nicht anders angezeigt, wird vom Geo-Browser freies Kartenmaterial zur Verfügung gestellt, das über den `DARIAH-DE GeoServer <https://ref.de.dariah.eu/geoserver/>`__ zur Verfügung gestellt wird, z. B. wurden die historischen Karten im Rahmen des Projekts europeana4D für den Vorgänger des Geo-Browsers (`e4D <https://www.sub.uni-goettingen.de/en/projects-research/project-details/projekt/europeanaconnect-europeana4d/>`__) genutzt. Die historischen Karten vom `ThinkQuest Team C006628 <https://web.archive.org/web/20060630061554/http://library.thinkquest.org/C006628/>`__ sind „in den meisten Fällen für eine Nutzung im Großen Maßstab gedacht, so dass nur die relativen Positionen der Länder verhältnismäßig genau sind. Bei den politischen Grenzen sollte ein durchschnittlicher Fehler von 40 Meilen angenommen werden." (siehe auch `Disclaimer <https://web.archive.org/web/20060903200541/http://library.thinkquest.org/C006628/disclaimer.html>`__)

Weitere genutzte Karten werden online von anderen Anbietern/Diensten eingebunden, beispielsweise Karten von OpenStreetmap und MapQuest. Die Lizensierung der Karten findet sich jeweils im unteren Teil der Geo-Browser-Kartenansicht.

Die Karte der Römischen Imperiums (`Digital Atlas of the Roman Empire <https://https://dh.gu.se/dare/>`__) wurde von Johan Åhlfeldt mit Unterstützung des Projekts `Pelagios <https://pelagios.org/>`__ entwickelt.


Versionierung and Release
-------------------------

Die aktuelle Version des DARIAH-DE Geo-Browsers ist zu erreichen unter https://geobrowser.de.dariah.eu.

Die Änderungen in neuen Versionen sind im CHANGELOG verfügbar, das direkt im `GIT Repository des Geo-Browsers <https://projects.gwdg.de/projects/geo-browser/repository>`_ vorliegt.


Spezifizierungen für die Nutzung
--------------------------------

* :download:`KML Spezifikation <../examples/M3.3.2_eConnect_KML_Specification_v1.0_UGOE.pdf>` für das Geo-Browser KML (PDF)
* :doc:`CSV Spezifikation <csvspec>`


Tools für die Erzeugung von KML- bzw. CSV-Dateien
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* https://www.freemaptools.com/kml-file-creator.htm
* https://www.latlong.net/
* https://mapsengine.google.com/map/?hl=de


Beispieldateien
^^^^^^^^^^^^^^^

KML
    * https://steinheim-institut.de/daten/epidat.kml (`öffnen im Geo-Browser <https://geobrowser.de.dariah.eu/?kml=https://steinheim-institut.de/daten/epidat.kml>`__)
    * https://geobrowser.de.dariah.eu/data/goethe.kml (`öffnen im Geo-Browser <https://geobrowser.de.dariah.eu/?kml=https://geobrowser.de.dariah.eu/data/goethe.kml>`__)
    * https://geobrowser.de.dariah.eu/data/rechtegewalt.kml (`öffnen im Geo-Browser <https://geobrowser.de.dariah.eu/?kml=https://geobrowser.de.dariah.eu/data/rechtegewalt.kml>`__)

CSV
    * :download:`Universal-Kirchenzeitung-Nachrichten-aus-der-Israelitschen-Abteilung.csv <../examples/Universal-Kirchenzeitung-Nachrichten-aus-der-Israelitschen-Abteilung.csv>`


GIT Repository
--------------

DARIAH-DE Geo-Browser Repository
    * https://gitlab.gwdg.de/dariah-de/geo-browser/-/tree/master

GeoTemCo
    * https://github.com/stjaenicke/GeoTemCo

PLATIN-Fork vom DARIAH-DE Geo-Browser (hauptsächlich angepasste Konfiguration)
    * https://github.com/DARIAH-DE/PLATIN


Bugtracking
-----------

    * https://gitlab.gwdg.de/dariah-de/geo-browser/-/issues
