.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


=================
CSV Spezifikation
=================


Eine CSV-Datei speichert Tabellendaten in einfachem Text und trennt die einzelnen Datenfelder mit einem Komma. Jede Zeile repräsentiert einen Datenpunkt mit Informationen in den durch Kommata getrennten Datenfeldern (Spalten). Die Erste Zeile der Datasheet Editor CSV-Datei definiert die Felder, indem sie in folgender Reihenfolge aufgelistet werden:

::

    "Name","Address","Description","Longitude","Latitude","TimeStamp","TimeSpan:begin","TimeSpan:end","GettyID"


Die Bedeutung der Datenfelder im Einzelnen sind:

* „Name“ Ermöglicht die Benennung eines Ortes. Da der Ort bei „Address“ eingegeben werden muss, kann hier beispielsweise der Name des dokumentierten Ereignisses oder der Name eines Ortes, der nicht als Adresse erkannt wird, eingetragen werden.

* „Address“ identifiziert die Adresse eines Ortes und ermöglicht das automatische Einfügen der Koordinaten in „Longitude“ and „Latitude” über „Geolocation completion“.

* „Description“ kann genutzt werden, um zusätzliche Informationen zu speichern. Hier darf auch HTML-Code genutzt werden, so dass die Ausgabe in der Liste des Geo-Browsers hier strukturiert und formatiert werden kann.

* „Longitude“ und „Latitude” werden genutzt, um Einträge räumlich im Geo-Browser darzustellen. Diese können manuell eingetragen oder mit Hilfe von „Geolocation completion“ automatisch eingetragen werden. Sie enthalten geographische Koordinaten in Dezimalgrad. Existierende Koordinaten werden nicht von „Geolocation completion“ überschrieben.

* „TimeStamp“ speichert das Datum eines Eintrags und benutzt JJJJ-MM-TT als Format. Monat und/oder Tag sind jedoch nicht erforderlich.

* „TimeSpan:begin“ und „TimeSpan:end“ können genutzt werden, um eine Zeitspanne abzubilden. Wie auch bei „TimeStamp“ wird JJJJ-MM-TT genutzt.

* „GettyID“ wird bei „Geolocation completion“ automatisch vom „Getty Thesaurus of Geographic Names“ abgefragt und in das Feld eingetragen. Eine existierende GettyID wird nicht von „Geolocation completion“ überschrieben.

Alle folgenden Zeilen enthalten Informationen in der Position dieser Kategorien. Datenfelder ohne eingetragene Informationen werden als "" gezeigt:

::

    "","Berlin","","13.4167","52.5333","2018","","","7003712"

In diesem Beispiel sind die Datenfelder „Name“, „Description“ sowie die beiden „TimeSpan“ Felder leer. Zeilen wie diese können im Geo-Browser genutzt werden, da nur die Felder „Longitude“, „Latitude“ und „Address“ zwingend erforderlich sind für eine Visualisierung des Ortes. Um Raum-Zeit-Relationen zu visualisieren, müssen entweder das Feld „TimeStamp“ oder die Felder „TimeSpan:begin“ und „TimeSpan:end“ vorhanden sein.

Der Datasheet Editor erstellt immer automatisch 40 Zeilen. Ungenutzte Spalten bleiben leer:

::

    "","","","","","","","","",""

Diese Zeilen können auch gelöscht oder gar nicht erst erzeugt werden.

Zusätzliche Felder können in die Tabelle aufgenommen werden, diese werden möglicherweise jedoch nicht vom Geo-Browser in der Tabellenansicht dargestellt.
