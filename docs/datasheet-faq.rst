.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


====================
Datasheet Editor FAQ
====================


.. topic:: Why can Geolocation Completion not find the places I added to the table?

    The names of places have to be added to the “Address” column and capitalized to be found via Geolocation Completion. If these two steps are followed and the location is still missing, its name might not be registered in the Getty Thesaurus of Geographic Names (TGN), which is the default geocoder to be used. Please try out one of the other available geocoders, OSM or GeoNames, in the the drop-down menu at “Place selection“.


.. topic:: Why is my Datasheet Editor data displayed incompletely in Geo-Browser?

    Some HTML-Symbols like “&” confuse the Geo-Browser. Please check your entries for special characters and replace “&” with “&amp” for example. For more information you can visit `Bug #14359 <https://projects.gwdg.de/issues/14359>`__ in our Geo-Browser-Bugtracker.


.. topic:: Is it possible to only add coordinates to certain places by using the “Set“ button?

    Yes, this feature is now available. For example, if you entered “Berlin” multiple times and want to change the coordinates for only some of them, you can select the respective rows or cells by left-clicking and holding the left mouse button until they are highlighted blue. Please make sure that the  “Address” fields are part of your selection, not just the Latitude/Longitude/GettyID fields. The easiest option is to select entire rows by left-clicking on the gray row index area (1,2,...) and extending or reducing the selection by dragging up and down. By both left-clicking and pressing CRTL on your keyboard, single rows can be added to the selection. If you then use the “Set“ button (or start the entire “Geolocation completion“ process), only the values within your selection will be considered for geolocation.


.. topic:: The automatically generated geographic data by Getty ID is partially incorrect. Why do some places appear to be up to 40 km removed from their origin, making them unusable for further examination?

    These inaccuracies are the result of matching the entered city to the coordinates of the administrative district by the same name. The displayed coordinates are therefore not wrong, but match a different definition of the given name. If Datasheet Editor finds multiple definitions of the entered name, you can change the selection at “Place Selection”. Alternatively you can also manually move the flag-marker on your map.
