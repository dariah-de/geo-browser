.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


================================
Geo-Browser and Datasheet Editor
================================


.. toctree::
   :caption: Geo-Browser

   Geo-Browser [en] <./geobrowser>
   Geo-Browser FAQ [en] <./geobrowser-faq>

   Geo-Browser [de] <./de/geobrowser>
   Geo-Browser FAQ [de] <./de/geobrowser-faq>


.. toctree::
   :caption: Datasheet Editor

   Datasheet Editor [en] <./datasheet>
   Datasheet Editor FAQ [en] <./datasheet-faq>

   Datasheet Editor [de] <./de/datasheet>
   Datasheet Editor FAQ [de] <./de/datasheet-faq>


.. toctree::
    :caption: Licenses

    Geo-Browser and Datasheet Editor <https://gitlab.gwdg.de/dariah-de/geo-browser/-/blob/master/COPYING.LESSER>
    PLATIN <https://github.com/DARIAH-DE/PLATIN/blob/master/COPYING.LESSER>


.. toctree::
   :caption: Imprint and Privacy Policy

   Imprint <https://de.dariah.eu/en_US/impressum>
   Privacy Policy <https://de.dariah.eu/en_US/privacy-policy>
