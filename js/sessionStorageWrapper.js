/*
 * Token wrapper methods.
 */
import {tkey, pkey, fkey, mkey} from "./dariah.geobro.conf.js";
export function setToken(tok) {
    sessionStorage.setItem(tkey, tok);
}

export function readToken() {
    return sessionStorage.getItem(tkey)
}

window.readToken = readToken(); //make function accessible in PLATIN

/*
 * Remove the token.
 */
export function removeToken() {
    sessionStorage.removeItem(tkey);
}

/*
 * Public flag wrapper methods.
 */
export function setPublic() {
    sessionStorage.setItem(pkey, 'true');
}

export function publicIsSet() {
    return sessionStorage.getItem(pkey) === 'true';
}

export function removePublic() {
    sessionStorage.removeItem(pkey);
}

/*
 * Migration flag wrapper methods.
 */
export function setMigration() {
    sessionStorage.setItem(mkey, 'true');
}

export function migrationIsSet() {
    return sessionStorage.getItem(mkey) === 'true';
}

export function removeMigration() {
    sessionStorage.removeItem(mkey);
}

/*
 * Files wrapper methods.
 */
export function setFiles(files) {
    sessionStorage.setItem(fkey, files);
}

export function getFiles() {
    return sessionStorage.getItem(fkey);
}

export function removeFiles() {
    sessionStorage.removeItem(fkey);
}

/*
 * Clear all storage entries.
 */
export function clearStorage() {
    sessionStorage.clear();
}
