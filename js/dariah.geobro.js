/*
 * Things to do if HTML is loaded.
 */
import {readToken, removeToken, setToken} from "./sessionStorageWrapper";
import {getAuthInfo} from "../edit/js/dariah.storage";
import {pdpURL} from "./dariah.geobro.conf";
import {host} from "../edit/js/dariah.workflow.conf";
init();

/*
 * Things to init all the Geo-Browser widgets.
 */
function init() {
    var datasets = [];
    var mapDiv = document.getElementById("mapContainerDiv");
    var map = new WidgetWrapper();
    var mapWidget = new MapWidget(map,mapDiv,{
        mapTitle: "Locations"
    });
    var timeDiv = document.getElementById("plotContainerDiv");
    var time = new WidgetWrapper();
    var timeWidget = new FuzzyTimelineWidget(time,timeDiv,{
        timeTitle: "Publication date"
    });
    timeWidget.options.timelineMode = 'stacking';
    var tableDiv = document.getElementById("tableContainerDiv");
    var table = new WidgetWrapper();
    var tableWidget = new TableWidget(table,tableDiv);
    var dataloaderDiv = document.getElementById("dataloaderContainerDiv");
    var dataloader = new WidgetWrapper();
    var dataloaderWidget = new DataloaderWidget(dataloader,dataloaderDiv);
    var overlayloaderDiv = document.getElementById("overlayloaderContainerDiv");
    var overlayloader = new WidgetWrapper();
    var overlayloaderWidget = new OverlayloaderWidget(overlayloader,overlayloaderDiv);
    overlayloaderWidget.attachMapWidget(mapWidget);
    var storytellingDiv = document.getElementById("storytellingContainerDiv");
    var storytelling = new WidgetWrapper();
    var storytellingWidget = new StorytellingWidget(storytelling,storytellingDiv, { dariahStorage: true });
    dataloaderWidget.loadFromURL();
}

/**
 * Stuff to setup when DOM is ready.
 */
$(document).ready(function() {
    // Set token first if token hash (#access_token=...) is existing.
    if (window.location.hash) {
        setTokenGB();
    }
    checkLoginLogoutMenuGB();
});

/*
 * Check and fill login/logout menu.
 */
function checkLoginLogoutMenuGB() {

    if (readToken() != null) {
        getAuthInfo();
        $('#loggedin').off('click').on('click', function(){logoutGB();});
    } else {
        $('#loggedin').append('<i class="icon-signin icon-white"></i> Login');
        $('#loggedin').off('click').on('click', function(){loginGB()});
    }
}

/**
 * Redirect to PDP and login.
 *
 * NOTE Keep appended csv and kml params for GB!
 */
function loginGB() {
    window.location = pdpURL + host + window.location.pathname + window.location.search;
}

/**
 * Logout: Just delete the token from session store and go to index page.
 *
 * NOTE Keep appended csv and kml params for GB!
 */
function logoutGB() {
    removeToken();
    window.location = window.location.origin + window.location.pathname + window.location.search;
}

/*
 * Sets the OAuth token from token hash, if existing.
 */
function setTokenGB() {
    // Get complete token hash from PDP.
    var acctok = window.location.hash.substr(1);
    // Get pure token out of complete token hash.
    var puretok = acctok.substr(acctok.indexOf("access_token=") + 13, acctok.indexOf("&") - 13);
    setToken(puretok);
    // Remove token hash from URL.
    window.location = window.location.origin + window.location.pathname + window.location.search;
}
