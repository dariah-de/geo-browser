/**
 * Common configuration for basic Datasheet Editor and Geo-Browser settings.
 */
// The DARIAH-DE OwnStorage endpoint.
export const storageURL = STORAGE_URL;

// The DARIAH-DE old OpenStorage endpoint.
export const oldStorageURL = 'https://geobrowser.de.dariah.eu/storage/';

// The DARIAH-DE PDP URL.
export const pdpURL = PDP_URL;

// The DARIAH status URL (for service maintenance warnings etc.)
export const dariahSTATUSURL = DARIAH_STATUS_URL;

// Token, public, migration, and files key string.
export const tkey = 'tok';
export const pkey = 'public';
export const mkey = 'migration';
export const fkey = 'files';
