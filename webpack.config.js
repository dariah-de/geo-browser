const path = require('path');
const glob = require("glob");
const CopyWebpackPlugin = require('copy-webpack-plugin');
module.exports = {
    resolve: {
        symlinks: false
    },
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: ["css-loader"],
            },
        ],
    },

    entry: {
        "main": [ './js/dariah.geobro.conf.js', './js/sessionStorageWrapper.js', './js/matomo.js',
            './edit/js/dariah.utils.js', './edit/js/dariah.workflow.conf.js', './edit/js/dariah.storage.js',
            './js/dariah.geobro.js'],
        "edit": ['./js/dariah.geobro.conf.js', './js/sessionStorageWrapper.js'].concat(glob.sync("./edit/js/*.js")),
    },
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, 'dist'),
        clean: true,
    },
    plugins: [
        new CopyWebpackPlugin({
            patterns: [
                { from: "./PLATIN/css", to: path.resolve(__dirname, './dist/PLATIN/css') },
                { from: "./PLATIN/platin.js", to: path.resolve(__dirname, './dist/PLATIN/platin.js') },
                { from: "./PLATIN/lib", to: path.resolve(__dirname, './dist/PLATIN/lib') },
                { from: "./PLATIN/data", to: path.resolve(__dirname, './dist/PLATIN/data') },
                { from: "./PLATIN/images", to: path.resolve(__dirname, './dist/PLATIN/images') },
                { from: "./PLATIN/php", to: path.resolve(__dirname, './dist/PLATIN/php') },
                { from: "./css", to: path.resolve(__dirname, './dist/css') },
                { from: "./edit/css", to: path.resolve(__dirname, './dist/edit/css') },
                { from: "./edit/img", to: path.resolve(__dirname, './dist/edit/img') },
            ]
        }),
    ]
}
