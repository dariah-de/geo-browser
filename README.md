# The DARIAH-DE Geo-Browser and Datasheet Editor

## Install DARIAH-DE Geo-Browser und Datasheet Editor

1. Check out Geo-Browser main folder

        git clone https://gitlab.gwdg.de/dariah-de/geo-browser.git
        cd geo-browser

2. Check out the PLATIN code (from DARIAH-DE PLATIN fork)

        git clone https://github.com/DARIAH-DE/PLATIN.git

3. (OPTIONAL) If you want to build the docs as well (requires Python):

        cd docs
        pip install -r requirements.txt
        make clean html

    and then copy `_build/html` into a new directory `/doc` in the same directory as `index.html` on your server.

4. Run and debug Geo-Browser locally (this already includes building PLATIN):

        npm install
        npm start

5. Enjoy your new Geo-Browser installation!
