# RELEASE.md

## Deployment Workflow

### Release PLATIN first (only if changed)

For a new Geo-Browser release, make sure that the [DARIAH-DE PLATIN Library](https://github.com/DARIAH-DE/PLATIN) is up-to-date. I.e., for an actual release, merge the updated `develop` branch into the `main` branch (only if changed at all!). For a BETA release, PLATIN's `develop`  branch will be checked out by the Pipeline, so if you have any separate new feature branches on PLATIN, merge them into `develop`. In PLATIN, the URL to the storage will be determined automatically depending on the URL of the application.

### Release Geo-Browser

* Update the [CHANGELOG.md](./CHANGELOG.md) to the new RELEASE version

      git commit -m "Update changelog for new RELEASE version 3.6.9"
      git push origin develop 

* Update the version number in **package.json** by typing **one** of these commands, depending on the type of change:

      npm version patch # e.g. 3.6.8 --> 3.6.9
      npm version minor # e.g. 3.6.8 --> 3.7.0
      npm version major # e.g. 3.6.8 --> 4.0.0

  This will increase the version automatically and create a version commit and tag. Afterwards, just do:

      git push --follow-tags

The tag will trigger the `develop` (BETA) pipeline on Gitlab CI and deploy this version on the BETA server.

Once this has been finished successfully, you can RELEASE the same version on the live server. All URL updates / version names will be handled automatically. You just need to trigger the `main` (RELEASE) pipeline by merging `develop` into `main`.
