# CHANGELOG

## Versin 4.1.0

* Add table handling for CSV files: Do not show rows with an empty header and do not show rows with no content BUT header

## Version 4.0.31

* Add enhanced readable text highlighting and text-shadow for highlighted fonts

## Version 4.0.28

* Add new footer to Datasheet Editor

## Version 4.0.27

* Add links to logo icons

## Version 4.0.23

* Add logo icons

## Version 4.0.22

* Remove Stamen maps

## Version 4.0.20

* Disable Mastomo cookie tracking

## Bugfix Version 4.0.15

* Add documentation for changing file suffixes of KML files to ".xml" to get processed (fixes #97)
* Add correct URL encoding for URL params in dataset URLs (fixes #98)

## Bugfix Version 4.0.8

* Remove outdated Readme
* Validate empty/deleted timestamp values as true

## Bugfix Version 4.0.7

* "/index.html" no longer necessary to recognize DatasheetEditor per URL
* Fixes bug #89

## Bugfix Version 4.0.6

* Change _window.hostname_ to _window.host_ to include the correct port automatically

## Bugfix Version 4.0.5

* Add main deploy stage
* Update documentation

## Bugfix Version 4.0.4

* Fix embed version

## Bugfix Version 4.0.3

* Pipeline bug fixes and error handling
* Change default basemap to terrain

## Bugfix Version 4.0.2

* Somehow fix proxy.php issue (though I don't know how I did it :-)

## Major Version 4.0.0

Internal major changes:

* Introduce Webpack for versioning/release workflow handling
* Modularize Datasheet Editor
* Add Stamen basemaps

## Bugfix Version 3.6.12

* Re-introduce excel MIME type to support CSVs exported from Excel

## Bugfix Version 3.6.11

* Add bugtracking URLs also to german documentation

## Bugfix Version 3.6.10 (September 16th 2021)

* Fix #81 (Datasheet nicht angezeigt)
* Change bugtracking URLs in documentation to Gitlab
* Add link to licenses to documentation

## Release Version 3.6.9 (September 14th 2021)

* Update documentation
* Fix #38: Add new version of Handsontable
* Remove lib folder and update OpenLayers in edit
* Remove bower components and fix local CSV file import
* Fix #75 (List OwnStorage Files“ leads to endless reload loop)
* Fix #72 (Place Selection for selected cells)
* Fix #65 ("add large amount of data" fixed by Handsontable update)
* Fix #55 (use GettyID for geolocation)
* Fix #54 (Handle (old) alerts, that are not valid anymore)
* Fix #51 (Import KML files)
* Fix #48 (We would need a "Set only for some places named" button)
* Fix #26 (Message when no content in Address field)
* Add options for batch-geocoding with OSM and Geonames

## Bugfix Version 3.6.8 (June 2021)

* Fix CSV upload bug if not logged in
* Refactor package management to use CDN

## Bugfix Version 3.6.7 (March 4th 2021)

* Fix #34679 (Add doc and pic for first registration with DARIAH Account)
* Fix #34684 (Check row indices in “Invalid coordinates“ error message)
* Fix #34693 (Table is not displayed in embed mode)

## Bugfix Version 3.6.6 (February 25th 2021)

* Fix #34678 (If coords undefined, write “undefined“ in KML, not “NaN“)
* Fix #34680 (Error message shall not appear if only one place is given in table)

## Bugfix Version 3.6.5 (February 24th 2021)

* Fix #34522 (Probleme mit der Funktion Share dataset)
* Fix #34666 (Refactor and wrap sessionStorage handling)
* Fix #34656 (Download buttons in Geo-Browser don't work correctly)
* Fix #34657 (Geolocation completion doesn't work properly for special chars)
* Fix #34662 (Downloading KML from table widget is not working properly)
* Fix #34667 (Correct error message if geocoordinates are invalid or missing in Datasheet Editor and datasheet shall be opened in Geo-Browser)
* Fix #34668 (Add tab name as filename for exported KML files)

## Bugfix Version 3.6.4 (January 19th 2021)

* Fix typos in documentation
* Increase doc version
* Adapt references in documentation to example data on productive OwnStorage
* Add RELEASE.md file
* (Add some monitoring probes for refs and docs and storage)

## Release Version 3.6.3 (January 14th 2021)

* Update some attribution information for map display
* Improve Geolocation completion button and visualisation
* Add spinning flower to table status bar
* Add feature OwnStorage files list (still BETA)
* Refined coordinate setting in Geolocation completion refined (coordinates are now also set, if the name found in TGN does not match exactly the one in the address field)
* Remove HTML support for IE9
* Add link to open datasheets in Datasheet Editor from Geo-Browser's magnetic link section
* Add colours to data links in Geo-Browser
* Adapt button colors to state of editor to guide users
* Fix upload to DARIAH-DE Storage (only if CSV file or convertible)
* Import button asks for permission if dataset is on display
* Adapt documentation to new version and functions
* Add support for DARIAH-DE OwnStorage and DARIAH-DE PDP
* Add WHAT IS NEW documentation
* Add OAuth2 token support
* Add many tooltips
* Fix many TGN issues
* Fix GUI bugs
* Add GeoNames for Map selection again
* Use sort of status management now
* Use gitflow for PLATIN fork now
* Fix general HTML and CSS issues
* Add login/logout menu items
* Add DARIAH-DE Status messages

## Release Version 2.7.8 (Juni 2020)

* Popup labels on mouseOver the Place circles now first take the “Name“ value, and if empty the “Address“ value. If both are not set, “unknown“ is used.
* Documentation slightly updated.

## Release Version 2.7.0 (April 2019)

* Update of the `GeoServer <https://ref.de.dariah.eu/geoserver/>`__ to version 2.13.1.
* Documentation of the CSV file specification
* English translation of the FAQ section
* Content update of documentation
* Usage of the `Sphinx framework <http://sphinx-doc.org/>`__ for generation of documentation

## Release Version 2.0.0 (Juli 2016)

* The `GeoServer <https://ref.de.dariah.eu/geoserver/>`__ that is providing the Geo-Browser with maps, has been updated to version 2.8.3.
* The continental borders of the historical maps have been corrected (GeoServer).
* Geo-Browser now does provide current EuroStat maps of 2013 and 2014 (GeoServer).
* The documentation has been translated to English.
* Licensing information is now provided directly in the map view (PLATIN).
* There is a possibility to visualise local XLS/XLSX files using "Load Data" (PLATIN).
* Under "Load Overlay" the Water Layer of Maps-For-Free can be used (PLATIN).
* The global DARIAH-DE tool menu has been integrated.
* The PIWIK of GWDG was integrated, to track website-usage and browser information (taking data privacy into account). You can deactivate the tracking at the `Privacy Policy Page <https://de.dariah.eu/en/datenschutz>`__ page, or just configure your browser accordingly.
* The usage of `OpenGeoNames <http://www.geonames.org/>`__ in the Datasheet Editor is again possible.
* The code of Geo-Browser and Datasheet Editor has been merged into a `shared code repository <https://projects.gwdg.de/projects/geo-browser/repository>`__.
* `PLATIN <https://github.com/DARIAH-DE/PLATIN>`__ has been forked on Github for the usage of Geo-Browser in DARIAH-DE.
* The static data has been added to the PLAITN fork as well.

## Release Version 1.0.0 (October 2015)

* Adapted to the DARIAH-DE Style Guide.
* Added information to documentation and FAQ.
* Update of the timeline module, so that time ranges can now be visualised, too.
