import {
    checkColumnHeadersGeobrowser,
    createInitialTableContent,
    getTable,
} from "./dariah.workflow.table";
import {
    getFiles,
    migrationIsSet,
    publicIsSet,
    readToken,
    removeFiles,
    removeMigration,
    setFiles
} from "../../js/sessionStorageWrapper";
import {checkCoordinates, initMap, resizeMap, toggleTGNOptions} from "./dariah.workflow.geo";
import {clearTableSelection} from "./dariah.workflow.table";
import {startGeocoding, setAllPlacesOSM, tgnSearch, osmSearch, ognSearch} from "./dariah.workflow.geo";
import {
    unpublishAndDeleteDataset,
    publishDataset,
    unpublishDataset,
    authenticate,
    getAuthInfo,
    loadFromDariahStorage,
    loadFromOldDariahStorage,
    migrateOldDataset,
    setTokenDSE,
    postToDariahStorage,
    checkSharedStatus, updateDariahStorage, logout
} from "./dariah.storage";
import {oldStorageURL, storageURL} from "../../js/dariah.geobro.conf";
import {allowedMimeTypes, host, getDSID, setDSID} from "./dariah.workflow.conf";
import {arr2csv, getEmbeddedDariahStatus, isEmptyString, newAlert} from "./dariah.utils";
import {checkDates} from "./dariah.workflow.datecheck";
import {loadOwnStorageFileList} from "./dariah.storage.list";
import * as csv from 'jquery-csv';
import ClipboardJS from "clipboard";
/**
 * Stuff to setup when DOM is ready.
 */
$(document).ready(function() {
    getEmbeddedDariahStatus(); //Check status of DARIAH services
    var isDataSheetEditor = document.location.href.includes("/edit/");
    var isList = document.location.href.includes("/edit/list.html");
    if (!isDataSheetEditor && !isList) return;
    if (isList) loadOwnStorageFileList();
    if (readToken()) {
        getAuthInfo();
        $('#loggedin').off('click').on('click', function (e) {e.preventDefault(); logout();});
    } else {
        if (isDataSheetEditor) setTableReadOnly(true); //read-only table
        // Set login menu items.
        $('#loggedin').append('<i class="icon-signin icon-white"></i> Login');
        $('#loggedin').off('click').on('click', function (e) {e.preventDefault(); login();});
    }

    if (!isDataSheetEditor) return; //only main DatasheetEditor page is relevant
    initMap(); // Init the OpenStreetmap widget.
    initClipboard(); // Init clipboard and tooltips.
    init();
    //showNews();
});

function toggleLogin(){

}

/**
 * Main init method, called every time the page is reloaded!
 */
function init() {
    // Drag and drop handler for CSV documents to workarea.
    console.log("INIT");
    $('#workArea')
        .on('dragover', handleDragOver, false)
        .on('drop', handleDropFileSelect);

    // Action for fileuploadbutton.
    $('#files').on('change', handleFileSelect);

    // jQuery creates it's own event object, and it doesn't have a dataTransfer property yet.
    // This adds dataTransfer to the event object. Thanks to l4rk for figuring this out!
    jQuery.event.props.push('dataTransfer');
    $('#newButton').off('click').on('click', function() {createNew()});
    $('#importButton').off('click').on('click', function(){toggleUploadButton()});
    // Do things depending on ID existance as URL parameter (id=).
    doThingsOnIDExistingCheck();
    // Set token first if token hash (#access_token=...) is existing.
    if (window.location.hash) {
        setTokenDSE();
    }
    // ...otherwise do everything else!
    else {
        // Get action and storage ID from URL param.
        let searchParams = new URLSearchParams(window.location.search)
        var action = searchParams.get('action') || '';
        setDSID(searchParams.get('id') || '');

        var dsid = getDSID();
        // Set id to login button if existing. So datasheet is reloaded after login.
        if (!readToken()) {
            // Append "id=" only if dsid is existing.
            var dsidkey = '';
            if (dsid) dsidkey = 'id='+dsid;
            $('#loggedin').off('click').on('click', function (e) {e.preventDefault(); authenticate(dsidkey);});
        }

        // Check for additional param in action string, PDP seems to prevail only the first param in the list!
        if (action.indexOf('-') > 0) {
            var split = action.split('-');
            var prefix = split[0];
            var suffix = split[1];
            // Handle 'create-123456' things (old storage IDs).
            if (prefix === 'create' && isOldStorageID(suffix)) {
                setDSID(suffix);
                migrateOldDataset(suffix);
            }
            // Handle loading files from file dialog or drag'n'drop.
            else if (prefix === 'create' && suffix === 'files') {
                var fileData = getFiles();
                if (csvToTable(fileData)) postToDariahStorage(fileData);
                removeFiles();
            }
        }
        // New datasheet shall be created.
        else if (action === 'create') {
            createNew();
        }
        // Datasheet shall be shared.
        else if (action === 'share') {
            shareGeobrowserDataset();
        }
        // Datasheet shall be unshared.
        else if (action === 'unshare') {
            unshareGeobrowserDataset();
        }
        // Datasheet shall be deleted.
        else if (action === 'delete') {
            deleteDataset();
        }
        // Autosave after authentication.
        else if (action === 'autosave') {
            autoSave();
        }
        // If we have got no action but an ID in the URL params.
        else {
            var dsid = getDSID();
            if (dsid) {
                // Check if we have got an old storage ID here, load data from old DARIAH-DE Storage then.
                if (isOldStorageID(dsid)) {
                    loadFromOldDariahStorage(dsid);
                } else {
                    // Load data from OwnStorage if ID supplied in params.
		            loadFromDariahStorage(dsid);
                    if (migrationIsSet()) {
                        removeMigration();
                        var url = host + '/edit/index.html?id=' + dsid;
                        var title = 'Dataset was successfully migrated to the DARIAH-DE OwnStorage!';
                        var message = '<p>The migrated dataset has got the ID <i>' + dsid + '</i> and is now stored in the DARIAH-DE OwnStorage. You can access it here in the Datasheet Editor using the URL ' + '<a href="'+ url + '">' + url + '</a> and you can now do everything you can do with your DARIAH-DE OwnStorage datasets :-) Have fun!</p>';
                        newAlert('success', title,  message);
                    }
                }
            }
            // If we have got no ID in the URL params (no create action).
        	else {
	            showFirstStartWizard();
    	    }
    	}
	}
}

/**
 * Ask for creation (modal) if a dsid is already existing.
 */
function createNew() {

    var dsid = getDSID();
    if (readToken() !== null) {
        if (dsid && !isOldStorageID(dsid)) {
            $('#askForCreation').modal('show');
        } else {
            createNewDataset();
        }
    } else {
        var action = 'create';
        if (isOldStorageID(dsid)) {
            action += '-' + dsid;
        }
        authenticate('action=' + action);
    }
}

/**
 * Create new dataset.
 */
function createNewDataset() {
    var tableInstance = getTable();
    var dsid = getDSID();
    // Create new dataset from old storage.
    if (isOldStorageID(dsid)) {
        // Get data out of table.
        var csv = arr2csv(tableInstance.getData());
        // Create new data file.
        postToDariahStorage(csv);
    }
    // Create new dataset with empty table body.
    else {
        // Re-set ID.
        setDSID('');
        // Fill with initial data.
        var initialData = createInitialTableContent();
        tableInstance.loadData(initialData);
        if (tableInstance.getSettings().readOnly) setTableReadOnly(false);
        // Create CSV structure.
        var arr = arr2csv(initialData);
        // Create new data file.
        postToDariahStorage(arr);
        setPermanentConsoleMessage('All changes will be autosaved.');
        showMessage('idMessage', 'This dataset is stored to the DARIAH-DE OwnStorage and has got the ID');
    }
}

/*
 *
 */
function deleteDataset() {
    if (readToken() !== null) {
        $('#askForDeletion').modal('show');
    } else {
        authenticate('action=delete');
    }
}

/*
 *
 */
function shareGeobrowserDataset() {
    if (readToken() !== null) {
        $('#askForSharing').modal('show');
    } else {
        authenticate('action=share');
    }
}

/*
 *
 */
function unshareGeobrowserDataset() {
    if (readToken() !== null) {
        $('#askForUnsharing').modal('show');
    } else {
        authenticate('action=unshare');
    }
}

/**
 *
 */
var saveTimer;
export function autoSave() {
        // Get data.
        var tableInstance = getTable();
        var arr = tableInstance.getData();
    	var csv = arr2csv(arr);
        // Make sure that not more than one save per 2,5 seconds occur.
        clearTimeout(saveTimer);
        saveTimer = setTimeout(function() {
            // Check for token first.
            if (readToken() !== null) {
                updateDariahStorage(getDSID(), csv);
            } else {
                authenticate('id=' + getDSID());
            }
        }, 2500);
}

/**
 * Functions for loading file into Browser, drag'n'drop or with button
 * look at
 * - http://www.html5rocks.com/de/tutorials/file/dndfiles/
 * - http://weblog.bocoup.com/using-datatransfer-with-jquery-events/
 */

/**
 *
 */
function handleFileSelect(evt) {
	var files = evt.target.files; // FileList object
	loadFromFiles(files);
}

/**
 *
 */
function handleDropFileSelect(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	var files = evt.dataTransfer.files; // FileList object.
	loadFromFiles(files);
}

/**
 *
 */
function handleDragOver(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}

/**
 *
 */
function loadFromFiles(files) {

    for (var i = 0, f; f = files[i]; i++) {
        //console.log(f.name + ' - ' + f.type || 'n/a' + ' - ' + f.size + ' bytes, last modified: ' + f.lastModifiedDate.toLocaleDateString());
        // Check for mimetype (browser-based).
        if($.inArray(f.type, allowedMimeTypes) < 0 ) {
            newAlert('error', 'Could not open file with name “' + f.name + '“!', 'Maybe the mimetype “' + f.type + '“ is not supported, can not be determined or your file is not a CSV or KML file at all. Datasheet Editor can only handle files of the following mimetypes: <strong>' + allowedMimeTypes + '</strong>. If your file really is a CSV file, please try adding “.csv“ as a file suffix! If KML, please try “.kml“ or “.xml“, so your browser can determine the mimetype.', 'fileImportError');
            continue;
        }
        var isKML = f.type === "application/vnd.google-earth.kml+xml" || f.type === "text/xml";
        if (isKML) setImportMessage('Converting KML to CSV...', true);
        else setImportMessage('Importing CSV...', true);
        var reader = new FileReader();
        reader.onload = function(e) {
            var data = e.target.result;
            setImportMessage('', false);
            if (isKML) data = loadKMLFromCSV(data);
            if (readToken() !== null) {
                if (isKML && data) {
                    postToDariahStorage(data);
                    loadKMLToTable(data);
                } else if (!isKML && csvToTable(data)) postToDariahStorage(data);
            } else {
                // Cache data.
                if (data) setFiles(data);
                authenticate('action=create-files');
            }
        };
        reader.readAsText(f);
    }
}

function getXMLTag(root, tag){
    //double quotes not allowed because they are used as delimiters
    var element  = $(root).find(tag).text();
    return element.replaceAll('"',"'");
}

function loadKMLFromCSV(data){
    $(".fileImportSuccess").alert('close');
    var xml = '';
    try {
        xml  = $($.parseXML(data));
    } catch (e){
        newAlert('error', 'Error reading KML!', '<p>Could not read local KML file! Please make sure that the file corresponds to the <a target="_blank" href="https://geobrowser.de.dariah.eu/doc/geobrowser.html#specification-for-use">specification</a>.<br>Error message: '+e.message+'</p>', 'fileImportError');
        return false;
    }
    var placemarks = xml.find( "Placemark" );
    if (!placemarks || isEmptyString(placemarks.text())) {
        newAlert('error', 'Error reading KML!', '<p>Could not read local KML file! "Placemark" tag is missing. Please make sure that the file corresponds to the <a target="_blank" href="https://geobrowser.de.dariah.eu/doc/geobrowser.html#specification-for-use">specification</a>.</p>', 'fileImportError');
        return false;
    }
    var rows = [["Name", "Address", "Description", "Longitude", "Latitude", "TimeStamp", "TimeSpan:begin", "TimeSpan:end", "GettyID"]];
    var name, address, description, point, coords, lon, lat, timeStamps, timeStamp, timeStampBegin, timeStampEnd, row;
    $.each(placemarks, function(index, placemark){
         name = getXMLTag(placemark, "name");
         address =  getXMLTag(placemark, "address");
         description = getXMLTag(placemark, "description");
         point = $(placemark).find("Point");
         coords = getXMLTag(point, 'coordinates');
         lon = coords.split(',')[0];
         lat = coords.split(',')[1];
         timeStamps =  $(placemark).find("TimeStamp");
         timeStamp =  getXMLTag(timeStamps, "when");
         timeStampBegin = getXMLTag(timeStamps, "begin");
         timeStampEnd = getXMLTag(timeStamps, "end");
         row = [name, address, description, lon, lat, timeStamp, timeStampBegin, timeStampEnd, ''];
         rows.push(row);
    })

   return rows;

}

function loadKMLToTable(rows){
    hideFirstStartWizard();
    newAlert('success', 'Converted KML to CSV!', '<p>Successfully converted KML to CSV. Default CSV column headers have been added. Please note that only following tags from the <a target="_blank" href="https://geobrowser.de.dariah.eu/doc/geobrowser.html#specification-for-use">KML specification</a> are considered during import: <i>Placemark, name, address, description, Point (coordinates), TimeStamp (when/begin/end)</i>. All other tags are ignored by default! </p>', 'fileImportSuccess');
    getTable().loadData(rows);
    getTable().updateSettings({
        minSpareRows: 1 // add spare row at the end
    });
    getTable().validateCells();
    getTable().render();
    return arr2csv(getTable().getData());
}

/**
 *
 */
export function csvToTable(data) {
    setTableReadOnly(false);
    var success = true;
    try {
        var arr = csv.toArrays(data); //default delimiter: "
        getTable().loadData(arr);
        getTable().updateSettings({
            minSpareRows: 1 // add spare row at the end
        });
        hideFirstStartWizard();
        getTable().render();
    } catch (e){
        success = false;
        newAlert('error', 'Error reading CSV!',  '<p>Could not read local CSV file! Error message: <i>'+e.message+'</i></p>', 'fileImportError');
    }
    return success;
}

function setTableReadOnly(readOnly){
        getTable().updateSettings({
            readOnly: readOnly
        });
}
/**
 *
 */
export function csvToReadOnlyTable(data) {
    setTableReadOnly(true);
    hideFirstStartWizard();
    var arr = csv.toArrays(data);
    getTable().loadData(arr);
    setPermanentConsoleMessage('This dataset is read-only.');
    showMessage('idMessage', 'This dataset was loaded from the old DARIAH-DE Storage and has got the ID');
    showMessage('sharedStatus', 'This dataset is public.');
}

/**
 * Just downloads dataset from table.
 */
function download() {
    // Get URL for filename and download.
    var url;
    var dsid = getDSID();
    // Create new a element.
    var fancyA = document.createElement('a');
    document.body.appendChild(fancyA);
    fancyA.style = 'display:none';
    // Get data out of table, set URL.
    var csv = arr2csv(getTable().getData());
    // TODO Maybe use csvStorageMimetype here?
    var blob = new Blob([csv], {type: 'text/csv;charset=utf-8'});
    url = window.URL.createObjectURL(blob);
    // Fill a element with URL and filename.
    fancyA.href = url;
    fancyA.download = dsid + '.csv';
    // Click on a :-)
    fancyA.click();
    // Remove URL.
    window.URL.revokeObjectURL(url);
}

/**
 * Display welcome screen.
 */
export function showFirstStartWizard() {

	$('#sheetArea').hide();
    setPermanentConsoleMessage('');
    showMessage('idMessage', '');
    showMessage('sharedStatus', '');
    // Empty tgn list.
    $('#tgnList').empty();
    $('#geolocationArea').hide();

}

/**
 * Display table and geolocation tools.
 */
function hideFirstStartWizard() {

	$('#sheetArea').show();
	if (!getTable().getSettings().readOnly) {
        $('#geolocationArea').show();
    }
    resizeMap();
}

/**
 *
 */
export function newSheetAlert() {
	hideFirstStartWizard();
    var url = host + '/edit/index.html?id=' + getDSID();
	var title = 'New datasheet created!';
	var message = '<p>Your datasheet has been successfully created and stored to the DARIAH-DE OwnStorage. ' + 'You can access it here in the Datasheet Editor using the URL ' + '<a href="'+ url + '">' + url + '</a>.</p><p>Please <strong>DO BOOKMARK THIS URL</strong> as direct link to this dataset!</p>';
	newAlert('success', title,  message);
}

/**
 *
 */
export function openGeoBrowser(newWindow, coordValidation) {
    // Check all at once.
    var val1 = checkColumnHeadersGeobrowser();
    var val2 = checkDates();
    // Overwrite geocoordinates validation result if coordValidation is set to true.
    if (!coordValidation) {
        val3 = true;
    }
    else {
        var val3 = checkCoordinates();
    }
    if (! (val1 && val2 && val3) ) {
        return;
    }
    var geoBrowserPath = window.location.pathname.replace("/edit","");
    var url = host + geoBrowserPath + '?csv1=';
    if (isOldStorageID(getDSID())) {
        url += oldStorageURL + getDSID();
    } else {
        url += storageURL + getDSID();
    }
	if (newWindow) {
		window.open(url);
	} else {
		document.location.href = url;
	}
}

 /**
 *
 */
function showMessage(area, text) {
    $('#' + area).text(text);
}

/**
 *
 */
export function doThingsOnIDExistingCheck () {

    var dsid = getDSID();
    if (dsid) {
        var url = storageURL + dsid;
        $('#dataLoc input').val(dsid);
        $('#dataLoc button').data('clipboard-text', url);
        $('#dataLoc button').attr('data-clipboard-text', url);
        // If logged in...
        if (readToken()) {
            checkSharedStatus(dsid, function() {
                setSharedUnsharedStatusOnScreen();
                $('#dataLoc button').removeClass('disabled');
                $('#dataLoc button').attr('disabled', false);
                $('#uploadButton').css('display','none');
                enableManagedButtons();
                setPermanentConsoleMessage('All changes will be autosaved.');
                showMessage('idMessage', 'This dataset is stored to the DARIAH-DE OwnStorage and has got the ID');
            });
        }
        // If not logged in...
        else {
            doThingsOnReadOnly();
            showMessage('idMessage', 'This dataset has got the ID');
            showMessage('sharedStatus', 'This dataset is public.');
            var title = 'You are accessing a shared dataset!';
            var message = '<p>This dataset with ID <i>' + dsid + '</i> is publicly available for reading! If you are the owner of this dataset, you can <strong>Login</strong> to manage your dataset. If not, you can either download it using the <strong>Download dataset</strong> button on the right, <strong>View the dataset</strong> with the Geo-Browser, or just <strong>Create a new dataset</strong>.</p>';
            newAlert('warning', title,  message);
        }
    } else {
        $('#dataLoc input').val('ID not (yet) set');
        $('#dataLoc button').addClass('disabled');
        disableManagedButtons();
    }

    getTable().render(); //re-render after DOM updates
}

/**
 *
 */
export function doThingsOnOldStorageID () {
    if (getDSID()) {
        // Old storage ID is always available, so we can put the URL in here.
        var url = oldStorageURL + getDSID();
        $('#dataLoc input').val(getDSID());
        $('#dataLoc button').data('clipboard-text', url);
        $('#dataLoc button').attr('data-clipboard-text', url);
        doThingsOnReadOnly();
    }
}

/**
 *
 */
function doThingsOnReadOnly () {

    $('#dataLoc button').removeClass('disabled');
    $('#dataLoc button').attr('disabled', false);
    // Enable download button only.
    $('#downloadButton').removeClass('disabled');
    $('#downloadButton').attr('disabled', false);
    $('#downloadButton').addClass('btn-primary');
    $('#downloadButton').off('click').on('click', function(){download();});

    // Disable other buttons.
    $('#deleteButton').addClass('disabled');
    $('#deleteButton').attr('disabled', true);
    $('#gbShareThisDatasetButton').addClass('disabled');
    $('#gbShareThisDatasetButton').attr('disabled', true);
    $('#gbUnshareThisDatasetButton').addClass('disabled');
    $('#gbUnshareThisDatasetButton').attr('disabled', true);
    showMessage('sharedStatus', 'This dataset is public.');
    // Disable Geolocation completion button.
    $('#geolocationCompletionButton').addClass('disabled');
    $('#geolocationCompletionButton').attr('disabled', true);
    // Disable Set button in map selection.
    $('#setAllPlacesOSMButton').addClass('disabled');
    $('#setAllPlacesOSMButton').attr('disabled', true);
    // Enable Geo-Browser button.
    $('#openWithGeoBrowserButton').removeClass('disabled');
    $('#openWithGeoBrowserButton').attr('disabled', false);
    $('#openWithGeoBrowserButton').addClass('btn-primary');
    $('#openWithGeoBrowserButton').off('click').on('click', function(){openGeoBrowser(true,true)});

    setPermanentConsoleMessage('This dataset is read-only.');
    $('#tgnList').empty();
    $('#geolocationArea').hide();
}

/**
 *
 */
function disableManagedButtons() {
    // Disable buttons and make them un-clickable.
    $('#downloadButton').addClass('disabled');
    $('#downloadButton').attr('disabled', true);
    $('#deleteButton').addClass('disabled');
    $('#deleteButton').attr('disabled', true);
    $('#gbShareThisDatasetButton').addClass('disabled');
    $('#gbShareThisDatasetButton').attr('disabled', true);
    $('#gbUnshareThisDatasetButton').addClass('disabled');
    $('#gbUnshareThisDatasetButton').attr('disabled', true);
    $('#geolocationCompletionButton').addClass('disabled');
    $('#geolocationCompletionButton').attr('disabled', true);
    $('#openWithGeoBrowserButton').addClass('disabled');
    $('#openWithGeoBrowserButton').attr('disabled', true);
    $('#setAllPlacesOSMButton').addClass('disabled');
    $('#setAllPlacesOSMButton').attr('disabled', true);
    // Change type of buttons (color) as guide for the users.
    $('#newButton').addClass('btn-primary');
    $('#importButton').addClass('btn-primary');
    $('#downloadButton').removeClass('btn-primary');
    $('#deleteButton').removeClass('btn-danger');
    $('#gbShareThisDatasetButton').removeClass('btn-primary');
    $('#gbUnshareThisDatasetButton').removeClass('btn-primary');
    $('#geolocationCompletionButton').removeClass('btn-primary');
    $('#openWithGeoBrowserButton').removeClass('btn-primary');
}

/**
 *
 */
function enableManagedButtons() {

    // Enable buttons and make them clickable.
    $('#downloadButton').removeClass('disabled');
    $('#downloadButton').attr('disabled', false);
    $('#deleteButton').removeClass('disabled');
    $('#deleteButton').attr('disabled', false);
    $('#gbShareThisDatasetButton').removeClass('disabled');
    $('#gbShareThisDatasetButton').attr('disabled', false);
    $('#gbUnshareThisDatasetButton').removeClass('disabled');
    $('#gbUnshareThisDatasetButton').attr('disabled', false);
    $('#geolocationCompletionButton').removeClass('disabled');
    $('#geolocationCompletionButton').attr('disabled', false);
    $('#openWithGeoBrowserButton').removeClass('disabled');
    $('#openWithGeoBrowserButton').attr('disabled', false);
    $('#setAllPlacesOSMButton').removeClass('disabled');
    $('#setAllPlacesOSMButton').attr('disabled', false);
    // Change type of buttons (color) as guide for the users.
    $('#newButton').removeClass('btn-primary');
    $('#importButton').removeClass('btn-primary');
    $('#downloadButton').addClass('btn-primary');
    $('#deleteButton').addClass('btn-danger');
    $('#gbShareThisDatasetButton').addClass('btn-primary');
    $('#gbUnshareThisDatasetButton').addClass('btn-primary');
    $('#geolocationCompletionButton').addClass('btn-primary');
    $('#openWithGeoBrowserButton').addClass('btn-primary');

    $('#newButton').off('click').on('click', function () {createNew();});
    $('#importButton').off('click').on('click', function () {toggleUploadButton();});
    $('#downloadButton').off('click').on('click', function () {download();});
    $('#deleteButton').off('click').on('click', function () {deleteDataset();});
    $('#gbShareThisDatasetButton').off('click').on('click', function () {shareGeobrowserDataset();});
    $('#gbUnshareThisDatasetButton').off('click').on('click', function () {unshareGeobrowserDataset();});
    $('#geolocationCompletionButton').off('click').on('click', function () {startGeocoding();});
    $('#openWithGeoBrowserButton').off('click').on('click', function () {
        openGeoBrowser(true,true);});

    $('#setAllPlacesOSMButton').off('click').on('click', function () {setAllPlacesOSM();});
    $('#geolocationSource').on('onchange', function(){toggleTGNOptions();});
    $('#deleteUnpublishButton').off('click').on('click', function () {unpublishAndDeleteDataset();});
    $('#publishConfirmButton').off('click').on('click', function () {publishDataset();});
    $('#unpublishConfirmButton').off('click').on('click', function () {unpublishDataset();});
    $('#newDatasetButton').off('click').on('click', function () {createNewDataset();});
    $('#confirmUploadButton').off('click').on('click', function () {showUploadButton();});
    $('#tgnButton').off('click').on('click', function () {tgnSearch();});
    $('#geonamesButton').off('click').on('click', function () {ognSearch();});
    $('#osmButton').off('click').on('click', function () {osmSearch();});
    $('#howtoButton').off('click').on('click', function () {toggleHowto();});
    $('#clearSelectionButton').off('click').on('click', function () {clearTableSelection();});
}

/**
 * Set shared (or unshared) status on screen!
 */
export function setSharedUnsharedStatusOnScreen() {
    if (publicIsSet()){
        $('#gbShareThisDatasetButton').hide();
        $('#gbUnshareThisDatasetButton').show();
        $('#sharedStatus').text('This dataset is publicly shared.');
    }
    else {
        $('#gbUnshareThisDatasetButton').hide();
        $('#gbShareThisDatasetButton').show();
        $('#sharedStatus').text('This dataset is private.');
    }
}

/*
 *
 */
function toggleUploadButton() {
    // Hiding button is always possible without warning.
    if (!document.getElementById('uploadButton').style.display) {
        $('#uploadButton').css('display','none');
    }
    // Showing the button reveals a warning, if a dataset is already on display.
    else {
        if (getDSID() && !isOldStorageID(getDSID())) {
            $('#askForImport').modal('show');
        } else {
            $('#uploadButton').css('display','');
        }
    }
}

/*
 *
 */
function showUploadButton() {
    $('#uploadButton').css('display', '');
}

/**
 *
 */
function initClipboard() {
    var clipboard = new ClipboardJS('.doi-copy');
    clipboard.on('success', function(e) {
        var originalTitle = e.trigger.getAttribute('data-original-title');
        $(e.trigger).tooltip('hide')
            .attr('data-original-title', 'Copied')
            .tooltip('show')
        e.clearSelection();
        setTimeout(function() {
            $(e.trigger).tooltip('hide').attr('data-original-title', originalTitle);
        }, 2000);
    });
}

/**
 * Returns true if given id is one of the old DARIAH Storage.
 */
export function isOldStorageID(id) {
    return /^[0-9]+$/.test(id);
}

/*
 *
 */
export function showNews() {
    var title = 'News';
    var message = '<p> –– put news here –– </p>';
    newAlert('info', title, message, '', '', '', 'nodate');
}

/*
 *
 */
function toggleHowto() {
    var title = 'How to fill the table?';
    var message = "<dl><dt><strong style='color:green;'>Name</strong></dt> <dd>enables you to name your data point. Place names (used for <i>Geolocation completion</i>) have to be entered in the <i>Address</i> field, so <i>Name</i> can be used to display the name of an event or a name that would not be recognized as Address.</dd>" +
    "<dt><strong style='color:green;'>Address</strong></dt> <dd>identifies a place with this place name and will be used to add <i>Longitude</i> and <i>Latitude</i> via <i>Geolocation completion</i>.</dd>" +
    "<dt><strong style='color:green;'>Description</strong></dt> <dd>is used to display any additional Data.</li>" +
    "<dt><strong style='color:green;'>Longitude</strong> and <strong style='color:green;'>Latitude</strong></dt> <dd>can be filled in by <i>Geolocation completion</i> or manually entered and uses decimal geographic coordinates. Existing coordinates will not be overwritten by <i>Geolocation completion</i>.</dd>" +
    "<dt><strong style='color:green;'>TimeStamp</strong></dt> <dd>is used to store the date of an entry. Some examples of entries for this field would be <i>1970</i>, <i>1970-12</i>, <i>1970-12-10</i>, and <i>1970-12-10T20:15:00</i>. For more information on accepted date and time formats please have a look at <a href='http://books.xmlschemata.org/relaxng/ch19-77041.html' target='_blank'>xsd:date</a>, <a href='http://books.xmlschemata.org/relaxng/ch19-77049.html' target='_blank'>xsd:dateTime</a>, <a href='http://books.xmlschemata.org/relaxng/ch19-77127.html' target='_blank'>xsd:gYear</a> and <a href='http://books.xmlschemata.org/relaxng/ch19-77135.html' target='_blank'>xsdGYearMonth</a> types.</dd>" +
    "<dt><strong style='color:green;'>TimeSpan:begin</strong> and <strong style='color:green;'>TimeSpan:end</strong></dt> <dd>can be used to convey a timespan. Some examples of entries for this field would be <i>1970</i>, <i>1970-12</i>, <i>1970-12-10</i>, and <i>1970-12-10T20:15:00</i>. For more information on accepted date and time formats please have a look at <a href='http://books.xmlschemata.org/relaxng/ch19-77041.html' target='_blank'>xsd:date</a>, <a href='http://books.xmlschemata.org/relaxng/ch19-77049.html' target='_blank'>xsd:dateTime</a>, <a href='http://books.xmlschemata.org/relaxng/ch19-77127.html' target='_blank'>xsd:gYear</a> and <a href='http://books.xmlschemata.org/relaxng/ch19-77135.html' target='_blank'>xsdGYearMonth</a> types.</dd>" +
    "<dt><strong style='color:green;'>GettyID</strong></dt> <dd>is used for the exact identification of TGN locations and is automatically filled in using the Getty Thesaurus of Geographic Names via <i>Geolocation completion</i>. An existing GettyID will not be overwritten by <i>Geolocation completion</i></dd></dl>";

    if (document.getElementById('howto')) {
        document.getElementById('howto').remove();
    } else {
        newAlert('info', title, message, '', 'howto', 'howtoArea', 'nodate');
    }
}

export function setImportMessage (message, show) {
    if (show) $('#importDataSpinner').removeClass('hide');
    else $('#importDataSpinner').addClass('hide');
    showMessage('importMessage', message);
}

/**
 * Show console message and spin the flower!
 */
export function setWorkingConsoleMessage (message) {
    $('#spinningConsoleCompletionFlower').removeClass('hide');
    showMessage('console', message);
}

/**
 * Show console message and hide the spinning flower!
 */
export function setPermanentConsoleMessage (message) {
    $('#spinningConsoleCompletionFlower').addClass('hide');
    showMessage('console', message);
}
