/*
 * Geolocation workflows
 */
import {
	maxCharsPerTitle,
	noResults,
	ognURL,
	ognUsername,
	osmSearchUrl,
	tgnBaseURL,
	tgnMatchURL
} from "./dariah.workflow.conf";
import {
	checkColumnHeadersGeolocation, clearTableSelection,
	findColumn,
	getTable, isSelected,
	rowIsEmpty,
	updateCoordinates
} from "./dariah.workflow.table";
import 'ol/ol.css';
import {Map, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import {fromLonLat, toLonLat} from "ol/proj";
import {isEmptyString, newAlert} from "./dariah.utils";
import {Point} from "ol/geom";
import {Icon, Style} from "ol/style";
import {Collection, Feature} from "ol";
import {Modify} from "ol/interaction";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {openGeoBrowser} from "./dariah.workflow";
var map;
var locationPickerVector; // marker
var dragAndDrop;

export function initMap() {

	map = new Map({
		layers: [
			new TileLayer({source: new OSM()})
		],
		view: new View({
			center: [0, 0],
			zoom: 0
		}),
		target: 'editMap'
	});

}

export function getMap(){
	if (!map) initMap();
	return map;
}

export function resizeMap(){
	getMap().updateSize();
}

export function startGeocoding() {
	$('#tgnArea').show();

	// do nothing if there are columns missing
	if (!checkColumnHeadersGeolocation()) {
		return;
	}
	$('#tgnList').empty();
	$('#osmPlaceName').val('');
	var source = $('#geolocationSource').val();
	if (source === 'TGN'){
		var columnSource = $('#geolocationColumnSource').val();
		if (columnSource === 'GettyID') tgnByGettyID();
		else tgnByAddress();
	} else if (source === 'OSM'){
		osmBatchSearch();
	} else if (source === 'GeoNames'){
		geonamesBatchSearch();
	}
}

export function toggleTGNOptions(){
	var source = $('#geolocationSource').val();
	if (source === 'TGN') $('#tgnOptions').removeClass('hide');
	else $('#tgnOptions').addClass('hide');
	$('#tgnList').empty();
}

function getUniqueColumnValues(columnName){
	var dataSet = getTable().getData();
	var column = findColumn(dataSet, columnName);
	var selection = getTable().getSelectedLast();
	var columnValues = getTable().getDataAtCol(column);

	var selectedValues = [];
	$.each(columnValues, function(index, value){
		if(!isEmptyString(value) && value !== columnName) {
			if (isSelected(index, column, selection) && !selectedValues.includes(value.trim())){
				selectedValues.push(value.trim());
			}
		}
	})
	return selectedValues;
}

/**
 * Creates item for the geolocated address list, including map/set buttons
 * @param place
 * @param elemId
 */
function createListItem(place, elemId){
	var titlePlace = place;
	if (place.length > maxCharsPerTitle) {
		titlePlace = place.substr(0, maxCharsPerTitle) + "...";
	}
	var elem = '<div class="input-append">'
		+ '<h4 style="margin-bottom:3px;">' + titlePlace + '</h4>'
		+ '<p><select style="width:64%;" id="' + elemId + '"></select>'
		+ '<button class="btn" style="width:18%;" id="pick-' + elemId + '" place="' + place + '" title="Show this place in the map selection on the right."><i class="icon-eye-open"></i>&nbsp;Map</button>'
		+ '<button class="btn" style="width:18%;" id="setAll-' + elemId + '" title="Set the coordinates of all places with address=' + place + ' to the coordinates of the chosen place."><i class="icon-pencil"></i>&nbsp;Set</button>'
		+ '</p></div>';

	$('#tgnList').append(elem);
	// Bind function to change event.
	$('#' + elemId).change(placeChanged);
	$('#pick-' + elemId).bind('click', pickPlace);
	$('#setAll-' + elemId).bind('click', setAllPlaces);
	// add disabled and hidden option for storing the original place name!
	$('#' + elemId).append('<option id="tgnOrigPlace-' + elemId + '" disabled="disabled" class="hide" value="' + place + '">' + place + '</option>');

}


/**
 * Geocodes all addresses using TGN
 * */
function tgnByAddress(){
	var places = getUniqueColumnValues('Address');
	var numberOfCallsToDo = places.length;
	if (numberOfCallsToDo === 0){
		showNoAddressesError();
	} else $(".geocodeAlert").alert('close');
	var numberOfCallsCompleted = 0;
	$.each(places, function(index, place) {
		setTimeout(function(){
			// disable Geolocation completion button before first Getty GET!
			if (index === 0 ) geolocationCompletionStart();
			// query TGN
			var placeAlreadySetInTable = false;
			$.get(tgnMatchURL, {q : place, georef : 'true'}).success(function(xml){

					var elemId = 'batch_' + getBase64Without(place);
					createListItem(place, elemId);
					// Handle TGN results.
					$(xml).find('term').each(function (candidateIndex, val) {
						var id = $(this).attr('id');
						id = id.substring(id.indexOf(':') + 1);
						var path = $(this).find('path').text();
						var name = $(this).find('name').text();
						var variant = $(this).find('variant').text();

						// Check for empty or not existing name tag, use match_name instead.
						if (name === "") name = $(this).find('match_name').text();
						var lat = $(this).find('latitude').text();
						var long = $(this).find('longitude').text();

						// Only add places with exact match or variant involved!
						if (place === name || variant.includes(place)) {
							$('#' + elemId).append('<option value="' + name + '|' + id + '|' + lat + '|' + long + '">' + name + ': ' + path + '</option>');
						}

						// Set coordinates of place to first returned entry with exact title match.
						if (!placeAlreadySetInTable && (place === name || variant.includes(place))) {
							updateCoordinates(name, lat, long, id, false, variant);
							// setOSMapCenter(id, lat, long, 12); //if place change should be shown in map for each item
							placeAlreadySetInTable = true;
						}
					});
					numberOfCallsCompleted++;
					if (numberOfCallsToDo === numberOfCallsCompleted) {
						geolocationCompletionComplete();
					}
					// Use additional text for empty TGN result.
					if (($(xml).find('term')).length === 0) {
						setNotFoundMessage(elemId);
					}
				}).fail(function(xhr, status, error) {
					numberOfCallsCompleted++;
					if (numberOfCallsToDo === numberOfCallsCompleted) {
						geolocationCompletionComplete();
					}
				showGeolocatorFailError(place, error);
				});
		}, 1500);
	});
}

/**
 * Geocodes all GettyIDs using TGN
 */
function tgnByGettyID(){
	var gettyIds = getUniqueColumnValues('GettyID');
	var numberOfCallsToDo = gettyIds.length;
	if (numberOfCallsToDo === 0){
		var selection = getTable().getSelectedLast();
		if (selection){
			newAlert('error', 'No GettyID values in current selection!', '<p>No GettyID column values found in selected cells. If you want to process the whole data set, clear the current selection first.' +
				' <br> Otherwise, select non-empty values in the <i>GettyID</i> column.</p>' +
				'<button class="btn btn-small btn-primary" id="clearSelectionFromMsg" title="Clear selection">Clear selection</button>', 'geocodeAlert');
			$('#clearSelectionFromMsg').bind('click', function () {clearTableSelection();$('.geocodeAlert').alert('close');});
		} else newAlert('error', 'No data available for Geolocation!', '<p>No values found in column <i>GettyID</i>.' + '</p>', 'geocodeAlert');
	} else $(".geocodeAlert").alert('close');
	var numberOfCallsCompleted = 0;
	$.each(gettyIds, function(index, id) {
		if (index === 0 ) geolocationCompletionStart();
		setTimeout(function(){
			$.get(tgnBaseURL + id + '.json').success(function (json) {
				var results = json.results.bindings;
				var lat, long, label;
				results.forEach(function (obj) {
					var pred = obj.Predicate.value;
					if (pred === "http://www.w3.org/2000/01/rdf-schema#label") {
						if (!label) label = obj.Object.value;
					} else if (pred === "http://schema.org/latitude"){
						lat = obj.Object.value;
					} else if (pred === "http://schema.org/longitude"){
						long = obj.Object.value;
					}
				})

				if (label && lat && long) {
					var elemId = 'batch_' + getBase64Without(id);
					var elem = '<div class="input-append">'
						+ '<h4 style="margin-bottom:3px;">' + id + '</h4>'
						+ '<p><select style="width:64%;" id="' + elemId + '"></select>'
						+ '<button class="btn" style="width:18%;" id="pick-' + elemId + '" id="' + id + '" title="Show this place in the map selection on the right."><i class="icon-eye-open"></i>&nbsp;Map</button>'
						+ '<button class="btn" style="width:18%;" id="setAll-' + elemId + '" title="Set the coordinates of all places with TGN ID =' + id + ' to the coordinates of the chosen place."><i class="icon-pencil"></i>&nbsp;Set</button>'
						+ '</p></div>';

					$('#tgnList').append(elem);
					$('#' + elemId).change(placeChanged);
					$('#pick-' + elemId).bind('click', pickPlace);
					$('#setAll-' + elemId).bind('click', setAllPlaces);
					$('#' + elemId).append('<option id="tgnOrigPlace-' + elemId + '" disabled="disabled" class="hide" value="' + label + '">' + label + '</option>');
					$('#' + elemId).append('<option value="' + label + '|' + id + '|' + lat + '|' + long + '">' + label + '</option>');
					updateCoordinates(label, lat, long, id, false);
				}

				numberOfCallsCompleted++;
				if (numberOfCallsToDo === numberOfCallsCompleted) {
					geolocationCompletionComplete();
				}
				/// Use additional text for empty TGN result.
				if (!label) setNotFoundMessage(elemId);

			}).fail(function(xhr, status, error) {
				numberOfCallsCompleted++;
				if (numberOfCallsToDo === numberOfCallsCompleted) {
					geolocationCompletionComplete();
				}
				showGeolocatorFailError(id, error);
			});
		}, 1500);
	});
}

/**
 * Search for all addresses in OSM (Nominatim).
 */
function osmBatchSearch(){
	var places = getUniqueColumnValues('Address');
	var numberOfCallsToDo = places.length;
	if (numberOfCallsToDo === 0){
		showNoAddressesError();
	} else $(".geocodeAlert").alert('close');
	var numberOfCallsCompleted = 0;

	$.each(places, function(index, place) {
		if (index === 0 ) geolocationCompletionStart();
		setTimeout(function(){ //add delay between requests
			$.getJSON(osmSearchUrl, {q : place, format : 'json'}).success(
				function(data) {
					var elemId = 'batch_' + getBase64Without(place);
					createListItem(place, elemId);
					// Handle OSM results.
					$.each(data, function(candidateIndex, val) {
						var name = val.display_name;
						var lat = val.lat;
						var long = val.lon;
						$('#' + elemId).append('<option value="' + name + '|' + '|' + lat + '|' + long + '">' + name + '</option>');
						if (candidateIndex === 0) { //first address candidate
							updateCoordinates(place, lat, long, '', false, null);
						}
					});
					numberOfCallsCompleted++;
					if (numberOfCallsToDo === numberOfCallsCompleted) {
						geolocationCompletionComplete();
					}
					// Use additional text for empty OSM result.
					if (data.length === 0) {
						setNotFoundMessage(elemId);
					}
				}).fail(function(xhr, status, error) {
				numberOfCallsCompleted++;
				if (numberOfCallsToDo === numberOfCallsCompleted) {
					geolocationCompletionComplete();
				}
				showGeolocatorFailError(place, error);
			});

		}, 1500);

	});
}

/**
 * Search for all addresses in GeoNames.
 */
function geonamesBatchSearch(){
	var places = getUniqueColumnValues('Address');
	var numberOfCallsToDo = places.length;
	if (numberOfCallsToDo === 0){
		showNoAddressesError();
	} else $(".geocodeAlert").alert('close');
	var numberOfCallsCompleted = 0;

	$.each(places, function(index, place) {
		if (index === 0 ) geolocationCompletionStart();
		setTimeout(function(){
			$.getJSON(ognURL, {placename : place, username : ognUsername }).success(
				function(data) {
					var elemId = 'batch_' + getBase64Without(place);
					createListItem(place, elemId);
					// Handle GeoNames results.
					$.each(data.postalCodes, function(candidateIndex, val) {
						if (candidateIndex > 12) return; //limit number of candidates
						var name = val.placeName;
						var path = val.adminName3 + ' | ' + val.adminName1 + ' | ' + val.countryCode;
						var lat = val.lat;
						var long = val.lng;
						if (candidateIndex === 0) {
							updateCoordinates(place, lat, long, '', false);
						}
						$('#'+elemId).append('<option value="' + name + '|' + '' + '|' + lat + '|' + long + '">'+ name + ': ' + path + '</option>');
					});

					numberOfCallsCompleted++;
					if (numberOfCallsToDo === numberOfCallsCompleted) {
						geolocationCompletionComplete();
					}

					// Use additional text for empty OSM result.
					if (data.postalCodes.length === 0) {
						setNotFoundMessage(elemId);
					}
				}).fail(function(xhr, status, error) {
					numberOfCallsCompleted++;
					if (numberOfCallsToDo === numberOfCallsCompleted) {
						geolocationCompletionComplete();
					}
					showGeolocatorFailError(place, error);
				});
		}, 1500);
	});
}
/**
 * Search either for a single GettyID or an address in TGN
 */
export function tgnSearch() {

	var place = $('#search').val().trim();
	if (isEmptyString(place)) {
		$('#tgnOsmList').html("");
		return;
	}
	var isNum = /^\d+$/.test(place);
	if (place.length === 7 && isNum) {
		var searchString = '<p></p><p>Searching for TGN ID "' + place + '" in Getty Thesaurus of Geographical Names...</p>';
		$('#tgnOsmList').html(searchString);
		$.get(tgnBaseURL + place + '.json').success(function (json) {
			var results = json.results.bindings;
			var value = "";
			results.forEach(function (obj) {
				var pred = obj.Predicate.value;
				if (pred === "http://www.w3.org/2000/01/rdf-schema#label") {
					value = obj.Object.value;
				}
			})
			place = value;
			tgnAddressSearch(place);
		}).fail(function() {
			$('#tgnOsmList').html('<p></p><p><strong>No results</strong> found for TGN ID "' + place + '" in Getty Thesaurus of Geographical Names...</p>');
		});
	} else {
		tgnAddressSearch(place);
	}
}

/**
 * Search for a single address in TGN
 * @param place
 */
function tgnAddressSearch(place){
	var searchString = '<p></p><p>Searching for "' + place + '" in Getty Thesaurus of Geographical Names...</p>';
	var foundString = '<p></p><p>Places found for "' + place + '" in Getty Thesaurus of Geographical Names:</p>';
	$('#tgnOsmList').html(searchString);

	$.get(tgnMatchURL, {q : place, georef : 'true'}).success(function(xml) {
		var datasets = $(xml).find('term');
		if ($(datasets).length < 1) {
			$('#tgnOsmList').html('<p></p><p><strong>No results</strong> found for "' + place + '" in Getty Thesaurus of Geographical Names.</p>');
			return;
		} else {
			$('#tgnOsmList').html(foundString);
		}

		var elemId = 'tgn_' + getBase64Without(place);
		var elem = '<p></p><div>'
			+ '<select id="' + elemId + '" style="width:98%;"></select>'
			+ '</div>';

		$('#tgnOsmList').html(foundString + elem);
		// bind function to change event
		$('#' + elemId).change(showOSMapPlace);

		$(datasets).each(function (index, val) {
			var id = $(this).attr('id');
			id = id.substring(id.indexOf(':')+1);
			var path = $(this).find('path').text();
			var name = $(this).find('name').text();
			// check for empty or not existing name tag, use match_name instead.
			if (name === "") {
				name = $(this).find('match_name').text();
			}
			var lat = $(this).find('latitude').text();
			var long = $(this).find('longitude').text();

			// Show coordinates of first returned entry.
			if (index === 0 ) {
				setOSMapCenter(id, lat, long, 12);
			}
			$('#'+elemId).append('<option value="' + name + '|' + id + '|' + lat + '|' + long + '">' + name + ': ' + path + '</option>');
		});
	});
}

/**
 * Search for a single address in GeoNames.
 */
export function ognSearch() {
	var place = $('#search').val();
	if (isEmptyString(place)) {
		$('#tgnOsmList').html("");
		return;
	};
    var searchString = '<p></p><p>Searching for "' + place + '" in GeoNames...</p>';
    var foundString = '<p></p><p>Places found for "' + place + '" in GeoNames:</p>';

    $('#tgnOsmList').html(searchString);
	$.getJSON(ognURL, {placename : place, username : ognUsername }).success(
		function(data) {
			if (data.postalCodes.length < 1) {
				$('#tgnOsmList').html('<p></p><p><strong>No results</strong> found for "' + place + '" in GeoNames.</p>');
				return;
			}
            else {
                $('#tgnOsmList').html(foundString);
            }

			var elemId = 'ogn_' + getBase64Without(place);
            var elem = '<p></p><div>'
                + '<select id="' + elemId + '" style="width:98%;"></select>'
                + '</div>';

            $('#tgnOsmList').html(foundString + elem);
			$('#' + elemId).change(showOSMapPlace);
			var id = '';
			$.each(data.postalCodes, function(index, val) {
				var name = val.placeName;
				var path = val.adminName3 + ' | ' + val.adminName1 + ' | ' + val.countryCode;
				if (index === 0 ) {
					setOSMapCenter(id, val.lat, val.lng, 12);
				}
				$('#'+elemId).append('<option value="' + name + '|' + id + '|' + val.lat + '|' + val.lng + '">'+ name + ': ' + path + '</option>');
			});

		});
}

/*
 *
 */
export function osmSearch() {
	var place = $('#search').val();
	if (isEmptyString(place)) {
		$('#tgnOsmList').html("");
		return;
	}
    var searchString = '<p></p><p>Searching for "' + place + '" in OpenStreetMap...</p>';
    var foundString = '<p></p><p>Places found for "' + place + '" in OpenStreetMap:</>';

    $('#tgnOsmList').html(searchString);
	$.getJSON(osmSearchUrl, {q : place, format : 'json'}).success(function(data) {
		if (data.length < 1) {
			$('#tgnOsmList').html('<p></p><p><strong>No results</strong> found for "' + place + '" in OpenStreetMap.</p>');
			return;
		} else {
            $('#tgnOsmList').html(foundString);
        }

		var elemId = 'osm_' + getBase64Without(place);
        var elem = '<p></p><div>'
            + '<select id="' + elemId + '" style="width:98%;"></select>'
            + '</div>';

		$('#tgnOsmList').html(foundString + elem);
		$('#' + elemId).change(showOSMapPlace);
		var id = '';
		$.each(data, function(index, val){
			if (index === 0 ) {
				setOSMapCenter(id, val.lat, val.lon, 12);
			}
			$('#' + elemId).append('<option value="' + val.display_name + '|' + id + '|' + val.lat + '|' + val.lon + '">' + val.display_name+'</option>');
		});
	});
}

function setNotFoundMessage(elemId){
	$('#' + elemId).append('<option value="' + noResults + '">' + noResults + '</option>');
	$('#setAll-' + elemId).prop('disabled', true);
}

function showGeolocatorFailError(place, error){
	var source = $('#geolocationSource').val();
	var errorMessage = 'Error when trying to resolve this value using '+source;
	if (error) errorMessage += ': <br/><i>'+error+'</i>';
	var elem = '<div><h4 style="margin-bottom:3px;">' + place + '</h4>'
		+ '<p>'+errorMessage+'</p></div>';
	$('#tgnList').append(elem);
}

function showNoAddressesError(){
	var selection = getTable().getSelectedLast();
	if (selection){
		newAlert('error', 'No address values in current selection!', '<p>No address column values found in selected cells. If you want to process the whole data set, clear the current selection first. <br>' +
			'Otherwise, select table rows with non-empty values in the <i>Address</i> column.</p>'+
			'<button class="btn btn-small btn-primary" id="clearSelectionFromMsg" title="Clear selection">Clear selection</button>',
			'geocodeAlert');
		$('#clearSelectionFromMsg').bind('click', function () {clearTableSelection();$('.geocodeAlert').alert('close');});
	} else newAlert('error', 'No data available for Geolocation!', '<p>No address values found in column <i>Address</i>.' + '</p>', 'geocodeAlert');
}

/**
 * Sets a selected result using the 'Set' button in place selection.
 */
function setAllPlaces() {

	var id = $(this).attr('id').split('-')[1];
	var selectedId = '#' + id;
	var origPlaceId = '#tgnOrigPlace-' + id;
	var origPlace = $(origPlaceId).val();
	var vals = $(selectedId).val().split('|');

	//var name = vals[0]; // name is not used for identifying the address to change anymore, maybe useful later.
	var gettyId = vals[1];
	var lat = vals[2];
	var long = vals[3];

	updateCoordinates(origPlace, lat, long, gettyId, true);
}

/**
 * Sets a selected result using the 'Set' button in map selection.
 */
export function setAllPlacesOSM() {

	var name = $('#osmPlaceName').val().replace('_', ' ');
	var gettyId = $('#osmID').val();
	var lat = $('#osmLat').val();
	var long = $('#osmLong').val();

	updateCoordinates(name, lat, long, gettyId, true);
}
/*
 *
 */
function setOSMapCenter(id, lat, long, zoomLevel) {

	$('#osmLat').val(lat);
	$('#osmLong').val(long);
	$('#osmID').val(id);

	// no valid coordinates
	if (!lat && !long) lat = long = zoomLevel = 0;

	// place marker
	const mapCenter = fromLonLat([long, lat]);
	map.getView().setCenter(mapCenter);
	map.getView().setZoom(zoomLevel);

	if (typeof(locationPickerVector) !== 'undefined') locationPickerVector.clear();
	if (typeof(dragAndDrop) !== 'undefined') map.removeInteraction(dragAndDrop);

	var locationPickerPoint = new Point(fromLonLat([long, lat]));
	var markerStyle = new Style({
		image: new Icon({
			anchor: [0.42, 28],
			offsetOrigin: 'top-left',
			anchorXUnits: 'fraction',
			anchorYUnits: 'pixels',
			src: 'img/flag.png'
		})
	});
	var iconFeature = new Feature({
		geometry: locationPickerPoint,
		name: 'Markierter Punkt'
	});
	iconFeature.setStyle(markerStyle);
	locationPickerVector = new VectorSource({features: [iconFeature]});
	var locationPickerLayer = new VectorLayer({source: locationPickerVector});
	map.addLayer(locationPickerLayer);
	dragAndDrop = new Modify({
		features: new Collection([iconFeature]),
		pixelTolerance: 20
	});
	map.on("pointermove", function (evt) {
		if (evt.dragging) return;
		var featureHover = this.forEachFeatureAtPixel(evt.pixel, function() {
			return true;
		});
		if (featureHover) this.getTargetElement().style.cursor = 'pointer';
		else this.getTargetElement().style.cursor = '';
	});
	map.addInteraction(dragAndDrop);
	var selectedPositionAsLonLat;
	iconFeature.on('change', function(evt){
		if (evt.dragging) return;
		selectedPositionAsLonLat = toLonLat(this.getGeometry().getCoordinates());
		$('#osmLong').val(selectedPositionAsLonLat[0]);
		$('#osmLat').val(selectedPositionAsLonLat[1]);
	}, iconFeature);

}

/*
 *
 */
function pickPlace() {
	var lat, long, zoomLevel;
	var id = $(this).attr('id').split('-')[1];
	var selectedId = '#' + id;
	var origPlaceId = '#tgnOrigPlace-' + id;
	var origPlace = $(origPlaceId).val();
	if ($(selectedId).val() === null) {
		lat = 0;
		long = 0;
		zoomLevel = 0;
	} else {
		var vals = $(selectedId).val().split('|');
		var name = vals[0];
		if (name === noResults) {
			name = origPlace;
		}
		id = vals[1];
		lat = vals[2];
		long = vals[3];
		zoomLevel = 12;
	}

	setOSMapCenter(id, lat, long, zoomLevel);
	$('#osmPlaceName').val(origPlace);
	$('#search').val(name);
	$('#tgnOsmList').empty();
}

/*
 *
 */
function placeChanged() {

	var origPlaceId = '#tgnOrigPlace-' + $(this).attr('id');
	var origPlace = $(origPlaceId).val();

	var vals = $(this).val().split('|');
	var address = vals[0];
	var id = vals[1];
	var lat = vals[2];
	var long = vals[3];

	setOSMapCenter(id, lat, long, 12);
	$('#osmPlaceName').val(origPlace);
	$('#search').val(address);
	$('#tgnOsmList').empty();
}

/*
 *
 */
function showOSMapPlace() {
	var vals = $(this).val().split('|');
	// var name = vals[0];
    var id = vals[1];
	var lat = vals[2];
	var long = vals[3];

	setOSMapCenter(id, lat, long, 12);
}


export function checkCoordinates() {
	$(".coordAlert").alert('close');
	var arr = getTable().getData();
	var latErrorInRow = [];
    var longErrorInRow = [];
    var errorObjects = [];

	var latCol = findColumn(arr, 'Latitude');
    var longCol = findColumn(arr, 'Longitude');

    var result = true;
	$(arr).each(function(rowIndex, rowData) {
		// Do not check header and empty rows.
        if (rowIndex !== 0 && !rowIsEmpty(rowData)) {
			var rvla = isValidLat(rowIndex, rowData, latCol);
			if (!rvla) {
				var errorLatObj= errLink(rowIndex, latCol);
                latErrorInRow.push(errorLatObj.tag);
				errorObjects.push(errorLatObj);
                result = rvla;
            }
			var rvlo = isValidLong(rowIndex, rowData, longCol);
			if (!rvlo) {
				var errorLongObj = errLink(rowIndex, longCol);
				longErrorInRow.push(errorLongObj.tag);
				errorObjects.push(errorLongObj);
                result = rvlo;
            }
        }
	});

	if (!result) {
        newAlert('error', 'Invalid coordinates', '<p>Some geocoordinates are out of range or missing! Longitude may be between -180 and +180, Latitude beetween -90 and +90. Both should not be empty to get correct results in the Geo-Browser. ' +
			'Details:' + '</p><p>Column/row (long): ' + longErrorInRow.join(", ") + '</p><p>Column/Row (lat): ' + latErrorInRow.join(", ") + "</p>" +
			'<button id="showAnyway" class="btn btn-small btn-danger pull-right" title="Open in Geo-Browser anyway!">I am aware of that, please open anyway!</button>', 'coordAlert');
		$('#showAnyway').bind('click', function () {openGeoBrowser(true,false);});
		$.each(errorObjects, function(index, value){
			$('#'+value.id).bind('click', function (e) {e.preventDefault(); getTable().selectCell(value.row, value.col)});
		});
	}

	return result;
}

/*
 *
 */
export function errLink(row, col) {
	var displayRow = row+1;
	var displayCol = getTable().getColHeader(col);
	var linkId = 'jumpTo'+row+col;
	var link = '<a id="'+linkId+'" href="">'+displayCol+displayRow+'</a>';

	return {tag: link, id: linkId, row: row, col: col};
}

/**
 * Check if rowData contains a valid Latitude at rowIndex and ColumNumber, mark errors in table.
 *
 * @param rowIndex
 * @param rowData
 * @param columnNumber
 * @return boolean if row was valid
 */
function isValidLat(rowIndex, rowData, columnNumber) {

	var entry = rowData[columnNumber];
	var isValid = false;
	if (!isEmptyString(entry)) {
		isValid = (entry >= -90 && entry <= 90);
		if (!isValid) {
			getTable().selectCell(rowIndex, columnNumber);
			getTable().getCell(rowIndex, columnNumber).className = 'error';
		}
	}

	return isValid;
}

/**
 * Check if rowData contains a valid Latitude at rowIndex and ColumNumber, mark errors in table.
 *
 * @param rowIndex
 * @param rowData
 * @param columnNumber
 * @return boolean if row was valid
 */
function isValidLong(rowIndex, rowData, columnNumber) {

	var entry = rowData[columnNumber];
	var isValid = false;
	if (!isEmptyString(entry)) {
		isValid = (entry >= -180 && entry <= 180);
		if (!isValid) {
			getTable().selectCell(rowIndex, columnNumber);
			getTable().getCell(rowIndex, columnNumber).className = 'error';
		}
	}

	return isValid;
}

/**
 * Disable Geolocation completion button etcpp
 */
function geolocationCompletionStart() {
  $('#geolocationCompletionButton').prop('disabled', true);
  $('#spinningGeolocationCompletionFlower').removeClass('hide');
}

/**
 * Enable Geolocation completion button etcpp
 */
function geolocationCompletionComplete() {
  $('#geolocationCompletionButton').prop('disabled', false);
  $('#spinningGeolocationCompletionFlower').addClass('hide');
}

/**
 * Get a valid NCName here for the XML IDs to address the place name in the place selection list.
 * We use Base64 here by replacing the three of '+', '/', and '='! Dirty, dirty! I'll fix it just
 * tomorrow! Mist! Heute ist tomorrow! ARGL!
 *
 * NOTE: We can cope with UTF8 chars also now!
 */
export function getBase64Without(theID) {
    try {
        return btoa(encode_utf8(theID)).replaceAll('+', '').replaceAll('/', '').replaceAll('=', '');
    }
    catch(err) {
        console.error("error with id: " + theID + "! " + err);
    }
}

/**
 * Thanks to: https://web.archive.org/web/20200226002830/http://ecmanaut.blogspot.com/2006/07/encoding-decoding-utf8-in-javascript.html !
 */
function encode_utf8(theString) {
    return unescape(encodeURIComponent(theString));
}
