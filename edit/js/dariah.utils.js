/**
 * Utility functions
 */
import {defaultColumnHeaders} from "./dariah.workflow.conf";
import {dariahSTATUSURL} from "../../js/dariah.geobro.conf";
import {getTable} from "./dariah.workflow.table";

/**
 * Dump things
 */
export function dump() {
	var arr = getTable().getData();
	console.log(arr2csv(arr));
}

/**
 * Convert array to CSV.
 *
 * TODO: Also look at http://www.uselesscode.org/javascript/csv/ and at handsontable csv2array
 * function http://www.filosophy.org/bitstream/2011/12/17/js_array_to_csv.html
 */
export function arr2csv(arr){
    var csv = '';
    for(var row in arr){
        for(var col in arr[row]){
            var value = arr[row][col];
            if (value) value.replace(/"/g, '""');
            else value = '';
            csv += '"' + value + '"';
            if(col !== arr[row].length - 1){
                csv += ",";
            }
        }
        csv += "\n";
    }
    return csv;
}



/**
 * Check for empty string.
 */
export function isEmptyString(str) {
    return (!str || ! str.length);
}

/**
 * Append a new alert frame.
 *
 * @param {String} type  Type of alert, one of: "warning", "error", "success", or "info".
 * @param {String} title Title of the alert.
 * @param {String} message The alert's message.
 * @param {String} extraClass Adds an extra class attribute to the alert div (optional).
 * @param {String} extraID Give the alert div an extra ID (optional).
 * @param {String} area Sets the HTML ID of the alert to be showed (optional, default is "alertArea").
 * @param {boolean} nodate If undefined, date will be appended to alert message (optional, default is true with current locale date and time)
 */
export function newAlert(type, title, message, extraClass, extraID, area, nodate) {
 	if (extraClass === undefined) {
        extraClass = '';
    }
    var id = '';
    if (extraID !== undefined && extraID !== "") {
        id = 'id="' + extraID + '" ';
    }

    var date;
    if (nodate === undefined || nodate === "") {
        date = ' <small>[' + new Date().toLocaleString() + ']</small>';
    }
    else {
        date = '';
    }

 	var elem = '<div ' + id + 'class="alert alert-' + type + ' alert-block fade in ' + extraClass + ' ">' + '<a class="close" data-dismiss="alert" href="#">×</a>';
 	if (title !== "") {
        elem += '<h2 class="">' + title + '</h2>';
    }
 	elem += message + date + '</div>';

    if (area === undefined || area === "") {
        area = "alertArea";
    }
    $("#" + area).append($(elem));
}

/**
 * Get status HTML from DARIAH status Github page <https://dariah-de.github.io/status/>.
 */
export function getEmbeddedDariahStatus() {

    fetch(dariahSTATUSURL)
        .then(function (response) {
    	return response.text();
    }).then(function (data) {
        var domParser = new DOMParser();
        var errorHtml = domParser.parseFromString(data, 'text/html')
            .querySelector('.geobrowser_status.dariah-status-message-error');
        var warningHtml = domParser.parseFromString(data, 'text/html')
            .querySelector('.geobrowser_status.dariah-status-message-warning');
        if (errorHtml) {
            $('#dariah-status-message-text').append(errorHtml);
            $('#dariah-status-message-area').removeClass('hide');
        }
        if (warningHtml) {
            $('#dariah-status-message-text').append(warningHtml);
            $('#dariah-status-message-area').removeClass('hide');
        }
    }).catch(function (err) {
    	console.warn('ERROR fetching DARIAH-DE status!', err);
    });
}

/**
 * We do check here, if we have ONE of the e4dHeader titles in the first line of the file to load.
 *
 * @param {String} firstLineOfData The first line of the data object.
 *
 * NOTE It is only a (more or less and more more than less) sophisticated guess if we have got a Datasheet file here!
 */
export function checkIfDatasheetEditorFile(firstLineOfData) {
    var headerCount = 0;
    defaultColumnHeaders.forEach(function (val, index) {
        if (firstLineOfData.includes(`${val}`)) {
            headerCount++;
        }
    });
    return headerCount > 0;

}
