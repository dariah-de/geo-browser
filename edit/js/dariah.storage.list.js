/**
 * Global variables
 */
import {readToken} from "../../js/sessionStorageWrapper";
import {storageURL} from "../../js/dariah.geobro.conf";
import {
    getOwnStorageElementList,
    setOwnStorageElementList,
    setDSID,
    bearerPrefix, logIDPrefix, allowedMimeTypes,
    ownStorageElementListSize,
} from "./dariah.workflow.conf";
import {newAlert} from "./dariah.utils";
import {authenticate, setTokenDSE, unpublishAndDeleteDataset} from "./dariah.storage";
var numberOfHEADCallsCompleted; // Used to determine if paginations can be enabled after loading.
/**
 * Load user's file OwnStorage list from DARIAH-DE storage to manage user's OwnStorage files.
 */
export function loadOwnStorageFileList() {
    // Assemble bearer token and logID.
    if (!readToken() && window.location.hash) {
        setTokenDSE();
    }
    var token = bearerPrefix + readToken();
    var logID = logIDPrefix + (new Date()).getMilliseconds();
    $.ajax({
		url: storageURL + "/list",
		type: 'GET',
        headers: { 'Authorization': token, 'X-Transaction-ID': logID },
		success: function(data) {
            setOwnStorageElementList(data);
            $('#deleteFromListButton').off('click').on('click', function() {unpublishAndDeleteDataset()});
            // Get page from URL param, if existing.
            var searchParams = new URLSearchParams(window.location.search)
            var pageFromURL = searchParams.get('page');
            var pageList = 1;
            if (pageFromURL) {
                pageList = pageFromURL;
            }
            updateList(pageList);
		},
        error: function(xhr, textStatus, errorThrown) {
            // Have we got a token already? If not, just do authenticate first!
            if (readToken() === null) {
                authenticate('list=list');
            }
            // If a token does exist and no read access is granted, just give the correct error.
            else {
                $('#spinningFileListFlower').addClass('hide');
                var title = 'Error loading file list: ' + xhr.status + ' ' + errorThrown + '!';
                var message = '<p>The file list could not be loaded from the DARIAH-DE Storage!</p>';
                newAlert('error', title,  message);
            }
		}
	});
}

/*
 *
 */
function createPaginationAndListElements(currentPage) {
    // TODO Check again for auth here?
    // Do some computations and saveguards.
    var ownStorageElementList = getOwnStorageElementList();
    var pages = Math.floor(ownStorageElementList.length / ownStorageElementListSize) + 1;
    if (currentPage > pages) {
        currentPage = pages;
    } else if (currentPage < 1) {
        currentPage = 1;
    }
    var start = currentPage * ownStorageElementListSize - ownStorageElementListSize + 1;
    var end = start + ownStorageElementListSize - 1;
    if (end > ownStorageElementList.length) {
        end = ownStorageElementList.length;
    }
    var amountOfElements = end - start + 1;

    // Create pagination.
    var firstPageRest = 'class="hide" href="" title="Go to first page">&laquo;</a>';
    var prevPageRest = 'class="hide" href="" title="Go to previous page [page ' + (currentPage - 1) + ']">&lsaquo;</a>';
    var middle = '<li class="active"><span><strong>' + start + '</strong>-<strong>' + end + '</strong> of ' + ownStorageElementList.length  + ' [page <strong>' + currentPage + '</strong> of ' + pages + ']</span></li>';
    var nextPageRest = 'class="hide" href="" title="Go to next page [page ' + (currentPage + 1) + ']">&rsaquo;</a>';
    var lastPageRest = 'class="hide" href="" title="Go to last page">&raquo;</a>';
    var upperPagination = '<ul>' +
        '<li id="firstPageUP" class="disabled"><span id="firstPageSpanUP">&laquo;</span><a id="firstPageAUP" class="hide" href="#" title="Go to first page">&laquo;</a></li>' +
        '<li id="prevPageUP" class="disabled"><span id="prevPageSpanUP">&lsaquo;</span><a id="prevPageAUP" ' + prevPageRest + '</li>' +
        middle +
        '<li id="nextPageUP" class="disabled"><span id="nextPageSpanUP">&rsaquo;</span><a id="nextPageAUP" ' + nextPageRest + '</li>' +
        '<li id="lastPageUP" class="disabled"><span id="lastPageSpanUP">&raquo;</span><a id="lastPageAUP" ' + lastPageRest + '</li>' +
    '</ul>';
    var lowerPagination = '<ul>' +
        '<li id="firstPageLP" class="disabled"><span id="firstPageSpanLP">&laquo;</span><a id="firstPageALP" ' + firstPageRest + '</li>' +
        '<li id="prevPageLP" class="disabled"><span id="prevPageSpanLP">&lsaquo;</span><a id="prevPageALP" ' + prevPageRest + '</li>' +
        middle +
        '<li id="nextPageLP" class="disabled"><span id="nextPageSpanLP">&rsaquo;</span><a id="nextPageALP" ' +  nextPageRest + '</li>' +
        '<li id="lastPageLP" class="disabled"><span id="lastPageSpanLP">&raquo;</span><a id="lastPageALP" ' + lastPageRest + '</li>' +
    '</ul>';

    // Define table header.
    var tableHeaderAndFooter = '<tr>' +
        '<th>No</th>' +
        '<th>ID</th>' +
        '<th>Mimetype</td>' +
        '<th>Last modified</th>' +
        '<th>Status</th>' +
        '<th colspan="3">Actions</th>' +
    '</tr>';

    // Define table heading.
    var elements = '<table class="table table-striped">' + tableHeaderAndFooter;
    // Loop over things and create table.
    numberOfHEADCallsCompleted = 0;
    var listedIDs = [];
    for (var i = start - 1; i < end; i++) {
        var ownStorageID = ownStorageElementList[i].id;
        listedIDs.push(ownStorageID);
        // Gather metadata information.
        getHTTPHeaderInformation(ownStorageID, start, end, currentPage, pages, amountOfElements);

        // Gather needed terms.
        var privateOrShared = "<td>private</td>";
        if (ownStorageElementList[i].public) {
            privateOrShared = '<td style="color:green;font-weight:bold;">shared</td>';
        }
        // Add file element table entries.
        elements +=
            '<tr>' +
                '<td><strong>' + (i + 1) + '</strong></td>' +
                '<td>' + ownStorageID + '</td>' +
                '<td id="type-' + ownStorageID + '"><img src="https://res.de.dariah.eu/dhrep/img/spinning-flower_slow.gif" alt="Loading type of ID "' + ownStorageID + '" width="16" /> &nbsp; <span style="color:orange;">loading...</span></td>' +
                '<td id="date-' + ownStorageID + '"><img src="https://res.de.dariah.eu/dhrep/img/spinning-flower_slow.gif" alt="Loading date of ID "' + ownStorageID + '" width="16" /> &nbsp; <span style="color:orange;">loading...</span></td>' +
                privateOrShared +
                '<td><button id="show-' + ownStorageID + '" class="btn btn-small btn disabled" disabled="true" onclick="window.location.href=\'./index.html?id=' + ownStorageID + '\'" title="Open this dataset in the Datasheet Editor">Show as datasheet</button></td>' +
                '<td><button id="download-' + ownStorageID + '" class="btn btn-small btn" title="Download this file or dataset">Download</button></td>' +
                '<td><button id="del-' + ownStorageID + '" class="btn btn-small btn-danger disabled" disabled="true" title="Delete this file or dataset!">Delete</button></td>' +
            '</tr>';
    }
    elements += tableHeaderAndFooter + '</table>';

    $('#fileArea.inner').replaceWith( '<div id="fileArea" class="inner">' +
        '<div class="pagination pagination-centered">' + upperPagination + '</div>' +
        '<div>' + elements + '</div>' +
        '<div class="pagination pagination-centered">' + lowerPagination + '</div>' +
        '</div>' );

    $('#firstPageAUP').on("click", function(e){e.preventDefault(); updateList(1);});
    $('#firstPageALP').on("click", function(e){e.preventDefault(); updateList(1);});
    $('#prevPageAUP').on("click", function(e){e.preventDefault(); updateList(currentPage-1);});
    $('#prevPageRest').on("click", function(e){e.preventDefault(); updateList(currentPage-1);});
    $('#nextPageAUP').on("click", function(e){e.preventDefault(); updateList(currentPage+1);});
    $('#nextPageRest').on("click", function(e){e.preventDefault(); updateList(currentPage+1);});
    $('#lastPageAUP').on("click", function(e){e.preventDefault(); updateList(pages);});
    $('#lastPageRest').on("click", function(e){e.preventDefault(); updateList(pages);});

    $.each(listedIDs, function(index, ownStorageID) {
        $('#download-' + ownStorageID).bind("click", function () {
            downloadFromDariahStorage(ownStorageID);
        });
        $('#del-' + ownStorageID).bind("click", function () {
            deleteDatasetLIST(ownStorageID);
        });
    });

}

/*
 *
 */
function updateList(currentPage) {
    if (getOwnStorageElementList() !== "") {
        $('#spinningFileListFlower').removeClass('hide');
        createPaginationAndListElements(currentPage);
        history.replaceState(null, 'list', "?page="+currentPage);
    } else console.error("NO LIST!");

}

/**
 * Gets metadata from the storage file's HTTP header.
 */
function getHTTPHeaderInformation(id, start, end, currentPage, pages, amountOfElements) {
    // Assemble bearer token and logID.
    var token = bearerPrefix + readToken();
    var logID = logIDPrefix + (new Date()).getMilliseconds();
    $.ajax({
		url: storageURL + id,
        type: 'GET',
        headers: { 'Authorization': token, 'X-Transaction-ID': logID },
		success: function(data, status, xhr) {
            var contentType = xhr.getResponseHeader("content-type");
            var lastModified = xhr.getResponseHeader("last-modified");

            // Set element content.
            $('#type-' + id).text(contentType);
            $('#date-' + id).text(lastModified);
            if (allowedMimeTypes.includes(contentType)) {
                $('#show-' + id).removeClass('disabled');
                $('#show-' + id).attr('disabled', false);
                $('#del-' + id).removeClass('disabled');
                $('#del-' + id).attr('disabled', false);
            } else {
                $('#show-' + id).addClass('disabled');
                $('#show-' + id).attr('disabled', true);
                $('#del-' + id).addClass('disabled');
                $('#del-' + id).attr('disabled', true);
            }

            numberOfHEADCallsCompleted++;

            // Set pagination content.
            if (numberOfHEADCallsCompleted >= amountOfElements) {
                enablePaginationControls(start, end, currentPage, pages);
            }
		},
        error: function(xhr, textStatus, errorThrown) {
            // Have we got a token already? If not, just do authenticate first!
            // if (readToken() === null) {
            //   authenticate('id=' + id);
            // }
            $('#type-' + id).text(xhr.status + " " + textStatus);
            $('#type-' + id).addClass('error');
            $('#date-' + id).text(xhr.status + " " + textStatus);
            $('#date-' + id).addClass('error');
            $('#show-' + id).addClass('disabled');
            $('#show-' + id).attr('disabled', true);

            numberOfHEADCallsCompleted++;

            // Set pagination content.
            if (numberOfHEADCallsCompleted >= amountOfElements) {
                enablePaginationControls(start, end, currentPage, pages);
            }
		}
	});
}

/**
 * Download file directly from DARIAH-DE storage using storage ID.
 **/
function downloadFromDariahStorage(id) {
    // Assemble bearer token and logID.
    var token = bearerPrefix + readToken();
    var logID = logIDPrefix + (new Date()).getMilliseconds();
    $.ajax({
		url: storageURL + id,
		type: 'GET',
        headers: { 'Authorization': token, 'X-Transaction-ID': logID },
		success: function(data, status, xhr) {
            // Create new a element.
            var fancyA = document.createElement('a');
            document.body.appendChild(fancyA);
            fancyA.style = 'display:none';
            // Gat data from data.
            var blob = new Blob([data], {type: xhr.getResponseHeader("content-type")});
            var downloadURL = window.URL.createObjectURL(blob);
            // Fill a element with URL and filename.
            fancyA.href = downloadURL;
            console.log(xhr.getResponseHeader("content-type"));
            fancyA.download = id+'.csv';
            // Click on a :-)
            fancyA.click();
            // Remove fancy a element.
            document.body.removeChild(fancyA);
		},
        error: function(xhr, textStatus, errorThrown) {
            // Have we got a token already? If not, just do authenticate first!
            if (readToken() === null) {
                // TODO Do test this download without a valid token!
                // authenticate('id=' + id);
            }
            // If a token does exist and no read access is granted, just give the correct error.
            else {
                var title = 'Error downloading dataset: ' + xhr.status + ' ' + errorThrown + '!';
                var message = '<p>The dataset with ID <i>' + id + '</i> could no be downloaded.</p>';
                newAlert('error', title,  message);
            }
		}
	});
}

/*
 *
 */
function deleteDatasetLIST(id) {
    if (readToken() !== null) {
        // Set dsidLIST to ID to delete!
        setDSID(id);
        $('#askForListDeletion').modal('show');
    } else {
        authenticate('action=delete');
    }
}

/**
 *
 */
function enablePaginationControls(start, end, currentPage, pages) {
    $('#spinningFileListFlower').addClass('hide');
    if (start > 1) {
        $('#firstPageUP').removeClass('disabled');
        $('#firstPageSpanUP').addClass('hide');
        $('#firstPageAUP').removeClass('hide');
        $('#prevPageUP').removeClass('disabled');
        $('#prevPageSpanUP').addClass('hide');
        $('#prevPageAUP').removeClass('hide');
        $('#firstPageLP').removeClass('disabled');
        $('#firstPageSpanLP').addClass('hide');
        $('#firstPageALP').removeClass('hide');
        $('#prevPageLP').removeClass('disabled');
        $('#prevPageSpanLP').addClass('hide');
        $('#prevPageALP').removeClass('hide');
    } else {
        // Leave first two buttons (a) hidden and disabled!
    }
    if (currentPage < pages) {
        $('#nextPageUP').removeClass('disabled');
        $('#nextPageSpanUP').addClass('hide');
        $('#nextPageAUP').removeClass('hide');
        $('#lastPageUP').removeClass('disabled');
        $('#lastPageSpanUP').addClass('hide');
        $('#lastPageAUP').removeClass('hide');
        $('#nextPageLP').removeClass('disabled');
        $('#nextPageSpanLP').addClass('hide');
        $('#nextPageALP').removeClass('hide');
        $('#lastPageLP').removeClass('disabled');
        $('#lastPageSpanLP').addClass('hide');
        $('#lastPageALP').removeClass('hide');
    } else {
        // Leave last two buttons (a) hidden and disabled!
    }
}
