// Regular expressions for datecheck, valid are xsd:date, xsd:dateTime, xsd:gYear and xsdGYearMonth.
import {isEmptyString, newAlert} from "./dariah.utils";
import {findColumn, getTable} from "./dariah.workflow.table";
import {errLink} from "./dariah.workflow.geo";

export const xsdGYear = /^(-?)(\d*)$/;
export const xsdGYearMonth = /^(-?)(\d*)-(0\d|1(0|1|2))$/;
// Date or DateTime (http://www.experts-exchange.com/Programming/Languages/Scripting/JavaScript/Q_21889287.html)
// Added month schema from xsdYearMonth, see above. Day schema was not changed... --fu
export const xsdDateOrDateTime = /(-)?(\d*)-(0\d|1(0|1|2))-(\d{2})(T(\d{2}):(\d{2}):(\d{2})(\.\d+)?)?(([\+-])((\d{2}):(\d{2})))?/;
var gYearSchemaUrl = 'https://books.xmlschemata.org/relaxng/ch19-77127.html';
var gYearMonthSchemaUrl = 'http://books.xmlschemata.org/relaxng/ch19-77135.html';
var dateSchemaUrl = 'http://books.xmlschemata.org/relaxng/ch19-77041.html';
var dateTimeSchemaUrl = 'http://books.xmlschemata.org/relaxng/ch19-77049.html';

/**
 * Check dates :-)
 */
 export function checkDates() {
	$(".dateAlert").alert('close');

	var arr = getTable().getData();
    var errorInRow = [];

	var timeStampCol = findColumn(arr, 'TimeStamp');
	var timeSpanBCol = findColumn(arr, 'TimeSpan:begin');
	var timeSpanECol = findColumn(arr, 'TimeSpan:end');

	var result = true;

	$(arr).each(function(rowIndex, rowData) {
		// don't check the header
		if(rowIndex !== 0) {
			var rvd1 = isValidDate(rowIndex, rowData, timeStampCol);
            result = rvd1 && result;
            if (!rvd1) errorInRow.push(errLink(rowIndex, timeStampCol));
			var rvd2 = isValidDate(rowIndex, rowData, timeSpanBCol);
            result = rvd2 && result;
            if (!rvd2) errorInRow.push(errLink(rowIndex, timeSpanBCol));
			var rvd3 = isValidDate(rowIndex, rowData, timeSpanECol);
            result = rvd3 && result;
            if (!rvd3) errorInRow.push(errLink(rowIndex, timeSpanECol));
            // Allow empty time fields by commenting out the following:
            //if (!(rvd1 && rvd2 && rvd3)) errorInRow.push(rowIndex + 1);
		}
	});

	if(!result) {
        var title = 'Invalid date format';
        var tags = errorInRow.map(err => err.tag);
        var message = '<p>Some date fields have an unrecognized date format. Date needs to be <a target="_blank" href="' + gYearSchemaUrl + '">xsd:gYear</a>, <a target="_blank" href="' + gYearMonthSchemaUrl + '">xsd:gYearMonth</a>, <a target="_blank" href="' + dateSchemaUrl + '">xsd:date</a> or <a target="_blank" href="' + dateTimeSchemaUrl + '">xsd:dateTime</a> to be interpreted correctly with the Geo-Browser!</p>' +
        '<p>Date format error in row(s): ' + tags.join(", ") + '</p>';
		newAlert('error', title, message, 'dateAlert');
		$.each(errorInRow, function(index, value){
			$('#'+value.id).bind('click', function (e) {e.preventDefault(); getTable().selectCell(value.row, value.col)});
		});
	}

	return result;
}

/**
 * Check if rowData contains a valid datetime at rowIndex and ColumNumber, mark errors in table.
 *
 * @param rowIndex
 * @param rowData
 * @param columnNumber
 * @return boolean if row was valid
 */
function isValidDate(rowIndex, rowData, columnNumber) {
		var entry = rowData[columnNumber];
		var isValid = true;
		if(!isEmptyString(entry)) {
			isValid = (xsdGYear.test(entry) || xsdGYearMonth.test(entry) || xsdDateOrDateTime.test(entry));
			if(!isValid) {
				getTable().selectCell(rowIndex, columnNumber);
                getTable().getCell(rowIndex, columnNumber).className = 'error';
			}
		}

		return isValid;
}
