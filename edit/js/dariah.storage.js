/**
 * Functions dealing with DARIAH-DE Storage upload, download, sharing, unsharing,
 * and deleting.
 *
 * TODO: jquery object
 */
import {storageURL, pdpURL, oldStorageURL} from "../../js/dariah.geobro.conf";
import {
    bearerPrefix, logIDPrefix, csvStorageMimetype, allowedMimeTypes, defaultColumnHeaders,
    setDSID, getDSID, setOwnStorageElementList,
} from "./dariah.workflow.conf";
import {checkIfDatasheetEditorFile, newAlert} from "./dariah.utils";
import {
    clearStorage,
    migrationIsSet, publicIsSet,
    readToken,
    removeMigration, removePublic,
    setMigration,
    setPublic,
    setToken
} from "../../js/sessionStorageWrapper";
import {
    setWorkingConsoleMessage,
    doThingsOnIDExistingCheck,
    showFirstStartWizard,
    csvToReadOnlyTable,
    isOldStorageID,
    newSheetAlert,
    setPermanentConsoleMessage,
    csvToTable,
    doThingsOnOldStorageID,
    setSharedUnsharedStatusOnScreen
} from "./dariah.workflow";


/*
 * Create new file, get storage file ID.
 */
export function postToDariahStorage(postdata) {
    // Check for migration from old storage.
    var dsid = getDSID();
    if (isOldStorageID(dsid)) {
        setMigration();
    } else {
        removeMigration();
    }
    // Assemble bearer token and logID.
    var token = bearerPrefix + readToken();
    var logID = logIDPrefix + (new Date()).getMilliseconds();
    setWorkingConsoleMessage('Creating...');
    // Disable create and import buttons to prevent double-clicking on them.
    $('#newButton').addClass('disabled');
    $('#importButton').addClass('disabled');
    $(".fileImportError").alert('close');
    // Create new file.
    $.ajax({
		url: storageURL,
		type: 'POST',
        headers: { 'Authorization': token, 'X-Transaction-ID': logID },
		contentType: csvStorageMimetype,
		data: postdata,
		success: function(data, status, xhr) {
			//console.log('loc: ' + xhr.getResponseHeader('Location'));
			var location = xhr.getResponseHeader('Location');
			setDSID(location.substring(location.lastIndexOf('/') + 1));
            // Reload dataset if just migrated.
            var redirectURL = window.location.origin + window.location.pathname + '?id=' + getDSID();
            if (migrationIsSet()) {
                document.location.href = redirectURL;
            }
            // No migration: Just change the URL here, do NO reload of page to stay in correkt workflow!
            // Sorry for the urgli:murgli! Really don't know what it's supposed to mean! :-D
            else {
                history.replaceState(
                    { urgli: 'murgli' }, 'id', redirectURL
                );
                newSheetAlert();
                doThingsOnIDExistingCheck();
                // Enable create and import buttons again.
                // Enable create and import buttons again.
                $('#newButton').removeClass('disabled');
                $('#importButton').removeClass('disabled');
            }
		},
        error: function(xhr, textStatus, errorThrown) {
            var title = 'Error creating dataset: ' + xhr.status + ' ' + errorThrown + '!';
            var message = '<p>No new dataset could be created!</p>';
            newAlert('error', title,  message);
            // Re-set storage ID.
        	setDSID("")
            setPermanentConsoleMessage(title);
		}
	});
}

/**
 * Load file from DARIAH-DE storage using storage ID.
 */
export function loadFromDariahStorage(id) {
    // Assemble bearer token and logID.
    var token = bearerPrefix + readToken();
    var logID = logIDPrefix + (new Date()).getMilliseconds();
    setWorkingConsoleMessage('Loading...');
    $.ajax({
		url: storageURL + id,
		type: 'GET',
        headers: { 'Authorization': token, 'X-Transaction-ID': logID },
		success: function(data, status, xhr) {
            var responseMimetype = xhr.getResponseHeader("content-type");
            var firstLineOfData = data.substring(0, data.indexOf('\n'));
            if (!allowedMimeTypes.includes(responseMimetype)) {
                var title = 'File has incorrect mimetype!';
                var message = '<p>The dataset with ID <i>' + id + '</i> (mimetype ' + responseMimetype+ ') can not be loaded into the Datasheet Editor: The mimetype must be one of the following: <strong>' + allowedMimeTypes + '</strong></p>';
                newAlert('error', title,  message);
                // Re-set storage ID.
        	    setDSID("")
                doThingsOnIDExistingCheck();
                showFirstStartWizard();
            }
            else if (!checkIfDatasheetEditorFile(firstLineOfData)) {
                var title = 'File has not got the correct Datasheet Editor format!';
                var message = '<p>The dataset with ID <i>' + id + ' (mimetype ' + responseMimetype + ') can not be loaded into the Datasheet Editor: The first line must at least contain one of the following titles: <strong>' + defaultColumnHeaders + '</strong>.</p><p>First line seems to be instead: <strong>' + firstLineOfData + '</strong></p>';
                newAlert('error', title,  message);
                // Re-set storage ID.
                setDSID("");
                doThingsOnIDExistingCheck();
                showFirstStartWizard();
            }
            else {
                doThingsOnIDExistingCheck();
		        csvToTable(data);
            }
		},
        error: function(xhr, textStatus, errorThrown) {
            // Have we got a token already? If not, just do authenticate first!
            if (readToken() === null) {
                console.error("No token! Re-authenticate...");
                authenticate('id=' + id);
            }
            // If a token does exist and no read access is granted, just give the correct error.
            else {
                var title = 'Error loading dataset: ' + xhr.status + ' ' + errorThrown + '!';
                var message = '<p>The dataset with ID <i>' + id + '</i> is not existing or private.</p>';
                newAlert('error', title,  message);
                // Re-set storage ID.
                setDSID("");
                doThingsOnIDExistingCheck();
                showFirstStartWizard();
            }
		}
	});
}

/**
 * Load file from old DARIAH-DE storage, set button states accordingly.
 */
export function loadFromOldDariahStorage(id) {
    setWorkingConsoleMessage('Loading...');
    $.ajax({
 		url: oldStorageURL + id,
 		type: 'GET',
 		success: function(data) {
            doThingsOnOldStorageID();
 			csvToReadOnlyTable(data);
            var title = 'This dataset is now read-only!';
            var message = '<p>The dataset with ID <i>' + id + '</i> is still stored in the old DARIAH-DE Storage and has been rendered read-only! You can either just download it using the <strong>Download dataset</strong> button on the right, <strong>View the dataset</strong> with the Geo-Browser, or just <strong>Create a new dataset</strong> in the DARIAH-DE OwnStorage to migrate and then further edit the data and share it if you wish.</p>';
            newAlert('warning', title,  message);
 		},
         error: function(xhr, textStatus, errorThrown) {
            var title = 'Error loading dataset: ' + xhr.status + ' ' + errorThrown + '!';
            var message = '<p>The dataset with ID <i>' + id + '</i> could not be loaded from the DARIAH-DE Storage!</p>';
            newAlert('error', title,  message);
            // Re-set storage ID.
             setDSID("");
            doThingsOnIDExistingCheck();
            showFirstStartWizard();
 		}
 	});
 }

/**
 * Load file from old DARIAH-DE storage using storage ID, store data to OwnStorage.
 */
export function migrateOldDataset(id) {
    setWorkingConsoleMessage('Migrating...');
    $.ajax({
 		url: oldStorageURL + id,
 		type: 'GET',
 		success: function(data) {
            postToDariahStorage(data);
 		},
        error: function(xhr, textStatus, errorThrown) {
            var title = 'Error loading dataset: ' + xhr.status + ' ' + errorThrown + '!';
            var message = '<p>The dataset with ID <i>' + id + '</i> could not be loaded from the DARIAH-DE Storage!</p>';
            newAlert('error', title,  message);
            // Re-set storage ID.
            setDSID("");
            doThingsOnIDExistingCheck();
            showFirstStartWizard();
 		}
 	});
}

/**
 * Load file from DARIAH-DE storage using storage ID, check shared status.
 * Shared means, every user can read file without token (file is public).
 */
export function checkSharedStatus(id, asyncFunc) {
    // Assemble logID only!
    var logID = logIDPrefix + (new Date()).getMilliseconds();
    // Read file WITHOUT token!
    // FIXME Have we really to check here, if the user is logged in or not!??
    $.ajax({
		url: storageURL + id,
        type: 'GET',
        // FIXME Do we really need to disable cache here?
        cache: false,
        headers: { 'X-Transaction-ID': logID },
    	success: function(response, textStatus, xhr) {
            // Respond according to server status.
            // 200 OK  -->  is shared!
            if (xhr.status === 200) {
                // File is readable without token (public) --> file has been shared (but possibly NOT by us)!
                setPublic();
            }
            else {
                // File is NOT readable without token (not public) --> file has not yet been shared!
                removePublic();
            }
            if (asyncFunc) asyncFunc();
    	},
        error: function(xhr, textStatus, errorThrown) {
            // Respond according to server status:
            // 401 Unauthorized  -->  is private!
            // 404 Not Found  -->  is not existing!
            if (xhr.status === 401 || xhr.status === 404) {
                // File is NOT readable (not public) --> file has not yet been shared!
                removePublic();
            }
            else {
                removePublic();
            }
            if (asyncFunc) asyncFunc();
        }
    });
}

/*
 * Update a dataset (file) using ID and post data.
 */
export function updateDariahStorage(id, postdata) {
    // Assemble bearer token and logID.
    var token = bearerPrefix + readToken();
    var logID = logIDPrefix + (new Date()).getMilliseconds();

    var updateError1 = '<p>The Dataset with ID <i>';
    var updateError2 = '</i> could not be updated to the DARIAH-DE Storage!</p>';

    // Check for shared status first, if true, we have to unshare, write, and share again!
    // ARGL! :-)
    if (publicIsSet()) {
        var error = false;
        var status;
        var thrown;
        // UNSHARE if shared!
        setWorkingConsoleMessage('Unsharing...');
        $.ajax({
        	url: storageURL + id + "/unpublish",
            type: 'POST',
            headers: { 'Authorization': token, 'X-Transaction-ID': logID },
        	success: function(response) {
                // UPDATE if unshared!
                setWorkingConsoleMessage('Updating...');
                $.ajax({
            		url: storageURL + id,
            		type: 'PUT',
                    headers: { 'Authorization': token, 'X-Transaction-ID': logID },
            		contentType: csvStorageMimetype,
            		data: postdata,
                    success: function(response) {
                        setWorkingConsoleMessage('Sharing...');
                        // SHARE if updated!
                        $.ajax({
                    		url: storageURL + id + "/publish",
                            type: 'POST',
                            headers: { 'Authorization': token, 'X-Transaction-ID': logID },
                    		success: function(response) {
                                setPermanentConsoleMessage('All changes saved.');
                    		},
                            error: function(xhr, textStatus, errorThrown) {
                                var title = 'Error updating dataset (share): ' + xhr.status + ' ' + errorThrown + '!';
                                error = true;
                            }
                    	});
            		},
                    error: function(xhr, textStatus, errorThrown) {
                        var title = 'Error updating dataset (update): ' + xhr.status + ' ' + errorThrown + '!';
                        error = true;
                    }
            	});
        	},
            error: function(xhr, textStatus, errorThrown) {
                var title = 'Error updating dataset (unshare): ' + xhr.status + ' ' + errorThrown + '!';
                error = true;
            }
        });
        if (error === true) {
            var message = updateError1 + id + updateError2;
            newAlert('error', title,  message);
            setPermanentConsoleMessage(title);
        }
    }
    // Just do UPDATE if not shared!
    else {
        setWorkingConsoleMessage('Saving...');
        $.ajax({
            url: storageURL + id,
            type: 'PUT',
            headers: { 'Authorization': token, 'X-Transaction-ID': logID },
            contentType: csvStorageMimetype,
            data: postdata,
            success: function(response) {
                setPermanentConsoleMessage('All changes saved.');
            },
            error: function(xhr, textStatus, errorThrown) {
                var title = 'Error updating dataset: ' + xhr.status + ' ' + errorThrown + '!';
                var message = updateError1 + id + updateError2;
                newAlert('error', title,  message);
            }
        });
    }
}

/**
 * Share a dataset using the publish() method of the DARIAH-DE PDP using DARIAH-DE OwnStorage.
 */
export function publishDataset() {
    // Assemble bearer token and logID.
    var token = bearerPrefix + readToken();
    var dsid = getDSID();
    var logID = logIDPrefix + (new Date()).getMilliseconds();
    // Share file.

    /*
    console.log("  ##  PUBLISH: " + storageURL + dsid + "/publish");
    console.log("  ##  TOKEN: " + token);
    console.log("  ##  LOGID: " + logID);
    */

    $.ajax({
		url: storageURL + dsid + "/publish",
        type: 'POST',
        headers: { 'Authorization': token, 'X-Transaction-ID': logID },
		success: function(response) {
            setPublic();
            setSharedUnsharedStatusOnScreen();
            var title = 'Dataset successfully shared!';
            var storageAccessURL =  storageURL + dsid;
            var datasheetURL = window.location.origin + '/edit?id=' + dsid;
            var geobrowserURL = window.location.origin + '?csv1=' + storageAccessURL;
            var message = '<p>The Dataset with ID <i>' + dsid + '</i> has been successfully shared.<p><p>As long as it stays shared, it can <ol>' +
                '<li>directly be accessed with the URL <a href="' + storageAccessURL + '" target="_blank">' + storageAccessURL + '</a>,</li>' +
                '<li>edited (only by you!) in the Datasheet Editor using <a href="' + datasheetURL + '">' + datasheetURL + '</a> (this page), and</li>' +
                '<li>viewed in the Geo-Browser using the URL <a href="' + geobrowserURL+ '" target="_blank">' + geobrowserURL + '</a>.</li></ol></p>';
            newAlert('success', title,  message);
		},
        error: function(xhr, textStatus, errorThrown) {
            var title = 'Error sharing dataset: ' + xhr.status + ' ' + errorThrown + '!';
            var message = '<p>The Dataset with ID <i>' + dsid + '</i> could not be shared! Maybe it has to be unshared first!</p>';
            newAlert('error', title,  message);
		}
	});
}

/**
 * Unshare a dataset using the unpublish() method of the DARIAH-DE PDP using DARIAH-DE OwnStorage.
 */
export function unpublishDataset() {
    // Assemble bearer token and logID.
    var token = bearerPrefix + readToken();
    var dsid = getDSID();
    var logID = logIDPrefix + (new Date()).getMilliseconds();
    // Unshare file.

    /*
    console.log("  ##  UNPUBLISH: " + storageURL + dsid + "/unpublish");
    console.log("  ##  TOKEN: " + token);
    console.log("  ##  LOGID: " + logID);
    */

    $.ajax({
		url: storageURL + dsid + "/unpublish",
        type: 'POST',
        headers: { 'Authorization': token, 'X-Transaction-ID': logID },
		success: function(response) {
            removePublic();
            setSharedUnsharedStatusOnScreen();
            var title = 'Dataset successfully unshared!';
            var message = '<p>The dataset with ID <i>' + dsid + '</i> has been successfully unshared.<br/> It can now only accessed by you again as long as you are logged in the Datasheet Editor or Geo-Browser!</p>';
            newAlert('success', title,  message);
		},
        error: function(xhr, textStatus, errorThrown) {
            var title = 'Error unsharing dataset: ' + xhr.status + ' ' + errorThrown + '!';
            var message = '<p>The dataset with ID <i>' + dsid + '</i> could not be unshared!</p>';
            newAlert('error', title,  message);
		}
	});
}

/**
 * Unpublish a dataset first, then delete.
 */
export function unpublishAndDeleteDataset() {
    // Assemble bearer token and logID.
    var dsid = getDSID();
    var token = bearerPrefix + readToken();
    var logID = logIDPrefix + (new Date()).getMilliseconds();
    // Delete file.
    $.ajax({
		url: storageURL + dsid + "/unpublish",
        type: 'POST',
        headers: { 'Authorization': token, 'X-Transaction-ID': logID },
		success: function() {
            deleteDatasetFromStorage(dsid);
		},
        error: function() {
            deleteDatasetFromStorage(dsid);
		}
	});
}

/**
 * Delete a dataset from the DARIAH-DE OwnStorage.
 */
export function deleteDatasetFromStorage(dsid) {
    // Assemble bearer token and logID.
    var token = bearerPrefix + readToken();
    var logID = logIDPrefix + (new Date()).getMilliseconds();
    // Delete file.
	$.ajax({
		url: storageURL + dsid,
		type: 'DELETE',
        headers: { 'Authorization': token, 'X-Transaction-ID': logID },
		success: function(response) {
            var title = 'Dataset successfully deleted!';
            var message = '<p>The dataset with ID <i>' + dsid + '</i> has been successfully deleted from the DARIAH-DE Storage.</p>';
            newAlert('success', title,  message);
            // Re-set storage ID.
            setDSID("");
            // Reload page.
            window.location = window.location.origin + window.location.pathname;
		},
        error: function(xhr, textStatus, errorThrown) {
            checkSharedStatus(dsid);
            var title = 'Error deleting dataset: ' + xhr.status + ' ' + errorThrown + '!';
            var message = '<p>The dataset with ID <i>' + dsid + '</i> could not be deleted from the DARIAH-DE Storage!</p>';
            newAlert('error', title,  message);
		}
	});
}

/**
 * Check if token is existing, if not, get one! action must always contain key AND value,
 * such as 'action=create' or 'id=12345'!
 */
export function authenticate(action) {
    // Show redirection message.
    var title = 'Redirecting to DARIAH AAI';
    var message = '<p>You are being redirected to the DARIAH AAI to get you a DARIAH-DE storage token for OwnStorage access.</p>';
    newAlert('success', title,  message);
    login(action);
}

/*
 * Sets the OAuth token from token hash, if existing.
 */
export function setTokenDSE() {
    // Get complete token hash from PDP.
    var acctok = window.location.hash.substr(1);
    // Get pure token out of complete token hash.
    var puretok = acctok.substr(acctok.indexOf("access_token=") + 13, acctok.indexOf("&") - 13);
    setToken(puretok);
    // Remove token hash from URL.
    window.location = window.location.origin + window.location.pathname + window.location.search;
}

/**
 * Redirect to PDP and login.
 */
export function login(action) {
    var questionmark = '';
    // Only use '?' if action is undefined.
    if (action) {
        questionmark = '?' + action;
    }
    window.location = pdpURL + window.location.href + questionmark;
}

/**
 * Logout: Just delete all entries (tokens, public and migration flags, and files) from (session) store and OwnStorage element list and back go to index page.
 */
export function logout() {
    clearStorage();
    setOwnStorageElementList("");
    var path = window.location.pathname;
    if (path.includes('list')) path = '/edit/index.html';
    window.location = window.location.origin+path;
}

/**
 * Get authentication information (in this case eppn/display name) from storage.
**/
export function getAuthInfo() {

    // Assemble bearer token and logID.
    var token = bearerPrefix + readToken();
    var logID = logIDPrefix + (new Date()).getMilliseconds();
    // Get auth info.
    $.ajax({
		url: storageURL + "/auth/info",
        type: 'GET',
        headers: { 'Authorization': token, 'X-Transaction-ID': logID },
		success: function(data, textStatus, xhr) {
            // Respond according to server status.
            // 200 OK  -->  success!
            if (xhr.status === 200) {
                $('#loggedin').append('<i class="icon-signout icon-white"></i> Logout (' + data.principal.attributes.DISPLAY_NAME + ')');
            }
            else {
                $('#loggedin').append('<i class="icon-signout icon-white"></i> Logout');
            }
		},
        error: function(xhr, textStatus, errorThrown) {
            // Respond according to server status.
            // FIXME How to react here? Are we not logged in then?
            $('#loggedin').append('<i class="icon-signout icon-white"></i> Logout');
		}
	});
}
