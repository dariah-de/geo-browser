/**
 * Configuration for the Datasheet Editor workflow and geo settings.
 */
/*
 * Configuration for OwnStorage element list.
 */
export const ownStorageElementListSize = 25;
let ownStorageElementList = "";

export function setOwnStorageElementList(value) {
    ownStorageElementList = value;
}

export function getOwnStorageElementList(){
    return ownStorageElementList;
}

// DARIAH storage id
let dsid;
export function setDSID(value) {
    dsid = value;
}

export function getDSID(){
    if (dsid && dsid === '') return null;
    return dsid;
}

export const host = window.location.protocol + '//' + window.location.host;

// Some prefixes.
export const logIDPrefix = 'DATASHEET_';
export const bearerPrefix = 'bearer ';

// All the possible headers (for new sheets).
export const defaultColumnHeaders = ["Name", "Address", "Description", "Longitude", "Latitude", "TimeStamp", "TimeSpan:begin", "TimeSpan:end", "GettyID"];
// Column headers which need to be there for geolocation completion.
export const requiredColumnsGeolocation = ["Address", "Longitude", "Latitude", "GettyID"];
// Column headers which need to be there for opening in Geo-Browser.
export const requiredColumnsGeobrowser = ["Address", "TimeStamp", "TimeSpan:begin", "TimeSpan:end", "Longitude", "Latitude"];
// columns with date format
export const dateColumns = ["TimeStamp", "TimeSpan:begin", "TimeSpan:end"];
// Mimetypes which pass check to get parsed into table. Excel is important to support CSVs exported from Excel!
export const allowedMimeTypes = ["text/csv", "text/comma-separated-values", "application/vnd.ms-excel", "application/vnd.dariahde.geobrowser.csv", "application/vnd.google-earth.kml+xml", "text/xml"]
// CSV mimetype used for storing datasheets.
export const csvStorageMimetype = 'application/vnd.dariahde.geobrowser.csv';

/*
 * Configuration for geographic checks & maps.
 */
export const tgnBaseURL = 'https://ref.de.dariah.eu/tgn/v1/';
export const tgnMatchURL = 'https://ref.de.dariah.eu/tgn/v1/exact-match';
export const ognURL = 'https://ref.de.dariah.eu/geonames/postalCodeSearchJSON';
export const ognUsername = 'dariahde';
export const osmSearchUrl = 'https://nominatim.openstreetmap.org/search';
export const noResults = 'No results, please try map selection.';
export const maxCharsPerTitle = 55;
