/**
 * Functions dealing with the handsontable widget
 *
 */
import {
	getDSID,
	dateColumns,
	defaultColumnHeaders,
	requiredColumnsGeobrowser,
	requiredColumnsGeolocation
} from "./dariah.workflow.conf";
import Handsontable from 'handsontable';
import {autoSave} from "./dariah.workflow";
import {isEmptyString, newAlert} from "./dariah.utils";
import {getBase64Without} from "./dariah.workflow.geo";
import {xsdDateOrDateTime, xsdGYear, xsdGYearMonth} from "./dariah.workflow.datecheck";
var tableInstance;

export function getTable(){

	if (!tableInstance) initTable();
	return tableInstance;
}

export function createInitialTableContent(){
 	 // column titles are placed in the first row, i.e. are no actual column headers. Do not use globals here!
	 var initialData = [["Name", "Address", "Description", "Longitude", "Latitude", "TimeStamp", "TimeSpan:begin", "TimeSpan:end", "GettyID"]];
	 for (var i = 0; i < 30; i++) initialData.push(['', '', '', '', '', '', '', '', '']);
	 return initialData;
 }

/**
 * Handles cell settings, i.e. read-only, autocomplete and date format
 */

function handleHeaderCells(row) {
	if (row === 0) { //header
		return {
			renderer: headerRenderer,
			type: 'autocomplete',
			trimDropdown: false,
			source: ["Name", "Address", "Description", "Longitude", "Latitude", "TimeStamp", "TimeSpan:begin", "TimeSpan:end", "GettyID"],
			strict: false,
		};
	}
}

/**
 * Returns settings for custom date type field
 */
function getDateFieldProps(){
	return {
		validator: function(value, callback){ //custom date validator
			if (!value || xsdGYear.test(value) || xsdGYearMonth.test(value) || xsdDateOrDateTime.test(value)) {
				callback(true);
			} else callback(false);
		},
		type: 'date',
		correctFormat: false,
		dateFormat: 'YYYY-MM-DD',
		defaultDate: '1900-01-01',
		datePickerConfig: {
			yearRange: [0, 2050]
		}
	};
}

export function initTable() {
	var container = document.getElementById('dataTable');
	if (tableInstance) tableInstance.destroy();
	tableInstance = new Handsontable(container, {
		licenseKey: 'non-commercial-and-evaluation',
		manualColumnResize: true,
		manualRowResize: true,
		dropdownMenu: true,
		contextMenu: true,
		rowHeaders: true, // 123
		colHeaders: true, // ABC
		stretchH: 'all',
		columnSorting: {
			headerAction: true,
			indicator: true,
			sortEmptyCells: false,
			compareFunctionFactory: function (sortOrder) {
				return function (value, nextValue) {
					if (!value) return 0;
					if (getColumnNames().includes(value)) return -1; //always stays on top
					if (value.toLowerCase() === nextValue.toLowerCase()) return 0;
					if (value.toLowerCase() > nextValue.toLowerCase()) return sortOrder === 'asc' ? 1 : -1;
					else return sortOrder === 'asc' ? -1 : 1;
				}
			}
		},
		outsideClickDeselects: function (event) {
			//clicking buttons does not clear selection
			return !(event.nodeName === 'BUTTON' || event.parentElement && event.parentElement.nodeName === 'BUTTON');
		},
		minSpareCols: 1, // always keep at least 1 spare column at the right
		cells: handleHeaderCells, // handles header row
		afterLoadData: function(){ //set date formats
				var dateColumnsIndices = getDateColumns();
				if (dateColumnsIndices) {
					var dateProps = getDateFieldProps();
					$.each(dateColumnsIndices, function (index, column) {
						for (var i = 1; i < tableInstance.getDataAtCol(column).length; i++) {
							tableInstance.setCellMetaObject(i, column, dateProps);
						}
					});
					tableInstance.render();
				}
		},
		afterChange: function (changes, source) {
			if (!changes) return;
			if (source !== 'loadData') { //don't save on load
				autoSave(changes);
			}
			changes.forEach(function([row, col, oldValue, newValue]) {
				if (row === 0){ //update columns with date format
					var addedDateColumn = dateColumns.includes(newValue);
					var removedDateColumn = dateColumns.includes(oldValue) && !dateColumns.includes(newValue);
					if (addedDateColumn || removedDateColumn){
						var dateProps = getDateFieldProps();
						for (var i = 1; i<tableInstance.getDataAtCol(col).length; i++) {
							if (addedDateColumn) tableInstance.setCellMetaObject(i, col, dateProps);
							else if (removedDateColumn) tableInstance.setCellMetaObject(i, col, {"type": "text", validator: null});
						}
						tableInstance.render();
					}
				}
			});
		}
	});

 }
/**
 * Retrieves values in the first table row, which we use as column names
 */
 export function getColumnNames(){
	var columnNames = defaultColumnHeaders;
	if (tableInstance) columnNames = tableInstance.getData()[0];
	return columnNames;
 }
/**
 * Identifies indices of columns that should receive "date" type
 */
function getDateColumns() {

	if (!tableInstance) return null;
	 var data = tableInstance.getData();
	 var indices = [];
	dateColumns.forEach(function(name){
		 if (data.length < 1) return;
		 var dateColumn = data[0].findIndex(x => x === name);
		 if (dateColumn !== -1) indices.push(dateColumn);
	 });
	 return indices;
 }

/**
 * Highlights column names in first row
 */
function headerRenderer(instance, td, row, col, prop, value, cellProperties) {
	Handsontable.renderers.TextRenderer.apply(this, arguments);
	td.style.fontWeight = 'bold';
	td.style.color = 'green';
}

/**
 * Clears selection of cells
 */
export function clearTableSelection(){
	if (tableInstance) tableInstance.deselectCell();
}

/**
 * This function checks if every needed column is there for envoking Geolocation completion
 * (means Address columns only!).
 */
export function checkColumnHeadersGeolocation() {

	// Close previous column alert.
	$(".columnAlert").alert('close');

	var arr = tableInstance.getData();
	var ready = true;

	$('#missingCols').empty();

	$(requiredColumnsGeolocation).each(function(index, val) {
		ready = findOrAlertCreatableColumn(arr, val) && ready;
	});

	return ready;
}

/**
 * This function checks if every needed column is there.
 */
export function checkColumnHeadersGeobrowser() {

    // Close previous column alert.
	$(".columnAlert").alert('close');

	var arr = tableInstance.getData();
	var ready = true;

	$('#missingCols').empty();

	$(requiredColumnsGeobrowser).each(function(index, val) {
		ready = findOrAlertCreatableColumn(arr, val) && ready;
	});

	return ready;
}

/**
 *
 */
 export function findColumn(arr, columnName) {
	return $.inArray(columnName, arr[0]);
}

/**
 *
 */
function findOrAlertCreatableColumn(arr, columnName) {
	var id = $.inArray(columnName, arr[0]);
	if (id < 0) {
		var buttonId = "add"+columnName.replace(":","_")+"Button";
		var message = '<p>Column name <strong>“' + columnName + '“</strong> is missing. ' +
			'You can either set an existing and matching cell header with this name, or you could create the missing column by clicking the ' +
			'<button id='+buttonId+' class="btn btn-primary btn-small">Create this column</button> button. ' +
			'In both cases: Please try again!</p>';
		newAlert('error', 'Missing Column', message, 'columnAlert', getBase64Without(columnName));
		$('#'+buttonId).bind('click', function () {createColumn(columnName);});
		return false;
	}
	return true;
}

/**
 *
 */
export function createColumn(columnName) {
	var arr = tableInstance.getData();
	var id = $.inArray(columnName, arr[0]);
	// entry not existing, create
	if (id < 0) {
		var colId;
		// find next empty column
		for (var i = 0; i<arr[0].length; i++) {
			if(!arr[0][i] || arr[0][i] === '') {
				colId=i;
				break;
			}
		}
		//tableInstance.alter('insert_col', colId, columnName);
		tableInstance.selectCell(0, colId);
		tableInstance.setDataAtCell(0, colId, columnName);
		clearTableSelection();
        // Delete column alert.
    	$("#" + getBase64Without(columnName)).alert('close');
	}

	return id;
}

export function isSelected (row, col){
	var selected = tableInstance.getSelectedLast();
	if (!selected) return true; //no selection at all --> treat as selected
	var startRow = selected[0];
	var startCol = selected[1];
	var endRow = selected[2];
	var endCol = selected[3];
	return row >= startRow && row <= endRow && col >= startCol && col <= endCol;
}

/**
 * Write value to table cell.
 * @param forceOverwrite -- whether the current table value should be overwritten
 * @param columnValue -- current column value
 * @param row -- current row
 * @param column -- column index
 * @param newValue -- value to write
 */
export function writeTableCell(forceOverwrite, columnValue, row, column, newValue){
	if ((forceOverwrite || isEmptyString(columnValue)) && isSelected(row, column)) {
		if (!isEmptyString(newValue)) tableInstance.setDataAtCell(row, column, newValue);
	}
}
/**
 * Table is filled with coords and Getty IDs only if address is contained in table's address field
 * or address field is empty AND (if given) address field content is contained in variant string, or matching GettyID is given.
 * Address values can be set by Map selection if address field is empty and matching GettyID available.
 * Existing GettyIDs cannot be overwritten by geolocation, but by Map selection. No values can be overwritten by empty values.
 */
export function updateCoordinates(address, lat, long, gettyId, forceOverwrite, variant) {
    // In case variant is not existing or given.
    if (!variant) {
        variant = "";
    }
    lat = String(lat); // Longitude/latitude fields expect text values
    long = String(long);
	var arr = tableInstance.getData();
	var addressColumn = findColumn(arr, 'Address');
	var longColumn = findColumn(arr, 'Longitude');
	var latColumn = findColumn(arr, 'Latitude');
	var gettyColumn = findColumn(arr, 'GettyID');

	var save = false;
	$(arr).each(function(index, val) {
		if (!isEmptyString(address) && $.trim(val[addressColumn]) === $.trim(address) || ($.trim(val[addressColumn]) !== "" && variant.includes($.trim(val[addressColumn])))) {
			writeTableCell(forceOverwrite, val[gettyColumn], index, gettyColumn, gettyId);
			writeTableCell(forceOverwrite, val[longColumn], index, longColumn, long);
			writeTableCell(forceOverwrite, val[latColumn], index, latColumn, lat);
			save = true;
		} else if (!isEmptyString(gettyId) && $.trim(val[gettyColumn]) === $.trim(gettyId)){ //update by GettyID
			writeTableCell(forceOverwrite, val[longColumn], index, longColumn, long);
			writeTableCell(forceOverwrite, val[latColumn], index, latColumn, lat);
			//addresses cannot be overwritten
			writeTableCell(false, val[addressColumn], index, addressColumn, address);
			save = true;
		}
	});

	if(save && getDSID() !== null) {
        autoSave();
    }
}

/**
 * This function checks if a row is empty.
 */
export function rowIsEmpty(rowData) {
    var result = true;
    rowData.forEach((item) => {
        if (item) {
            result = false;
        }
    }) ;
    return result;
}
