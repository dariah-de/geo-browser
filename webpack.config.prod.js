const webpack = require("webpack");
const {merge}  = require('webpack-merge');
const baseConfig = require('./webpack.config.js');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const version = require("./package.json").version;

module.exports = merge(baseConfig, {
    mode: 'production',
    plugins: [
        new webpack.DefinePlugin({
            STORAGE_URL: JSON.stringify('https://cdstar.de.dariah.eu/dariah/'),
            PDP_URL: JSON.stringify('https://pdp.de.dariah.eu/oauth2/oauth2/authorize?response_type=token&client_id=dariah-de-geo-browser&scope=read,write&redirect_uri='),
            DARIAH_STATUS_URL: JSON.stringify('https://dariah-de.github.io/status/dariah/embed.html'),
        }),
        new webpack.BannerPlugin({
            banner: version
        }),
        new HtmlWebpackPlugin({
            template: './index.html',
            inject: 'head',
            chunks : ['main'],
            version: version
        }),
        new HtmlWebpackPlugin({
            template: './edit/list.html',
            filename: 'edit/list.html',
            inject: 'head',
            chunks : ['edit'],
            version: version
        }),
        new HtmlWebpackPlugin({
            template: './edit/index.html',
            filename: 'edit/index.html',
            inject: 'head',
            chunks : ['edit'],
            version: version
        }),
        new HtmlWebpackPlugin({
            template: './embed/index.html',
            filename: 'embed/index.html',
            inject: 'head',
            chunks: ['main'],
            version: version
        }),
    ]
});
